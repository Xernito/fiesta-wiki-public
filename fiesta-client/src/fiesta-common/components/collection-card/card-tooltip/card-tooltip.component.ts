import { Component, OnInit } from '@angular/core';
import { Card } from '@fiesta-common/dto/cards/card';
import { CardsService } from '@fiesta-common/services/cards.service';

@Component({
    selector: 'app-card-tooltip',
    templateUrl: './card-tooltip.component.html',
    styleUrls: ['./card-tooltip.component.scss']
})
export class CardTooltipComponent implements OnInit {

    card: Card;

    get cardRank(): string {
        return this.cardService.cardRank(this.card);
    }

    constructor(private cardService: CardsService) {
    }

    ngOnInit() {
    }
}
