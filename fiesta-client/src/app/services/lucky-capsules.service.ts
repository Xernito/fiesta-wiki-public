import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { forkJoin, Observable } from 'rxjs';
import { LuckyCapsule, LuckyCapsuleDTO, LuckyCapsuleGroupRate, LuckyCapsuleGroupRateDTO, LuckyCapsuleReward, LuckyCapsuleRewardDTO } from '@dto/lucky-capsules/lucky-capsules';
import { environment } from '@environments/environment';
import { ItemService } from '@fiesta-common/services/item.service';
import { defaultIfEmpty, map, mergeMap } from 'rxjs/operators';

@Injectable({
   providedIn: 'root'
})
export class LuckyCapsulesService {

   constructor(
      private http: HttpClient,
      private itemService: ItemService
   ) { }

   getCapsuleGroupRate(itemId: number): Observable<LuckyCapsuleGroupRate[]> {
      return this.http.get<LuckyCapsuleGroupRateDTO[]>(environment.api + 'lucky-capsule-rewards/' + itemId).pipe(
         map(groups => groups.map(g => this.convertGroup(g)))
      );
   }

   getLuckyCapsules(type: number): Observable<LuckyCapsule[]> {
      return this.http.get<LuckyCapsuleDTO[]>(environment.api + 'lucky-capsules/' + type).pipe(
         mergeMap(capsules => forkJoin(capsules.map(c => {
            return this.itemService.getItem(c.capsule_id).pipe(
               map(capsule => {
                  return { capsule, server_id: c.server_id } as LuckyCapsule;
               })
            );
         }
         )).pipe(defaultIfEmpty<LuckyCapsule[]>([])))
      );
   }

   convertReward(reward: LuckyCapsuleRewardDTO): LuckyCapsuleReward {
      const _ = new LuckyCapsuleReward();

      _.item = this.itemService.convertItem(reward.item);

      return _;
   }

   convertGroup(group: LuckyCapsuleGroupRateDTO): LuckyCapsuleGroupRate {
      const _ = new LuckyCapsuleGroupRate();

      _.id = group.id;
      _.rewards = group.rewards.map(r => this.convertReward(r));
      _.rate = group.rate;

      return _;
   }
}
