package com.fiestawiki.license.dao.join.repository;

import com.fiestawiki.license.dao.join.LicenseJoin;
import org.hibernate.SessionFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional(readOnly = true)
public class LicenseJoinRepositoryImpl implements LicenseJoinRepositoryCustom {

    @PersistenceContext
    EntityManager entityManager;

    CriteriaBuilder criteriaBuilder;

    SessionFactory sessionFactory;

    @PostConstruct
    private void init()
    {
        criteriaBuilder = entityManager.getCriteriaBuilder();
    }

    @Override
    public Page<LicenseJoin> findActiveLicenses(Pageable pageable, Optional<Boolean> active)
    {
        List<String> whereClauses = new ArrayList<>();

        if(active.isPresent()) {
            whereClauses.add("active = :active ");
        }

        String orderByClause = "";
        if(!pageable.getSort().isEmpty()) {
            orderByClause = "ORDER BY ";
            List<String> orders = new ArrayList<>();

            for(Sort.Order o : pageable.getSort().toList())
            {
                switch(o.getProperty())
                {
                    case "name":
                        orders.add("name " + o.getDirection().toString());
                    case "level":
                        orders.add("level " + o.getDirection().toString());
                }
            }

            orderByClause += String.join(", ", orders);
        }

        // Select distinct on allow to avoid GROUP BY and SELECT other columns that are not part of the GROUP BY
        // ORDER BY ms.name is mandatory first for DISTINCT ON and then order by level to always get lowest level mob
        String speciesSubQuery = "(SELECT DISTINCT ON(ms.species_name) ms.id, ms.species_name, mi.name, mi.level " +
                "FROM mob_species AS ms " +
                "INNER JOIN " +
                "(SELECT * FROM mob_info) AS mi " +
                "ON ms.id = mi.mob_species_fk " +
                (!whereClauses.isEmpty() ? "WHERE " + String.join(" AND ", whereClauses) : "") + // WHERE clause should be on sub query because the WHERE in main query only apply to the first of this subset
                "ORDER BY ms.species_name, mi.level)";

        Query nativeQuery = entityManager.createNativeQuery("SELECT * " +
                        "FROM licenses AS lic " +
                        "INNER JOIN " +
                        speciesSubQuery + " AS s " +
                        "ON s.id = lic.pk1 " +
                        orderByClause,
                LicenseJoin.class)
                .setFirstResult((int) pageable.getOffset())
                .setMaxResults(pageable.getPageSize());

        Query countQuery = entityManager.createNativeQuery("SELECT COUNT(*) " +
                "FROM licenses AS lic " +
                "INNER JOIN " +
                speciesSubQuery + " AS s " +
                "ON s.id = lic.pk1 ");

        if(active.isPresent())
        {
            nativeQuery.setParameter("active", active.get());
            countQuery.setParameter("active", active.get());
        }

        @SuppressWarnings("unchecked")
        List<LicenseJoin> result = nativeQuery.getResultList();

        return new PageImpl<>(
                result,
                pageable,
                ((Number) countQuery.getSingleResult()).longValue());
    }
}
