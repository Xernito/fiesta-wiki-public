﻿using iQuest;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace SHN_SQL_Converter.Files
{
    class QuestEndNpc : SHNFileBase
    {
        public override void Load(string input_folder)
        {
            SHNFile shn = new SHNFile(true);

            LoadingData loading_data = shn.LoadFile(Path.Combine(input_folder, Filename));
            shn.ReadColumns(loading_data);

            Table = shn.table;
            Table.Namespace = Namespace;
            Table.TableName = Tablename;

            // Because MobInfo ID has been changed to UInt32
            Table.Columns["MobID"].DataType = typeof(uint);

            shn.ReadRows(loading_data);

            // Rename because SQL 'Group' keyword is reserved
            Table.Columns["Group"].ColumnName = "group_id";
        }

        private bool DoesMobExist(DataRow row)
        {
            var mob_id = row["MobID"];
            if (mob_id != DBNull.Value)
            {
                DataRow[] r = DataSet.Tables["mob_info"].Select(string.Format("ID = {0}", (uint)mob_id));
                if (r.Length == 0)
                {
                    return false;
                }
            }

            return true;
        }

        public override void Patch(string patch_folder)
        {
            List<DataRow> rowsToRemove = new List<DataRow>();

            foreach (DataRow dr in Table.Rows)
            {
                // Some mobs in EU don't exists
                if (!DoesMobExist(dr))
                {
                    // The previous quest doesn't exist, to avoid invalid foreign key just remove this quest
                    rowsToRemove.Add(dr);
                    continue;
                }
            }

            foreach (var r in rowsToRemove)
            {
                Table.Rows.Remove(r);
            }
        }

        public override void CreateConstraints()
        {
            Utils.AddForeignKey(
                DataSet,
                Tablename, "MobID",
                "mob_info",
                "mob_info_id_fk"
                );

            Utils.AddForeignKeyColumn(
                DataSet,
                Tablename, new string[] { "ID" },
                "quest_data", new string[] { "ID" },
                "quest_id");
        }
    }
}
