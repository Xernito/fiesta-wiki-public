import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { LicenseLevel } from 'license/dto/licenses';

@Component({
    selector: 'app-table-info',
    templateUrl: './table-info.component.html',
    styleUrls: ['./table-info.component.scss']
})
export class TableInfoComponent implements OnInit {

    dataSource: MatTableDataSource<LicenseLevel>;

    displayedColumns: string[] =
        [
            'suffix',
            'killCount',
            'damage',
            'crit'
        ];

    private _data: LicenseLevel[];
    @Input()
    get data(): LicenseLevel[] { return this._data; }
    set data(d: LicenseLevel[]) {
        this._data = d;
        this.dataSource = new MatTableDataSource(d);
    }

    constructor() { }

    ngOnInit(): void {
    }

}
