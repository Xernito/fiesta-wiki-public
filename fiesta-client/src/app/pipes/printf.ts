import { Pipe, PipeTransform } from '@angular/core';
import { FiestaMinutesTimePipe } from '../../fiesta-common/pipes/fiesta-time';
import { FiestaMoneyPipe } from './money-pipe';

@Pipe({
   name: 'printf'
})
export class PrintfPipe implements PipeTransform {

   constructor(
      private timePipe: FiestaMinutesTimePipe,
      private moneyPipe: FiestaMoneyPipe
      ) {}

   transform(input: string, value: any): string {

      const reg = /\{\{.*\}\}/g;
      let index = input.search(reg);
      while (index !== -1) {

         if (this.match(input, index, '{{VALUE}}')) {
            input = input.replace(reg, value);
         } else if (this.match(input, index, '{{TIME}}')) {
            input = input.replace(reg, this.timePipe.transform(value));
         } else if (this.match(input, index, '{{MONEY}}')) {
            input = input.replace(reg, this.moneyPipe.transform(value));
         } else {
            return '';
         }

         index = input.search(RegExp('\{\{.*\}\}', 'g'));
      }

      return input;
   }

   private match(input: string, index: number, keyword: string): boolean {
      return input.substring(index, index + keyword.length) === keyword;
   }

}
