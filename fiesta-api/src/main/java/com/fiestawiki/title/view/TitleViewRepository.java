package com.fiestawiki.title.view;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TitleViewRepository  extends JpaRepository<TitleView, Long> {
}
