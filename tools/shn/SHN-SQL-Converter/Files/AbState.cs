﻿using iQuest;
using System.Data;
using System.IO;

namespace SHN_SQL_Converter.Files
{
    class AbState : SHNFileBase
    {
        public override void Load(string input_folder)
        {
            SHNFile shn = new SHNFile(Path.Combine(input_folder, Filename));

            Table = shn.table;
            Table.Namespace = Namespace;
            Table.TableName = Tablename;

            // Replaced by the auto generated unique_id because those ID have some duplicates
            Table.Columns.Remove("ID");

            Table.Columns["InxName"].Unique = true;

            foreach (DataRow dr in Table.Rows)
            {
                Utils.RemoveEmptyString(dr, "PartyState1");
                Utils.RemoveEmptyString(dr, "PartyState2");
                Utils.RemoveEmptyString(dr, "PartyState3");
                Utils.RemoveEmptyString(dr, "PartyState4");
                Utils.RemoveEmptyString(dr, "PartyState5");
            }
        }

        public override void CreateConstraints()
        {
            Utils.AddForeignKeyColumn(
                DataSet,
                Tablename, new string[] { "PartyState1" },
                Tablename, new string[] { "InxName" },
                "party_state_1_fk");
            Utils.AddForeignKeyColumn(
                DataSet,
                Tablename, new string[] { "PartyState2" },
                Tablename, new string[] { "InxName" },
                "party_state_2_fk");
            Utils.AddForeignKeyColumn(
                DataSet,
                Tablename, new string[] { "PartyState3" },
                Tablename, new string[] { "InxName" },
                "party_state_3_fk");
            Utils.AddForeignKeyColumn(
                DataSet,
                Tablename, new string[] { "PartyState4" },
                Tablename, new string[] { "InxName" },
                "party_state_4_fk");
            Utils.AddForeignKeyColumn(
                DataSet,
                Tablename, new string[] { "PartyState5" },
                Tablename, new string[] { "InxName" },
                "party_state_5_fk");
        }
    }
}
