import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { StoreItemSet, StoreItem, StoreItemView } from '@fiesta-common/dto/store-items/store-item';

@Component({
   selector: 'app-store-item-set-edition-dialog',
   templateUrl: './store-item-set-edition-dialog.component.html',
   styleUrls: ['./store-item-set-edition-dialog.component.scss']
})
export class StoreItemSetEditionDialogComponent implements OnInit {

   storeItemSetForm: FormGroup;
   nameControl = new FormControl({ value: '' });

   output: { set: StoreItemSet; items: StoreItem[]; };

   view: StoreItemView;

   constructor(
      public dialogRef: MatDialogRef<StoreItemSetEditionDialogComponent, StoreItemSet>,
      private formBuilder: FormBuilder,
      @Inject(MAT_DIALOG_DATA) public data: any
   ) {

      this.output = { set: new StoreItemSet(), items: [] };

      this.view = data.view;

      this.storeItemSetForm = this.formBuilder.group({
         name: this.nameControl
      });

      this.nameControl.setValue(this.output.set.name);
   }

   ngOnInit() {

      this.nameControl.valueChanges.subscribe(name => {
         this.output.set.name = name;
      });
   }

   onItemChecked(item: StoreItem, event: MatCheckboxChange) {
      if (event.checked) {
         this.output.items.push(item);
      } else {
         const index =  this.output.items.indexOf(item);

         if (index >= 0) {
            this.output.items.splice(index, 1);
         }
      }
   }
}
