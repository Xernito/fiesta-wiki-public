package com.fiestawiki.cards;

import com.fiestawiki.cards.dto.CardDTO;
import com.fiestawiki.cards.dto.CardGroupDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CardController {

    @Autowired
    private CardService cardService;

    @GetMapping("cards")
    public ResponseEntity<List<Long>> getCards()
            throws Exception
    {
        List<Long> cards = cardService.getCards();

        return ResponseEntity.ok(cards);
    }

    @GetMapping("card/{id}")
    public ResponseEntity<CardDTO> getCard(@PathVariable(value = "id") Long id)
            throws Exception
    {
        CardDTO card = cardService.getCard(id);

        return ResponseEntity.ok(card);
    }

    @GetMapping("card-group/{id}")
    public ResponseEntity<CardGroupDTO> getCardGroup(@PathVariable(value = "id") Long id)
            throws Exception
    {
        CardGroupDTO card_group = cardService.getCardGroup(id);

        return ResponseEntity.ok(card_group);
    }
}
