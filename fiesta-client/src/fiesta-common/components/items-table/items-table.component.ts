import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Item } from '@fiesta-common/dto/items/item';
import { Page } from '@fiesta-common/dto/items/item-info-page';
import { LoadingStatus } from '@enums/loading-return';

@Component({
    selector: 'app-items-table',
    templateUrl: './items-table.component.html',
    styleUrls: ['./items-table.component.scss']
})
export class ItemsTableComponent implements OnInit {

    @Output()
    pageChange: EventEmitter<PageEvent> = new EventEmitter();

    @Input()
    pageObs: Observable<Page<Item>>;

    @Input()
    enhancement = 0;

    @Input('columns')
    displayedColumns: string[] =
        [
            'icon',
            'name',
        ];
    dataSource: MatTableDataSource<Item>;

    page = new Page;

    loadingErr: LoadingStatus = 'loading';

    constructor(
        private router: Router
    ) { }

    ngOnInit() {
        this.pageObs.subscribe(
            page => {
                this.page = page;
                this.dataSource = new MatTableDataSource(page.content);
                this.loadingErr = 'ok';
            },
            err => {
                this.loadingErr = 'error';
            }
        );
    }

    clear() {
        this.dataSource = new MatTableDataSource;
        this.loadingErr = 'loading';
    }

    onPageChange(pageEvent: PageEvent) {
        this.pageChange.emit(pageEvent);
    }

    onSort(event: Sort) {

        let orderId = 0;

        switch (event.active) {
            case 'id':
                orderId = event.direction === 'desc' ? 0 : 1;
                break;
            case 'name':
                orderId = event.direction === 'desc' ? 2 : 3;
                break;
            case 'level':
                orderId = event.direction === 'desc' ? 4 : 5;
                break;
            case 'damage':
                orderId = event.direction === 'desc' ? 6 : 7;
                break;
            case 'mdamage':
                orderId = event.direction === 'desc' ? 8 : 9;
                break;
            case 'aim':
                orderId = event.direction === 'desc' ? 10 : 11;
                break;
            case 'crit':
                orderId = event.direction === 'desc' ? 12 : 13;
                break;
            case 'atkspeed':
                orderId = event.direction === 'desc' ? 14 : 15;
                break;
            case 'buyprice':
                orderId = event.direction === 'desc' ? 16 : 17;
                break;
            case 'sellprice':
                orderId = event.direction === 'desc' ? 18 : 19;
                break;
            case 'itemgradetype':
                orderId = event.direction === 'desc' ? 20 : 21;
                break;
            case 'grade':
                orderId = event.direction === 'desc' ? 22 : 23;
                break;
            case 'defense':
                orderId = event.direction === 'desc' ? 24 : 25;
                break;
            case 'mdefense':
                orderId = event.direction === 'desc' ? 26 : 27;
                break;
            case 'evasion':
                orderId = event.direction === 'desc' ? 28 : 29;
                break;
            case 'blockrate':
                orderId = event.direction === 'desc' ? 34 : 35;
                break;
            case 'poisonresist':
                orderId = event.direction === 'desc' ? 36 : 37;
                break;
            case 'diseaseresist':
                orderId = event.direction === 'desc' ? 38 : 39;
                break;
            case 'curseresist':
                orderId = event.direction === 'desc' ? 40 : 41;
                break;
            case 'aimRate':
                orderId = event.direction === 'desc' ? 42 : 43;
                break;
            case 'evasionRate':
                orderId = event.direction === 'desc' ? 44 : 45;
                break;
        }

        this.router.navigate([], { queryParams: { order: orderId }, queryParamsHandling: 'merge' });
    }
}
