package com.fiestawiki.title;

import com.fiestawiki.cards.CardService;
import com.fiestawiki.title.data.TitleData;
import com.fiestawiki.title.data.TitleDataCompositeDTO;
import com.fiestawiki.title.data.TitleDataDTO;
import com.fiestawiki.title.data.TitleDataRepository;
import com.fiestawiki.title.stats.TitleStateViewDTO;
import com.fiestawiki.title.stats.TitleStateViewRepository;
import com.fiestawiki.title.view.TitleViewDTO;
import org.modelmapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TitleService {

    @Autowired
    private CardService cardService;

    @Autowired
    private TitleDataRepository titleDataRepository;

    @Autowired
    private TitleStateViewRepository titleStateViewRepository;

    @Autowired
    private ModelMapper modelMapper;

    @PostConstruct
    public void postConstruct() {

        ///
        Converter<TitleData, TitleDataCompositeDTO> getTitleDataComposite = new AbstractConverter<TitleData, TitleDataCompositeDTO>() {
            public TitleDataCompositeDTO convert(TitleData src) {

                TitleDataCompositeDTO composite = new TitleDataCompositeDTO();

                if(src.getTitle0() != null)
                {
                    composite.addTitle(convertTitleData(src, 0));
                }
                if(src.getTitle1() != null)
                {
                    composite.addTitle(convertTitleData(src, 1));
                }
                if(src.getTitle2() != null)
                {
                    composite.addTitle(convertTitleData(src, 2));
                }
                if(src.getTitle3() != null)
                {
                    composite.addTitle(convertTitleData(src, 3));
                }

                Type listType = new TypeToken<List<TitleStateViewDTO>>(){}.getType();
                List<TitleStateViewDTO> stats = modelMapper.map(src.getStats(), listType);

                composite.setStats(stats);

                return composite;
            }
        };

        modelMapper.addConverter(getTitleDataComposite);
    }

    public TitleDataDTO convertTitleData(TitleData title, int titleIndex)
    {
        TitleDataDTO titleData = new TitleDataDTO();
        titleData.setPermit(title.getPermit());
        titleData.setRefresh(title.getRefresh());
        switch(titleIndex)
        {
            case 0:
                titleData.setTitle(title.getTitle0());
                titleData.setFame(title.getFame0());
                titleData.setValue(title.getValue0());
                break;
            case 1:
                titleData.setTitle(title.getTitle1());
                titleData.setFame(title.getFame1());
                titleData.setValue(title.getValue1());
                break;
            case 2:
                titleData.setTitle(title.getTitle2());
                titleData.setFame(title.getFame2());
                titleData.setValue(title.getValue2());
                break;
            case 3:
                titleData.setTitle(title.getTitle3());
                titleData.setFame(title.getFame3());
                titleData.setValue(title.getValue3());
                break;
        }

        if(title.getView().isPresent())
        {
            titleData.setView(modelMapper.map(title.getView().get(), TitleViewDTO.class));
        }
        if(title.getCardTitle() != null)
        {
            titleData.setCard(
                    cardService.convertCardMinimal(title.getCardTitle().getCard())
            );
        }

        return titleData;
    }

    public List<TitleDataDTO> getTitles() {

        List<TitleData> titles = titleDataRepository.findAll();

        Type listType = new TypeToken<List<TitleDataCompositeDTO>>(){}.getType();
        List<TitleDataCompositeDTO> compositeDTO = modelMapper.map(titles, listType);

        List<TitleDataDTO> titlesDTO =
                compositeDTO
                        .stream()
                        .map(
                                title -> {
                                    List<TitleDataDTO> ts = title.getTitles();

                                    for(int i = 0; i < ts.size(); i++) {
                                        TitleDataDTO t = ts.get(i);
                                        t.setStats(title.getStat(i));
                                    }
                                    return ts;
                                }
                        )
                        .flatMap(Collection::stream)
                        .collect(Collectors.toList());

        return titlesDTO;
    }
}
