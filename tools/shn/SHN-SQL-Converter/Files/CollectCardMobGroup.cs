﻿
namespace SHN_SQL_Converter.Files
{
    class CollectCardMobGroup : SHNFileBase
    {
        public override void CreateConstraints()
        {
            Utils.AddForeignKeyColumn(
                DataSet,
                Tablename, new string[] { "CC_MobInx" },
                "mob_info", new string[] { "InxName" },
                "mob_info_fk", false);

            Utils.AddForeignKey(
                DataSet,
                Tablename, "CC_CardMobGroup",
                "collect_card_group_desc",
                "card_group_fk"
                );
        }
    }
}
