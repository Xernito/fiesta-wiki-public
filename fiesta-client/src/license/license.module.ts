import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FiestaCommonModule } from '@fiesta-common/fiesta-common.module';
import { MatTableModule } from '@angular/material/table';
import { FormsModule } from '@angular/forms';
import { MatPaginatorModule } from '@angular/material/paginator';
import { TableInfoComponent } from './components/table-info/table-info.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LicensePageComponent } from './components/license-page/license-page.component';

const routes: Routes = [
    { path: '', component: LicensePageComponent }
];

@NgModule({
    declarations: [
        LicensePageComponent,
        TableInfoComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FiestaCommonModule,
        FlexLayoutModule,
        MatTableModule,
        MatPaginatorModule,
        FormsModule,
    ]
})
export class LicenseModule { }
