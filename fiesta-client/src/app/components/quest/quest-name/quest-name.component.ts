import { Component, OnInit, Input } from '@angular/core';
import { Quest } from '@fiesta-common/dto/quests/quest';

@Component({
    selector: 'app-quest-name',
    templateUrl: './quest-name.component.html',
    styleUrls: ['./quest-name.component.scss']
})
export class QuestNameComponent implements OnInit {

    @Input()
    quest: Quest;

    constructor() { }

    ngOnInit() {
    }
}
