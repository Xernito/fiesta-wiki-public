import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuestPreviewComponent } from './quest-preview.component';

describe('QuestComponent', () => {
  let component: QuestPreviewComponent;
  let fixture: ComponentFixture<QuestPreviewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
