package com.fiestawiki.items.dao.iteminfoserver;

import com.fiestawiki.items.dao.info.ItemInfo;
import com.fiestawiki.items.dao.itemdropgroup.ItemDropGroup;
import com.fiestawiki.items.dao.randomoption.RandomOption;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "item_info_server")
@EntityListeners(AuditingEntityListener.class)
public class ItemInfoServer implements Serializable {

    @Id
    @Column(name = "unique_id", nullable = false)
    private Long id;

    @OneToOne(mappedBy = "serverInfo")
    private ItemInfo itemInfo;

    @Column(name = "inxname")
    private String inxname;

    @Column(name = "marketindex")
    private String marketIndex;

    @Column(name = "city")
    private short city;

    @Column(name = "dropgroupa")
    private String dropGroupA;

    @Column(name = "dropgroupb")
    private String dropGroupB;

    @ManyToMany
    @JoinTable(
            name = "item_info_drop_group_join",
            joinColumns = @JoinColumn(name = "pk1"),
            inverseJoinColumns = @JoinColumn(name = "pk2"))
    private List<ItemDropGroup> dropGroups = new ArrayList<>();

    @Column(name = "randomoptiondropgroup")
    private String randomOptionDropGroup;

    @ManyToMany
    @JoinTable(
            name = "item_info_server_random_option_join",
            joinColumns = @JoinColumn(name = "pk1"),
            inverseJoinColumns = @JoinColumn(name = "pk2"))
    private List<RandomOption> randomStats = new ArrayList<>();

    @Column(name = "vanish")
    private Long vanish;

    @Column(name = "looting")
    private Long looting;

    @Column(name = "dropratekilledbymob")
    private int dropRateKilledByPlayer;

    @Column(name = "iset_index")
    private Long isetIndex;

    @Column(name = "itemsort_index")
    private String itemSortIndex;

    @Column(name = "kqitem")
    private short KQItem;

    @Column(name = "pk_kq_use")
    private short pkKqUse;

    @Column(name = "kq_item_drop")
    private short kqItemDrop;

    @Column(name = "preventattack")
    private short preventAttack;

    @Column(name = "valueunsure")
    private Boolean valueUnsure;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ItemInfo getItemInfo() { return itemInfo; }

    public void setItemInfo(ItemInfo itemInfo) { this.itemInfo = itemInfo; }

    public String getInxname() {
        return inxname;
    }

    public void setInxname(String inxname) {
        this.inxname = inxname;
    }

    public String getMarketIndex() {
        return marketIndex;
    }

    public void setMarketIndex(String marketIndex) {
        this.marketIndex = marketIndex;
    }

    public short getCity() {
        return city;
    }

    public void setCity(short city) {
        this.city = city;
    }

    public String getDropGroupA() {
        return dropGroupA;
    }

    public void setDropGroupA(String dropGroupA) {
        this.dropGroupA = dropGroupA;
    }

    public String getDropGroupB() {
        return dropGroupB;
    }

    public void setDropGroupB(String dropGroupB) {
        this.dropGroupB = dropGroupB;
    }

    public List<ItemDropGroup> getDropGroups() { return dropGroups; }

    public void setDropGroups(List<ItemDropGroup> dropGroups) { this.dropGroups = dropGroups; }

    public List<RandomOption> getRandomStats() {
        return this.randomStats;
    }

    public void setRandomStats(List<RandomOption> randomStats) {
        this.randomStats = randomStats;
    }

    public Long getVanish() {
        return vanish;
    }

    public void setVanish(Long vanish) {
        this.vanish = vanish;
    }

    public Long getLooting() {
        return looting;
    }

    public void setLooting(Long looting) {
        this.looting = looting;
    }

    public int getDropRateKilledByPlayer() {
        return dropRateKilledByPlayer;
    }

    public void setDropRateKilledByPlayer(int dropRateKilledByPlayer) {
        this.dropRateKilledByPlayer = dropRateKilledByPlayer;
    }

    public Long getIsetIndex() {
        return isetIndex;
    }

    public void setIsetIndex(Long isetIndex) {
        this.isetIndex = isetIndex;
    }

    public String getItemSortIndex() {
        return itemSortIndex;
    }

    public void setItemSortIndex(String itemSortIndex) {
        this.itemSortIndex = itemSortIndex;
    }

    public short getKQItem() {
        return KQItem;
    }

    public void setKQItem(short KQItem) {
        this.KQItem = KQItem;
    }

    public short getPkKqUse() {
        return pkKqUse;
    }

    public void setPkKqUse(short pkKqUse) {
        this.pkKqUse = pkKqUse;
    }

    public short getKqItemDrop() {
        return kqItemDrop;
    }

    public void setKqItemDrop(short kqItemDrop) {
        this.kqItemDrop = kqItemDrop;
    }

    public short getPreventAttack() {
        return preventAttack;
    }

    public void setPreventAttack(short preventAttack) {
        this.preventAttack = preventAttack;
    }

    public Boolean getValueUnsure() { return valueUnsure; }

    public void setValueUnsure(Boolean valueUnsure) { this.valueUnsure = valueUnsure; }
}
