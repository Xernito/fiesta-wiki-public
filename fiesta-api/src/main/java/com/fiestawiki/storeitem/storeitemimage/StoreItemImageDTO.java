package com.fiestawiki.storeitem.storeitemimage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StoreItemImageDTO
{
    @JsonProperty("id")
    private Long id;

    @JsonProperty("filename")
    private String filename;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
