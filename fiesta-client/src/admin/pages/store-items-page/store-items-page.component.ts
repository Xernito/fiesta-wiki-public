import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PageDescriptionService } from '@services/page-description.service';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { StoreItemView, StoreItem, StoreCategory } from '@fiesta-common/dto/store-items/store-item';
import { StoreItemService, StoreItemSearch } from '@fiesta-common/services/store-item.service';
import { StoreItemAdminService } from 'admin/services/store-item-admin.service';
import { environment } from '@environments/environment';
import { ItemCategoryEditionDialogComponent } from './item-category-edition-dialog/item-category-edition-dialog.component';
import { StoreItemViewEditionDialogComponent } from './store-item-view-edition-dialog/store-item-view-edition-dialog.component';
import { StoreItemEditionDialogComponent } from './store-item-edition-dialog/store-item-edition-dialog.component';
import { Page } from '@fiesta-common/dto/items/item-info-page';
import { StoreItemSetEditionDialogComponent } from './store-item-set-edition-dialog/store-item-set-edition-dialog.component';
import { LoadingStatus } from '@enums/loading-return';

@Component({
    selector: 'app-store-items-page',
    templateUrl: './store-items-page.component.html',
    styleUrls: ['./store-items-page.component.scss']
})
export class StoreItemsPageComponent implements OnInit {

    @Input()
    pageObs: Observable<Page<StoreItemView>>;

    page$: Observable<Page<StoreItemView>>;

    page: Page<StoreItemView>;

    items: StoreItemView[];
    categories: StoreCategory[];

    STATIC_ASSETS = environment.static_assets;

    loadingErr: LoadingStatus;

    private dialogSize = { width: '600px', height: '700px' };

    constructor(
        protected router: Router,
        protected route: ActivatedRoute,
        protected pageDescriptionService: PageDescriptionService,
        private storeItemService: StoreItemService,
        private storeItemAdminService: StoreItemAdminService,
        private dialog: MatDialog
    ) {
    }

    ngOnInit() {

        this.pageDescriptionService.setPageDescription({
            title: $localize`Premium Items`,
        });

        this.page$ = this.route.paramMap.pipe(
            switchMap(params => {

                let page = 0;

                if (params.has('page')) {
                    page = +params.get('page');
                }

                return this.storeItemService.getStoreItemViews(page, new StoreItemSearch());
            }));

        this.page$.subscribe(
            page => {
                this.page = page;
                this.items = page.content;
                this.loadingErr = 'ok';
            },
            err => {
                this.loadingErr = 'error';
            });

        this.storeItemService.getStoreCategories().subscribe(
            categories => {
                this.categories = categories.sort((c1, c2) => c1.type - c2.type);
            });
    }

    onPageChange(pageEvent: PageEvent) {
        this.router.navigate(['/admin/premium', pageEvent.pageIndex], { queryParamsHandling: 'merge' });
    }

    onCreateStoreItemView() {
        this.dialog.open(StoreItemViewEditionDialogComponent, {
            height: this.dialogSize.height,
            width: this.dialogSize.width,
        })
            .afterClosed()
            .subscribe(item => {
                if (item) {
                    this.storeItemAdminService.createStoreItemView(item)
                        .subscribe(
                            newItem => {
                                this.items.push(newItem);
                            }
                        );
                }
            });
    }

    onEditStoreItemView(view: StoreItemView) {
        this.dialog.open(StoreItemViewEditionDialogComponent, {
            height: this.dialogSize.height,
            width: this.dialogSize.width,
            data: {
                mode: 'edit',
                item: view
            }
        })
            .afterClosed()
            .subscribe(item => {
                if (item) {
                    this.storeItemAdminService.updateStoreItemView(item)
                        .subscribe(
                            newItem => {
                                const index = this.items.indexOf(view, 0);
                                if (index > -1) {
                                    this.items[index] = newItem;
                                }
                            }
                        );
                }
            });
    }

    onDeleteStoreItemView(view: StoreItemView) {

        this.storeItemAdminService.deleteStoreItemView(view.id).subscribe(ok => {
            if (ok) {
                const index = this.items.indexOf(view, 0);
                if (index > -1) {
                    this.items.splice(index, 1);
                }
            }
        });
    }

    onAddStoreItem(view: StoreItemView) {
        this.dialog.open(StoreItemEditionDialogComponent, {
            height: this.dialogSize.height,
            width: this.dialogSize.width,
        })
            .afterClosed()
            .subscribe(item => {
                if (item) {
                    this.storeItemAdminService.createStoreItem(item, view)
                        .subscribe(
                            newItem => {
                                view.store_items.push(newItem);
                            }
                        );
                }
            });
    }

    onCreateStoreItemSet(view: StoreItemView) {
        this.dialog.open(StoreItemSetEditionDialogComponent, {
            height: this.dialogSize.height,
            width: this.dialogSize.width,
            data: {
                view
            }
        })
            .afterClosed()
            .subscribe(data => {
                if (data) {
                    this.storeItemAdminService.createStoreItemSet(data.set)
                        .subscribe(newSet => {

                            for (const item of data.items) {
                                this.storeItemAdminService.addStoreItemToSet(item, newSet).subscribe(
                                    newItem => {
                                        const index = view.store_items.findIndex((storeItem) => storeItem.id === newItem.id);
                                        if (index > -1) {
                                            view.store_items[index] = newItem;
                                        }
                                    });
                            }
                        });
                }
            });
    }

    onEditStoreItem(storeItem: StoreItem, view: StoreItemView) {
        this.dialog.open(StoreItemEditionDialogComponent, {
            height: this.dialogSize.height,
            width: this.dialogSize.width,
            data: {
                mode: 'edit',
                item: storeItem
            }
        })
            .afterClosed()
            .subscribe(item => {
                if (item) {
                    this.storeItemAdminService.updateStoreItem(item)
                        .subscribe(
                            updatedItem => {
                                const index = view.store_items.indexOf(storeItem, 0);
                                if (index > -1) {
                                    view.store_items[index] = updatedItem;
                                }
                            }
                        );
                }
            });
    }

    onDeleteStoreItem(storeItem: StoreItem, view: StoreItemView) {
        this.storeItemAdminService.deleteStoreItem(storeItem.id).subscribe(ok => {
            if (ok) {
                const index = view.store_items.indexOf(storeItem, 0);
                if (index > -1) {
                    view.store_items.splice(index, 1);
                }
            }
        });
    }

    onCreateItemCategory() {
        this.dialog.open(ItemCategoryEditionDialogComponent, {
            height: this.dialogSize.height,
            width: this.dialogSize.width,
        })
            .afterClosed()
            .subscribe(category => {
                if (category) {
                    this.storeItemAdminService.createStoreCategory(category)
                        .subscribe(
                            newCategory => {
                                this.categories.push(newCategory);
                            }
                        );
                }
            });
    }

    onEditItemCategory(storeCategory: StoreCategory) {
        this.dialog.open(ItemCategoryEditionDialogComponent, {
            height: this.dialogSize.height,
            width: this.dialogSize.width,
            data: {
                mode: 'edit',
                category: storeCategory
            }
        })
            .afterClosed()
            .subscribe(category => {
                if (category) {
                    this.storeItemAdminService.updateStoreCategory(category)
                        .subscribe(
                            updatedCategory => {
                                const index = this.categories.indexOf(storeCategory, 0);
                                if (index > -1) {
                                    this.categories[index] = updatedCategory;
                                }
                            }
                        );
                }
            });
    }

    onDeleteItemCategory(storeCategory: StoreCategory) {
        this.storeItemAdminService.deleteStoreCategory(storeCategory.id).subscribe(ok => {
            if (ok) {
                const index = this.categories.indexOf(storeCategory, 0);
                if (index > -1) {
                    this.categories.splice(index, 1);
                }
            }
        });
    }
}
