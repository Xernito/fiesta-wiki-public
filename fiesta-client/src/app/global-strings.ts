export const PAGES_NAME = {
    quests: $localize`Quests`,
    items: $localize`Items`,
    weapons: $localize`Weapons`,
    shields: $localize`Shields`,
    fighterShield: $localize`Fighter Shield`,
    clericShield: $localize`Cleric Shield`,
    gears: $localize`Gears`,
    jewels: $localize`Jewels`,
    rings: $localize`Rings`,
    earrings: $localize`Earrings`,
    necklace: $localize`Necklace`,
    bracelets: $localize`Bracelets`,
    talismans: $localize`Talismans`,
    premiumItems: $localize`Premium Items`,
    skills: $localize`Skills`,
    alchemy: $localize`Alchemy`,
    stones: $localize`Stones`,
    potions: $localize`Potions`,
    scrolls: $localize`Scrolls`,
    composition: $localize`Composition`,
    decomposition: $localize`Decomposition`,
    titles: $localize`Titles`,
    cards: $localize`Cards`,
    collectionCards: $localize`Collection Cards`,
    luckyCapsule: $localize`Lucky Capsule`,
    mobs: $localize`Mobs`,
    licenses: $localize`Licenses`,
}

export const WEAPONS_NAME = {
    oneHandSword: $localize`One Hand Sword`,
    twoHandSword: $localize`Two Hand Sword`,
    axe: $localize`Axe`,

    mace: $localize`Mace`,
    hammer: $localize`Hammer`,

    bow: $localize`Bow`,
    crossbow: $localize`Crossbow`,

    staff: $localize`Staff`,
    wand: $localize`Wand`,

    claws: $localize`Claws`,
    dualSword: $localize`Dual Sword`,

    blade: $localize`Blade`,
}

export const CLASSES_NAME = {
    fighter: $localize`Fighter`,
    cleverFighter: $localize`CleverFighter`,
    warrior: $localize`Warrior`,
    gladiator: $localize`Gladiator`,
    knight: $localize`Knight`,

    cleric: $localize`Cleric`,
    highCleric: $localize`High Cleric`,
    paladin: $localize`Paladin`,
    guardian: $localize`Guardian`,
    holyKnight: $localize`Holy Knight`,

    archer: $localize`Archer`,
    hawkArcher: $localize`HawkArcher`,
    scout: $localize`Scout`,
    ranger: $localize`Ranger`,
    sharpShooter: $localize`SharpShooter`,

    mage: $localize`Mage`,
    wizMage: $localize`WizMage`,
    enchanter: $localize`Enchanter`,
    wizard: $localize`Wizard`,
    warlock: $localize`Warlock`,

    trickster: $localize`Trickster`,
    gambit: $localize`Gambit`,
    renegade: $localize`Renegade`,
    reaper: $localize`Reaper`,
    spectre: $localize`Spectre`,

    crusader: $localize`Crusader`,
    templar: $localize`Templar`,
}