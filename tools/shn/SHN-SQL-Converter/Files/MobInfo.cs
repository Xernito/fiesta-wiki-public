﻿using iQuest;
using System.Data;
using System.IO;
using System.Linq;

namespace SHN_SQL_Converter.Files
{
    class MobInfo : SHNFileBase
    {
        public override void Load(string input_folder)
        {
            SHNFile shn = new SHNFile(false);

            LoadingData loading_data = shn.LoadFile(Path.Combine(input_folder, Filename));
            shn.ReadColumns(loading_data);

            Table = shn.table;
            Table.Namespace = Namespace;
            Table.TableName = Tablename;

            Table.Columns["ID"].DataType = typeof(uint);
            Table.Columns["InxName"].Unique = true;

            shn.ReadRows(loading_data);
        }

        public override void Patch(string patch_folder)
        {
            /////////////////////////////////////////////////////////////////////////////////
            // Remove if there is duplicate mobs
            {
                for (int i = Table.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow mob_info = Table.Rows[i];

                    DataRow[] selectedRows =
                       Table
                            .AsEnumerable()
                            .Where(mob_info_2 =>
                            {
                                return mob_info["InxName"].Equals(mob_info_2["InxName"]);
                            })
                            .ToArray();

                    if (selectedRows.Count() > 1)
                    {
                        for (int row_index = 1; row_index < selectedRows.Count(); row_index++)
                        {
                            Table.Rows.Remove(selectedRows[row_index]);
                        }
                    }
                }
            }
            /////////////////////////////////////////////////////////////////////////////////

            DataColumn[] primary_keys = new DataColumn[1];
            primary_keys[0] = Table.Columns["ID"];
            Table.PrimaryKey = primary_keys;

            Table.Columns["Name"].ColumnName = "NameID";

            DataColumn location_column = new DataColumn("location", typeof(uint))
            {
                DefaultValue = 0
            };

            DataColumn active_column = new DataColumn("active", typeof(bool))
            {
                DefaultValue = false
            };

            DataColumn name_column = new DataColumn("name", typeof(string))
            {
                DefaultValue = ""
            };

            Table.Columns.Add(location_column);
            Table.Columns.Add(active_column);
            Table.Columns.Add(name_column);

            for (int i = 0; i < Table.Rows.Count ; i++)
            {
                DataRow mob_info = Table.Rows[i];

                DataRow[] selectedRows =
                   DataSet.Tables["mob_loca"]
                        .AsEnumerable()
                        .Where(mob_loca =>
                        {
                            return mob_loca["Identifier"].Equals(mob_info["NameID"]);
                        })
                        .ToArray();

                if (selectedRows.Count() == 1)
                {
                    for (int row_index = 0; row_index < selectedRows.Count(); row_index++)
                    {
                        mob_info["name"] = selectedRows[row_index]["Translation"];
                    }
                }
                else
                {
                    throw new System.Exception("duplicated row");
                }
            }
        }
    }
}
