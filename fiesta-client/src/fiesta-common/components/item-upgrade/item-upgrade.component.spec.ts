import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemUpgradeComponent } from './item-upgrade.component';

describe('ItemUpgradeComponent', () => {
  let component: ItemUpgradeComponent;
  let fixture: ComponentFixture<ItemUpgradeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemUpgradeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemUpgradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
