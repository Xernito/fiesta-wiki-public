import { PipeTransform, Pipe } from '@angular/core';


/*
0 : Enemy
1 : Self
2 : Party
3 : Self / Friendly
4 : Position
5 : Throw Projectile
6 : Mob Self / Friendly
7 : Enemy User (PvP)
8 : Self / Friendly (Not displayed)
18 : Teleportation
*/

@Pipe({ name: 'target' })
export class TargetPipe implements PipeTransform {
    transform(target: number): string {
        switch (target) {
            case 0:
                return 'Enemy';
            case 1:
                    return 'Self';
            case 2:
                return 'Party';
            case 3:
                return 'Self / Friendly';
            case 4:
                return 'Position';
            case 5:
                return 'Throw Projectile';
            case 6:
                return 'Mob Self / Friendly';
            case 7:
                return 'Enemy User (PvP)';
            case 8:
                return 'Self / Friendly';
            case 18:
                return 'Teleportation';
            default:
                return 'Not Defined';
        }
    }
}
