import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PassiveSkillIconComponent } from './passive-skill-icon.component';

describe('PassiveSkillIconComponent', () => {
  let component: PassiveSkillIconComponent;
  let fixture: ComponentFixture<PassiveSkillIconComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PassiveSkillIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PassiveSkillIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
