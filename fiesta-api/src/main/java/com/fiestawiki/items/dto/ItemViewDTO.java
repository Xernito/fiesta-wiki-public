package com.fiestawiki.items.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ItemViewDTO {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("icon_index")
    private int iconIndex;

    @JsonProperty("icon_file")
    private String iconFile;

    @JsonProperty("sub_icon_index")
    private int subIconIndex;

    @JsonProperty("sub_icon_file")
    private String subIconFile;

    @JsonProperty("period_icon_index")
    private int periodIconIndex;

    @JsonProperty("period_icon_file")
    private String periodIconFile;

    @JsonProperty("border_color")
    private String borderColor;

    @JsonProperty("background_color")
    private String backgroundColor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getIconIndex() {
        return iconIndex;
    }

    public void setIconIndex(int iconIndex) {
        this.iconIndex = iconIndex;
    }

    public String getIconFile() {
        return iconFile.toLowerCase();
    }

    public void setIconFile(String iconFile) {
        this.iconFile = iconFile;
    }

    public int getSubIconIndex() { return subIconIndex; }

    public void setSubIconIndex(int subIconIndex) { this.subIconIndex = subIconIndex; }

    public String getSubIconFile() { return subIconFile == null ? null : subIconFile.toLowerCase(); }

    public void setSubIconFile(String subIconFile) { this.subIconFile = subIconFile; }

    public int getPeriodIconIndex() { return periodIconIndex; }

    public void setPeriodIconIndex(int periodIconIndex) { this.periodIconIndex = periodIconIndex; }

    public String getPeriodIconFile() { return periodIconFile == null ? null : periodIconFile.toLowerCase();}

    public void setPeriodIconFile(String periodIconFile) { this.periodIconFile = periodIconFile; }

    public String getBorderColor() { return borderColor; }

    public void setBorderColor(String borderColor) { this.borderColor = borderColor; }

    public String getBackgroundColor() { return backgroundColor; }

    public void setBackgroundColor(String backgroundColor) { this.backgroundColor = backgroundColor; }
}
