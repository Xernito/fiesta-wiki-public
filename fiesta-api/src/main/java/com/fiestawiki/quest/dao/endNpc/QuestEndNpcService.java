package com.fiestawiki.quest.dao.endNpc;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class QuestEndNpcService {

    @Autowired
    private ModelMapper modelMapper;

    @PostConstruct
    public void postConstruct()
    {
        ///
        TypeMap<QuestEndNpc, QuestMobKillDTO> typeMap =
                modelMapper.createTypeMap(QuestEndNpc.class, QuestMobKillDTO.class);

        typeMap.addMappings( mapper -> {
            mapper.map(QuestEndNpc::getCount, QuestMobKillDTO::setKillCount);
        });
    }

    public QuestMobKillDTO convertQuestMobKillDTO(QuestEndNpc questEndNpc) {
        return modelMapper.map(questEndNpc, QuestMobKillDTO.class);
    }
}
