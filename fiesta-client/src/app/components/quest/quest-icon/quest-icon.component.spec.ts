import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuestIconComponent } from './quest-icon.component';

describe('QuestIconComponent', () => {
  let component: QuestIconComponent;
  let fixture: ComponentFixture<QuestIconComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
