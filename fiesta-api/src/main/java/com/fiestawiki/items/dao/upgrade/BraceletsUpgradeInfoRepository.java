package com.fiestawiki.items.dao.upgrade;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BraceletsUpgradeInfoRepository extends JpaRepository<BraceletsUpgradeInfo, Long> {
}
