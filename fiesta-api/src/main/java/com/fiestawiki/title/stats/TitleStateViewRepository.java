package com.fiestawiki.title.stats;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TitleStateViewRepository extends JpaRepository<TitleStateView, Long> {
}
