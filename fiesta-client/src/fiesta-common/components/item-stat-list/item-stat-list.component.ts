import { Component, OnInit, Input } from '@angular/core';
import { FiestaSettingsService } from '@fiesta-common/services/fiesta-settings.service';
import { ItemInfo } from '@fiesta-common/dto/items/item';
import { environment } from '@environments/environment';

@Component({
   selector: 'app-item-stat-list',
   templateUrl: './item-stat-list.component.html',
   styleUrls: ['./item-stat-list.component.scss']
})
export class ItemStatListComponent implements OnInit {

   readonly environment = environment;

   @Input()
   item_info: ItemInfo;

   @Input()
   enhancement = 0;

   showMaxStats = false;

   constructor(private fiestaSettings: FiestaSettingsService) { }

   ngOnInit() {
      this.fiestaSettings.showRandomStats.subscribe( value => this.showMaxStats = value );
   }

}
