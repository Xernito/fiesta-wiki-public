import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AbStateComponent } from './ab-state.component';

describe('AbStateComponent', () => {
  let component: AbStateComponent;
  let fixture: ComponentFixture<AbStateComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AbStateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
