import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { Quest } from '@fiesta-common/dto/quests/quest';
import { ActivatedRoute } from '@angular/router';
import { ViewportScroller } from '@angular/common';

@Component({
    selector: 'app-quest',
    templateUrl: './quest-preview.component.html',
    styleUrls: ['./quest-preview.component.scss']
})
export class QuestPreviewComponent implements OnInit, AfterViewInit {

    @Input()
    quest: Quest;

    @Input()
    xpRequired: number;

    infoVisible = false;

    frag: string = null;

    constructor(
        private route: ActivatedRoute,
        private scroller: ViewportScroller
    ) {
    }

    ngOnInit() {
        this.scroller.setOffset([0, 90]);

        this.route.fragment.subscribe(
            frag => {
                if (this.quest.id === +frag) {
                    this.infoVisible = true;
                    this.frag = frag;
                }
            }
        );
    }

    ngAfterViewInit() {
        if (this.frag) {
            setTimeout(() => {
                this.scroller.scrollToAnchor(this.frag);
            }, 1000);
        }
    }

    getQuestClass(type: number) {
        switch (type) {
            case 3:
                return 'event';
            case 5:
                return 'party';
            case 6:
                return 'epic';
            case 7:
                return 'party-epic';
            case 8:
                return 'expedition';
            default:
                return 'default-quest';
        }
    }
}
