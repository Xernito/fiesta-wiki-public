import { Component, ViewChild, ElementRef, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EmscriptenWasmDirective } from './emscripten-wasm.component';
import { environment } from '@environments/environment';

const requestFullscreen =
    document.documentElement.requestFullscreen ||
    document.documentElement['webkitRequestFullscreen'] ||
    document.documentElement['msRequestFullscreen'] ||
    document.documentElement['mozRequestFullScreen'];

@Component({
    templateUrl: './nifviewer-page.component.html',
    styleUrls: ['./nifviewer-page.component.scss'],
})
export class NifviewerPageComponent extends EmscriptenWasmDirective {

    @ViewChild('canvas') canvas: ElementRef;
    error: string;
    supportsFullscreen: boolean;
    file: string;

    constructor(private httpClient: HttpClient, private ngZone: NgZone) {
        super('Nifviewer', 'NifViewer.js');

        this.supportsFullscreen = !!requestFullscreen;

        this.moduleDecorator = (mod) => {
            mod.arguments = [];
            mod.preRun = [
                () => {
                },
            ];
            mod.postRun = [
                () => {
                    mod.fetchnif('http://lost-temple.xyz:7772/reschar/Slime/Slime.nif');
                }
            ];
            mod.canvas = this.canvas.nativeElement as HTMLCanvasElement;
            mod.printErr = (what: string) => {
                if (!what.startsWith('WARNING')) {
                    this.ngZone.run(() => (this.error = what));
                }
            };
        };
    }

    toggleFullscreen() {
        if (requestFullscreen) {
            requestFullscreen.bind(this.canvas.nativeElement)();
        }
    }

    fetch(newValue) {
        this.module.fetchnif(`http://lost-temple.xyz:7772/reschar/${newValue}/${newValue}.nif`);
    }
}
