import { Pipe, PipeTransform } from '@angular/core';
import { CLASSES_NAME } from '@app/global-strings';

/*
1 : All
2 : Fighter
3 : CleverFighter
4 : Warrior
5 : Knight & Gladiator
6 : Gladiator
7 : Knight
8 : Cleric
9 : High Cleric
10 : Paladin
11 : Guardian
12 : Holy Knight
13 : Guardian & Holy Knight
14 : Archer
15 : HawkArcher
16 : Scout
17 : Ranger
18 : SharpShooter
19 : Ranger & SharpShooter
20 : Mage
21 : WizMage
22 : Enchanter
23 : Wizard
24 : Warlock
25 : Wizard & Warlock
26 : All ?
27 : Trickster
28 : Gambit
29 : Renegade
30 : Reaper
31 : Spectre
32 : Reaper & Spectre
33 : Crusader
34 : Templar
35 : Warrior & Scout
36 : Paladin & Warrior
37 : All
38 : Knight, Guardian & HolyKnight
*/

enum UseClass {
   All = 1,
   Fighter = 2,
   CleverFighter = 3,
   Warrior = 4,
   KnightAndGladiator = 5,
   Gladiator = 6,
   Knight = 7,
   Cleric = 8,
   HighCleric = 9,
   Paladin = 10,
   Guardian = 11,
   HolyKnight = 12,
   GuardianAndHolyKnight = 13,
   Archer = 14,
   HawkArcher = 15,
   Scout = 16,
   Ranger = 17,
   SharpShooter = 18,
   RangerAndSharpShooter = 19,
   Mage = 20,
   WizMage = 21,
   Enchanter = 22,
   Wizard = 23,
   Warlock = 24,
   WizardAndWarlock = 25,
   AllButCrus = 26, // SP Ext
   Trickster = 27,
   Gambit = 28,
   Renegade = 29,
   Reaper = 30,
   Spectre = 31,
   ReaperAndSpectre = 32,
   Crusader = 33,
   Templar = 34,
   WarriorAndScout = 35,
   PaladinAndWarrior = 36,
   PrestigeClass = 37, // Class change item
   KnightAndGuardianAndHolyKnight = 38
};

export interface ClassInfo {
   name: string;
   compatible: UseClass[];
}

export const classes = new Map<UseClass, ClassInfo>([
   [UseClass.All, { name: "All", compatible: [] }],

   [UseClass.Fighter, {
      name: "Fighter", compatible: [
         UseClass.Fighter
      ]
   }],
   [UseClass.CleverFighter, {
      name: "CleverFighter", compatible: [
         UseClass.Fighter,
         UseClass.CleverFighter
      ]
   }],
   [UseClass.Warrior, {
      name: "Warrior", compatible: [
         UseClass.Fighter,
         UseClass.CleverFighter,
         UseClass.Warrior
      ]
   }],
   [UseClass.Gladiator, {
      name: "Gladiator", compatible: [
         UseClass.Fighter,
         UseClass.CleverFighter,
         UseClass.Warrior,
         UseClass.Gladiator,
         UseClass.KnightAndGladiator
      ]
   }],
   [UseClass.Knight, {
      name: "Knight", compatible: [
         UseClass.Fighter,
         UseClass.CleverFighter,
         UseClass.Warrior,
         UseClass.Knight,
         UseClass.KnightAndGladiator
      ]
   }],
   [UseClass.KnightAndGladiator, {
      name: `${CLASSES_NAME.knight} & ${CLASSES_NAME.gladiator}`, compatible: [
         UseClass.Fighter,
         UseClass.CleverFighter,
         UseClass.Warrior,
         UseClass.KnightAndGladiator,
         UseClass.Gladiator,
         UseClass.Knight
      ]
   }],

   [UseClass.Cleric, {
      name: "Cleric", compatible: [
         UseClass.Cleric
      ]
   }],
   [UseClass.HighCleric, {
      name: "HighCleric", compatible: [
         UseClass.Cleric,
         UseClass.HighCleric
      ]
   }],
   [UseClass.Paladin, {
      name: "Paladin", compatible: [
         UseClass.Cleric,
         UseClass.HighCleric,
         UseClass.Paladin
      ]
   }],
   [UseClass.HolyKnight, {
      name: "HolyKnight", compatible: [
         UseClass.Cleric,
         UseClass.HighCleric,
         UseClass.Paladin,
         UseClass.HolyKnight,
         UseClass.GuardianAndHolyKnight
      ]
   }],
   [UseClass.Guardian, {
      name: "Guardian", compatible: [
         UseClass.Cleric,
         UseClass.HighCleric,
         UseClass.Paladin,
         UseClass.Guardian,
         UseClass.GuardianAndHolyKnight
      ]
   }],
   [UseClass.GuardianAndHolyKnight, {
      name: "Guardian & Holy Knight", compatible: [
         UseClass.Cleric,
         UseClass.HighCleric,
         UseClass.Paladin,
         UseClass.GuardianAndHolyKnight,
         UseClass.HolyKnight,
         UseClass.Guardian
      ]
   }],

   [UseClass.Archer, {
      name: "Archer", compatible: [
         UseClass.Archer
      ]
   }],
   [UseClass.HawkArcher, {
      name: "HawkArcher", compatible: [
         UseClass.Archer,
         UseClass.HawkArcher
      ]
   }],
   [UseClass.Scout, {
      name: "Scout", compatible: [
         UseClass.Archer,
         UseClass.HawkArcher,
         UseClass.Scout
      ]
   }],
   [UseClass.Ranger, {
      name: "Ranger", compatible: [
         UseClass.Archer,
         UseClass.HawkArcher,
         UseClass.Scout,
         UseClass.Ranger,
         UseClass.RangerAndSharpShooter
      ]
   }],
   [UseClass.SharpShooter, {
      name: "SharpShooter", compatible: [
         UseClass.Archer,
         UseClass.HawkArcher,
         UseClass.Scout,
         UseClass.SharpShooter,
         UseClass.RangerAndSharpShooter
      ]
   }],
   [UseClass.RangerAndSharpShooter, {
      name: "Ranger & SharpShooter", compatible: [
         UseClass.Archer,
         UseClass.HawkArcher,
         UseClass.Scout,
         UseClass.RangerAndSharpShooter,
         UseClass.Ranger,
         UseClass.SharpShooter
      ]
   }],

   [UseClass.Mage, {
      name: "Mage", compatible: [
         UseClass.Mage
      ]
   }],
   [UseClass.WizMage, {
      name: "WizMage", compatible: [
         UseClass.Mage,
         UseClass.WizMage
      ]
   }],
   [UseClass.Enchanter, {
      name: "Enchanter", compatible: [
         UseClass.Mage,
         UseClass.WizMage,
         UseClass.Enchanter
      ]
   }],
   [UseClass.Wizard, {
      name: "Wizard", compatible: [
         UseClass.Mage,
         UseClass.WizMage,
         UseClass.Enchanter,
         UseClass.Wizard,
         UseClass.WizardAndWarlock
      ]
   }],
   [UseClass.Warlock, {
      name: "Warlock", compatible: [
         UseClass.Mage,
         UseClass.WizMage,
         UseClass.Enchanter,
         UseClass.Warlock,
         UseClass.WizardAndWarlock
      ]
   }],
   [UseClass.WizardAndWarlock, {
      name: "Wizard & Warlock", compatible: [
         UseClass.Mage,
         UseClass.WizMage,
         UseClass.Enchanter,
         UseClass.WizardAndWarlock,
         UseClass.Wizard,
         UseClass.Warlock
      ]
   }],

   [UseClass.Trickster, {
      name: "Trickster", compatible: [
         UseClass.Trickster
      ]
   }],
   [UseClass.Gambit, {
      name: "Gambit", compatible: [
         UseClass.Trickster,
         UseClass.Gambit
      ]
   }],
   [UseClass.Renegade, {
      name: "Renegade", compatible: [
         UseClass.Trickster,
         UseClass.Gambit,
         UseClass.Renegade
      ]
   }],
   [UseClass.Reaper, {
      name: "Reaper", compatible: [
         UseClass.Trickster,
         UseClass.Gambit,
         UseClass.Renegade,
         UseClass.Reaper,
         UseClass.ReaperAndSpectre
      ]
   }],
   [UseClass.Spectre, {
      name: "Spectre", compatible: [
         UseClass.Trickster,
         UseClass.Gambit,
         UseClass.Renegade,
         UseClass.Spectre,
         UseClass.ReaperAndSpectre
      ]
   }],
   [UseClass.ReaperAndSpectre, {
      name: "Reaper & Spectre", compatible: [
         UseClass.Trickster,
         UseClass.Gambit,
         UseClass.Renegade,
         UseClass.ReaperAndSpectre,
         UseClass.Reaper,
         UseClass.Spectre
      ]
   }],

   [UseClass.Crusader, {
      name: "Crusader", compatible: [
         UseClass.Crusader
      ]
   }],
   [UseClass.Templar, {
      name: "Templar", compatible: [
         UseClass.Crusader,
         UseClass.Templar
      ]
   }],

   [UseClass.WarriorAndScout, {
      name: "Warrior & Scout", compatible: [
         UseClass.WarriorAndScout,
         UseClass.Warrior,
         UseClass.Scout
      ]
   }],

   [UseClass.PaladinAndWarrior, {
      name: "Paladin & Warrior", compatible: [
         UseClass.PaladinAndWarrior,
         UseClass.Warrior,
         UseClass.Paladin
      ]
   }],

   [UseClass.PrestigeClass, {
      name: "All prestige class", compatible: [
         UseClass.Knight,
         UseClass.Gladiator,
         UseClass.HolyKnight,
         UseClass.Guardian,
         UseClass.SharpShooter,
         UseClass.Ranger,
         UseClass.Warlock,
         UseClass.Wizard,
         UseClass.Reaper,
         UseClass.Spectre,
         UseClass.Templar
      ]
   }],

   [UseClass.KnightAndGuardianAndHolyKnight, {
      name: "Knight, Guardian & HolyKnight", compatible: [
         UseClass.Knight,
         UseClass.HolyKnight,
         UseClass.Guardian
      ]
   }]
]);

@Pipe({ name: 'class' })
export class UseClassPipe implements PipeTransform {

   transform(useClass: number): string {
      if (classes.has(useClass)) {
         return classes.get(useClass).name;
      }
      else {
         return "All";
      }
   }
}

@Pipe({ name: 'compatibleClass' })
export class UseClassCompatiblePipe implements PipeTransform {

   transform(useClass: number, compClass: number): boolean {
      if(useClass == UseClass.All)
      {
         return true;
      }
      else if (classes.has(useClass)) {
         return classes.get(useClass).compatible.find(c => c == compClass) != undefined;
      }

      return false;
   }
}