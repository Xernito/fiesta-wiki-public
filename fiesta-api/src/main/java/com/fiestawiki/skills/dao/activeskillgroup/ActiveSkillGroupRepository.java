package com.fiestawiki.skills.dao.activeskillgroup;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ActiveSkillGroupRepository  extends JpaRepository<ActiveSkillGroup, Long> {
}
