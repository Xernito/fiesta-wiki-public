import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CardDescriptionPageComponent } from './card-description-page.component';

describe('CardDescriptionPageComponent', () => {
  let component: CardDescriptionPageComponent;
  let fixture: ComponentFixture<CardDescriptionPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CardDescriptionPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardDescriptionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
