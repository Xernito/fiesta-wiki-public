import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { QuestLevelComponent } from './quest-level.component';

describe('QuestLevelComponent', () => {
  let component: QuestLevelComponent;
  let fixture: ComponentFixture<QuestLevelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestLevelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestLevelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
