package com.fiestawiki.items;

import com.fiestawiki.items.dao.info.ItemInfo;

import java.util.List;

public class ItemPageDTO {
    private int page_index;

    private int total_pages;

    private List<ItemInfo> content;

    public int getPage_index() {
        return page_index;
    }

    public void setPage_index(int page_index) {
        this.page_index = page_index;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    public List<ItemInfo> getContent() {
        return content;
    }

    public void setContent(List<ItemInfo> content) {
        this.content = content;
    }
}
