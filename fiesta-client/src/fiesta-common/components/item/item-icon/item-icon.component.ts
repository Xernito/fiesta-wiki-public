import { Component, OnInit, Input, HostListener } from '@angular/core';
import { IconComponent } from '@fiesta-common/components/icon/icon.component';
import { environment } from '@environments/environment';
import { TooltipComponent } from 'tooltip/tooltip/tooltip.component';
import { TooltipService } from 'tooltip/tooltip.service';
import { Item } from '@fiesta-common/dto/items/item';
import { ItemTooltipComponent } from '../item-tooltip/item-tooltip.component';

@Component({
    selector: 'app-item-icon',
    templateUrl: './item-icon.component.html',
    styleUrls: ['./item-icon.component.scss']
})
export class ItemIconComponent extends IconComponent implements OnInit {

    STATIC_ASSETS = environment.static_assets + 'icons/';

    private currentItem: Item;
    get item(): Item {
        return this.currentItem;
    }
    @Input()
    set item(i: Item) {
        this.currentItem = i;
        if (this.tooltip) {
            if (this.tooltip.view.value) {
                this.tooltip.view.value.item = i;
            }
        }
    }

    private enhancementPrivate = 0;
    @Input()
    set enhancement(tier: number) {
        this.enhancementPrivate = tier;
        if (this.tooltip) {
            this.tooltip.view.value.enhancement = tier;
        }
    }

    tooltip: TooltipComponent<ItemTooltipComponent>;

    @HostListener('mouseenter', ['$event'])
    mouseEnter(event): void {

        if (!this.tooltip) {
            this.tooltipRef = this.tooltipService.appendComponentToBody(ItemTooltipComponent, event.target);
            this.tooltip = this.tooltipRef.instance;
            this.tooltip.view.subscribe(
                comp => {
                    if (comp) {
                        comp.item = this.item;
                        comp.enhancement = this.enhancementPrivate;
                    }
                });
        }

        this.tooltip.fadeIn();
    }

    @HostListener('mouseleave', ['$event'])
    mouseLeave(event): void {
        this.tooltip.fadeOut(event);
    }

    constructor(
        private tooltipService: TooltipService
    ) {
        super();
    }

    ngOnInit() {
    }
}
