SET db=db_db-dev_1

SET user=docker

SET output=dump

SET schema=store_item

mkdir %output%

docker exec -t %db% pg_dump -s -n %schema% -U %user% > %output%/store_item.sql

docker exec -t %db% pg_dump --column-inserts -t %schema%.store_item_view -U %user% > %output%/z1-store_item_view.sql

docker exec -t %db% pg_dump --column-inserts -t %schema%.store_category -U %user% > %output%/z2-store_category.sql

docker exec -t %db% pg_dump --column-inserts -t %schema%.store_item -U %user% > %output%/z3-store_item.sql

docker exec -t %db% pg_dump --column-inserts -t %schema%.store_item_category -U %user% > %output%/z4-store_item_category.sql

docker exec -t %db% pg_dump --column-inserts -t %schema%.store_item_image -U %user% > %output%/z4-store_item_image.sql