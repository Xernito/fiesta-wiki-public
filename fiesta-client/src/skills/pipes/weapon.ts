import { PipeTransform, Pipe } from '@angular/core';

/*
0 : One Handed Weapon
1 : Two Handed Weapon
2 : Mob ???
3 : Weapon
4 : Hammer
5 : Mace
6 : Nothing
7 : Shield
8 : Bow
9 : Crossbow
10 : Staff
11 : Wand
12 : Claws
13 : Dual Swords
14 : Blade
*/

@Pipe({ name: 'weapon' })
export class WeaponPipe implements PipeTransform {
   transform(weapon: number): string {
      switch (weapon) {
         case 0:
            return 'One Handed Weapon';
         case 1:
            return 'Two Handed Weapon';
         case 2:
            return 'Mob';
         case 3:
            return 'Weapon';
         case 4:
            return 'Hammer';
         case 5:
            return 'Mace';
         case 6:
            return 'Nothing';
         case 7:
            return 'Shield';
         case 8:
            return 'Bow';
         case 9:
            return 'Crossbow';
         case 10:
            return 'Staff';
         case 11:
            return 'Wand';
         case 12:
            return 'Claws';
         case 13:
            return 'Dual Swords';
         case 14:
            return 'Blade';
         default:
            return '';
      }
   }
}

/* In ItemInfo
0: Other
1: Sword
2: Bow
3: Staff
4: Axe
5: Mace
6: -
7: -
8: -
9: -
10: Crossbow
11: Wand
12: -
13: Hammer
14: -
15: Pickaxe / Hoe / Hammer
16: KQ Hammer
17: Dual Sword
18: Claws
19: Blade
20: -
21: Two Handed Sword
*/
