import { Component } from '@angular/core';
import { PAGES_NAME } from '@app/global-strings';
import { ROUTES_URL } from '@app/routing/routes';

@Component({
    selector: 'app-shields-menu',
    templateUrl: './shields-menu.component.html',
    styleUrls: ['./shields-menu.component.scss']
})
export class ShieldsMenuComponent {

    readonly ROUTES = ROUTES_URL;

    readonly PAGES = PAGES_NAME;

    constructor() { }
}
