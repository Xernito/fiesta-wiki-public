import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MapInfo } from '@fiesta-common/dto/maps/map-info';
import { forkJoin, Observable } from 'rxjs';
import { environment } from '@environments/environment';
import { defaultIfEmpty, publishLast, refCount, switchMap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class MapService {

    mapMap = new Map<number, Observable<MapInfo>>();

    constructor(private http: HttpClient) { }

    getMapInfo(mapId: number): Observable<MapInfo> {

        if (this.mapMap.has(mapId)) {
            return this.mapMap.get(mapId);
        }

        const obs$ = this.http.get<MapInfo>(environment.api + 'map/' + mapId).pipe(
            defaultIfEmpty<MapInfo>(),
            publishLast(),
            refCount()
        );

        this.mapMap.set(mapId, obs$);

        return obs$;
    }

    getAllMapInfo(): Observable<MapInfo[]> {

        return this.http.get<number[]>(environment.api + 'maps').pipe(
            switchMap(maps => forkJoin(maps.map(mapID => this.getMapInfo(mapID))))
        );
    }
}
