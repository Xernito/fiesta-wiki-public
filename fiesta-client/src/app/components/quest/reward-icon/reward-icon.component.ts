import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { IconComponent } from '@fiesta-common/components/icon/icon.component';

@Component({
   selector: 'app-reward-icon',
   templateUrl: './reward-icon.component.html',
   styleUrls: ['./reward-icon.component.scss']
})
export class RewardIconComponent extends IconComponent implements OnInit {

   // Exp = 0
   // Money = 1
   // Item = 2
   // Fame = 4
   @Input()
   rewardType: number;

   constructor() { super(); }

   ngOnInit() {
   }

}
