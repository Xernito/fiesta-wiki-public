import { Component, OnInit, Input, ViewChild, TemplateRef } from '@angular/core';

@Component({
    selector: 'app-multi-selection-option',
    templateUrl: './multi-selection-option.component.html',
    styleUrls: ['./multi-selection-option.component.scss']
})
export class MultiSelectionOptionComponent implements OnInit {

    @ViewChild('contentWrapper', { static: true })
    template: TemplateRef<any>;

    @Input()
    value: any;

    checked = false;

    constructor() { }

    ngOnInit(): void {
    }

}
