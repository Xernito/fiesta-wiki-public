package com.fiestawiki.production;

import com.fiestawiki.items.ItemService;
import com.fiestawiki.items.dao.info.ItemInfo;
import com.fiestawiki.items.dto.ItemDTO;
import com.fiestawiki.items.dao.info.repository.ItemInfoRepository;
import org.modelmapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;

@Service
public class ProductionService {

    @Autowired
    private ProductionRepository productionRepository;

    @Autowired
    private ItemInfoRepository itemInfoRepository;

    @Autowired
    private ItemService itemService;

    @Autowired
    private ModelMapper modelMapper;

    @PostConstruct
    public void postConstruct()
    {
        ///
        Converter<String, ItemDTO> getItemInfoFromInxname = new AbstractConverter<String, ItemDTO>() {
            public ItemDTO convert(String itemInxname)
            {
                List<ItemInfo> items = itemInfoRepository.findByItemInxname(itemInxname);

                if(!items.isEmpty())
                {
                    return itemService.convertItemDTO(items.get(0));
                }

                return null;
            }
        };

        ///
        TypeMap<Production, ProductionDTO> typeMap =
                modelMapper.createTypeMap(Production.class, ProductionDTO.class);

        typeMap.addMappings( mapper -> {
            mapper.using(getItemInfoFromInxname).map(src -> src.getRecipe(),
                    ProductionDTO::setRecipe);
            mapper.using(getItemInfoFromInxname).map(src -> src.getProduct(),
                    ProductionDTO::setProduct);
            mapper.using(getItemInfoFromInxname).map(src -> src.getIngredient1(),
                    ProductionDTO::setIngredient1);
            mapper.using(getItemInfoFromInxname).map(src -> src.getIngredient2(),
                    ProductionDTO::setIngredient2);
            mapper.using(getItemInfoFromInxname).map(src -> src.getIngredient3(),
                    ProductionDTO::setIngredient3);
            mapper.using(getItemInfoFromInxname).map(src -> src.getIngredient4(),
                    ProductionDTO::setIngredient4);
        });
    }

    public List<ProductionDTO> getProductions(Long type)
    {
        List<Production> productions = productionRepository.findByType(type);

        Type productionDTOListType = new TypeToken<List<ProductionDTO>>(){}.getType();
        return modelMapper.map(productions, productionDTOListType);
    }
}
