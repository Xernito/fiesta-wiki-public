package com.fiestawiki.mobs.mobinfo.repository;

import com.fiestawiki.mobs.mobinfo.MobInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MobInfoRepository extends JpaRepository<MobInfo, Long>, MobInfoRepositoryCustom {
}
