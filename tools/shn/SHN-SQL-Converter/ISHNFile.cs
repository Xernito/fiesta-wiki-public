﻿using System.Data;

namespace SHN_SQL_Converter
{
    public enum FileType
    {
        Client,
        Server,
        World
    }

    public interface ISHNFile
    {
        public string Filename { get; }
        public string Tablename { get; }
        public DataTable Table { get; }
        public DataSet DataSet { get; set; }
        public FileType Type { get; set; }
        public bool Temporary{ get; set; }

        void Load(string input_folder);
        void Patch(string patch_folder);
        void CreateConstraints();
    }
}
