package com.fiestawiki.items.dao.randomoption;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RandomOptionRepository extends JpaRepository<RandomOption, Long> {
}
