package com.fiestawiki.storeitem.storeitemview;

import com.fiestawiki.storeitem.storeitem.StoreItem;
import com.fiestawiki.storeitem.storeitemimage.StoreItemImage;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(
        name = "store_item_view",
        schema = "store_item"
)
@EntityListeners(AuditingEntityListener.class)
public class StoreItemView {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "icon")
    private String icon;

    @OneToMany(
            orphanRemoval = true,
            cascade = CascadeType.ALL,
            mappedBy = "storeItemView"
    )
    private List<StoreItemImage> imagePreviews = new ArrayList<>();

    @Column(
            columnDefinition = "TEXT",
            name = "description"
    )
    private String description;

    @OneToMany(
            orphanRemoval = true,
            cascade = CascadeType.ALL,
            mappedBy = "storeItemView"
    )
    @OrderBy(value = "id ASC")
    List<StoreItem> storeItems = new ArrayList();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() { return icon; }

    public void setIcon(String icon) { this.icon = icon; }

    public List<StoreItemImage> getImagePreviews() { return imagePreviews; }

    public void setImagePreviews(List<StoreItemImage> imagePreviews)
    {
        this.imagePreviews.clear();
        if(imagePreviews != null)
        {
            this.imagePreviews.addAll(imagePreviews);
        }
    }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public List<StoreItem> getStoreItems() { return storeItems; }

    public void setStoreItems(List<StoreItem> storeItems)
    {
        this.storeItems.clear();
        if(storeItems != null)
        {
            this.storeItems.addAll(storeItems);
        }
    }
}
