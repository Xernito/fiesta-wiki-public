package com.fiestawiki.storeitem;

import com.fiestawiki.storeitem.storecategory.StoreCategory;
import com.fiestawiki.storeitem.storecategory.StoreCategoryDTO;
import com.fiestawiki.storeitem.storecategory.StoreCategoryRepository;
import com.fiestawiki.storeitem.storeitem.StoreItem;
import com.fiestawiki.storeitem.storeitem.StoreItemDTO;
import com.fiestawiki.storeitem.storeitem.StoreItemRepository;
import com.fiestawiki.storeitem.storeitemimage.StoreItemImage;
import com.fiestawiki.storeitem.storeitemimage.StoreItemImageDTO;
import com.fiestawiki.storeitem.storeitemimage.StoreItemImageRepository;
import com.fiestawiki.storeitem.storeitemset.StoreItemSet;
import com.fiestawiki.storeitem.storeitemset.StoreItemSetDTO;
import com.fiestawiki.storeitem.storeitemset.StoreItemSetRepository;
import com.fiestawiki.storeitem.storeitemview.StoreItemView;
import com.fiestawiki.storeitem.storeitemview.StoreItemViewDTO;
import com.fiestawiki.storeitem.storeitemview.StoreItemViewRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;

@Service
public class StoreItemService {

    @Autowired
    StoreItemViewRepository storeItemViewRepository;

    @Autowired
    StoreItemRepository storeItemRepository;

    @Autowired
    StoreCategoryRepository storeCategoryRepository;

    @Autowired
    StoreItemImageRepository storeItemImageRepository;

    @Autowired
    StoreItemSetRepository storeItemSetRepository;

    @Autowired
    private ModelMapper modelMapper;

    @PostConstruct
    public void postConstruct() {

        modelMapper.emptyTypeMap(StoreItemDTO.class, StoreItem.class).addMappings( mapper -> {
            mapper.map(src -> src.getId(), StoreItem::setId);
            mapper.map(src -> src.getItem(), StoreItem::setItem);
            mapper.map(src -> src.getLink(), StoreItem::setLink);
            mapper.map(src -> src.getSc(), StoreItem::setSc);
            mapper.map(src -> src.getDuration(), StoreItem::setDuration);
            mapper.map(src -> src.getCategories(), StoreItem::setCategories);
            mapper.map(src -> src.getSet(), StoreItem::setStoreItemSet);
        });
        modelMapper.emptyTypeMap(StoreItem.class, StoreItemDTO.class).addMappings( mapper -> {
            mapper.map(src -> src.getId(), StoreItemDTO::setId);
            mapper.map(src -> src.getItem(), StoreItemDTO::setItem);
            mapper.map(src -> src.getLink(), StoreItemDTO::setLink);
            mapper.map(src -> src.getSc(), StoreItemDTO::setSc);
            mapper.map(src -> src.getDuration(), StoreItemDTO::setDuration);
            mapper.map(src -> src.getCategories(), StoreItemDTO::setCategories);
            mapper.map(src -> src.getStoreItemSet(), StoreItemDTO::setSet);
        });

        modelMapper.emptyTypeMap(StoreItemSetDTO.class, StoreItemSet.class).addMappings( mapper -> {
            mapper.map(src -> src.getId(), StoreItemSet::setId);
            mapper.map(src -> src.getName(), StoreItemSet::setName);
        });
        modelMapper.emptyTypeMap(StoreItemSet.class, StoreItemSetDTO.class).addMappings( mapper -> {
            mapper.map(src -> src.getId(), StoreItemSetDTO::setId);
            mapper.map(src -> src.getName(), StoreItemSetDTO::setName);
        });
    }

    private List<StoreItemViewDTO> convertAllStoreItemViewDTO(List<StoreItemView> item_list)
    {
        Type listType = new TypeToken<List<StoreItemViewDTO>>(){}.getType();
        return  modelMapper.map(item_list, listType);
    }

    private List<StoreCategoryDTO> convertAllStoreCategoryDTO(List<StoreCategory> category_list)
    {
        Type listType = new TypeToken<List<StoreCategoryDTO>>(){}.getType();
        return  modelMapper.map(category_list, listType);
    }

    private List<StoreItemImage> convertAllStoreItemImage(List<StoreItemImageDTO> store_item_images)
    {
        Type listType = new TypeToken<List<StoreItemImage>>(){}.getType();
        return  modelMapper.map(store_item_images, listType);
    }

    private List<StoreItem> convertAllStoreItem(List<StoreItemDTO> store_item)
    {
        Type listType = new TypeToken<List<StoreItem>>(){}.getType();
        return  modelMapper.map(store_item, listType);
    }

    public Page<StoreItemViewDTO> getStoreItemViewPage(
            int page_index,
            Optional<String> search,
            Optional<List<Long>> filteredStats,
            Optional<List<Long>> filteredSlots,
            Optional<List<Long>> filteredDuration,
            Optional<List<Long>> filteredDesigner
    )
    {
        Pageable item_pageable = PageRequest.of(page_index, 10, Sort.by("name"));
        Page<StoreItemView> items = storeItemViewRepository.filteredItemView(
                search,
                filteredStats,
                filteredSlots,
                filteredDuration,
                filteredDesigner,
                item_pageable);

        List<StoreItemViewDTO> items_dto = convertAllStoreItemViewDTO(items.getContent());

        return new PageImpl<>(items_dto, item_pageable, items.getTotalElements());
    }

    public StoreItemViewDTO getStoreItemView(Long id)
    {
        Optional<StoreItemView> storeItemView = storeItemViewRepository.findById(id);

        if(storeItemView.isPresent())
        {
            return modelMapper.map(storeItemView.get(), StoreItemViewDTO.class);
        }

        return null;
    }

    public Long addStoreItemView(StoreItemViewDTO itemViewDTO)
    {
        StoreItemView itemView = modelMapper.map(itemViewDTO, StoreItemView.class);

        for(StoreItemImage img : itemView.getImagePreviews())
        {
            img.setStoreItemView(itemView);
        }

        Long newId = storeItemViewRepository.save(itemView).getId();

        return newId;
    }

    public StoreItemViewDTO updateStoreItemView(StoreItemViewDTO itemViewDTO)
    {
        Optional<StoreItemView> i = storeItemViewRepository.findById(itemViewDTO.getId());
        if(i.isPresent())
        {
            StoreItemView itemView = i.get();

            itemView.setDescription(itemViewDTO.getDescription());
            itemView.setIcon(itemViewDTO.getIcon());

            List<StoreItemImage> image_preview = convertAllStoreItemImage(itemViewDTO.getImagePreviews());
            for(StoreItemImage img : image_preview)
            {
                img.setStoreItemView(itemView);
            }
            itemView.setImagePreviews(image_preview);

            itemView.setName(itemViewDTO.getName());

            List<StoreItem> store_items = convertAllStoreItem(itemViewDTO.getStoreItems());
            for(StoreItem item : store_items)
            {
                item.setStoreItemView(itemView);
            }
            itemView.setStoreItems(store_items);

            Long id = storeItemViewRepository.save(itemView).getId();

            Optional<StoreItemView> newStoreItemView = storeItemViewRepository.findById(id);

            if(newStoreItemView.isPresent()) {
                return modelMapper.map(newStoreItemView.get(), StoreItemViewDTO.class);
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }

    public Boolean deleteStoreItemView(Long id)
    {
        Optional<StoreItemView> i = storeItemViewRepository.findById(id);

        if(i.isPresent())
        {
            storeItemViewRepository.deleteById(i.get().getId());

            return true;
        }

        return false;
    }

    public StoreItemDTO getStoreItem(Long id)
    {
        Optional<StoreItem> storeItem = storeItemRepository.findById(id);

        if(storeItem.isPresent())
        {
            return modelMapper.map(storeItem.get(), StoreItemDTO.class);
        }

        return null;
    }

    public StoreItemSetDTO getStoreItemSet(Long id)
    {
        Optional<StoreItemSet> storeItemSet = storeItemSetRepository.findById(id);

        if(storeItemSet.isPresent())
        {
            return modelMapper.map(storeItemSet.get(), StoreItemSetDTO.class);
        }

        return null;
    }

    public Long addStoreItem(StoreItemDTO itemDTO, Long view)
    {
        Optional<StoreItemView> storeItemView = storeItemViewRepository.findById(view);

        if(storeItemView.isPresent())
        {
            StoreItem item = modelMapper.map(itemDTO, StoreItem.class);

            item.setStoreItemView(storeItemView.get());

            return storeItemRepository.save(item).getId();
        }

        return 0L;
    }

    public Long addStoreItemToSet(StoreItemDTO itemDTO, Long set) throws Exception {
        Optional<StoreItemSet> storeItemSet = storeItemSetRepository.findById(set);
        Optional<StoreItem> i = storeItemRepository.findById(itemDTO.getId());

        if(!i.isPresent())
            throw new Exception("store item don't exist");
        if(!storeItemSet.isPresent())
            throw new Exception("store item set don't exist");

        if(storeItemSet.isPresent() && i.isPresent())
        {
            StoreItemSet itemSet = storeItemSet.get();
            StoreItem item = i.get();

            if(item.getStoreItemSet() == null)
            {
                item.setStoreItemSet(itemSet);

                return storeItemRepository.save(item).getId();
            }
        }

        return 0L;
    }

    public StoreItemDTO updateStoreItem(StoreItemDTO itemDTO)
    {
        Optional<StoreItem> i = storeItemRepository.findById(itemDTO.getId());
        if(i.isPresent())
        {
            StoreItem item = i.get();
            StoreItem updateItem = modelMapper.map(itemDTO, StoreItem.class);

            item.setItem(updateItem.getItem());
            item.setLink(updateItem.getLink());
            item.setSc(updateItem.getSc());
            item.setDuration(updateItem.getDuration());
            item.setCategories(updateItem.getCategories());

            StoreItemSet currentSet = item.getStoreItemSet();

            item.setStoreItemSet(updateItem.getStoreItemSet());
            storeItemRepository.saveAndFlush(item);

            if(currentSet != null && updateItem.getStoreItemSet() == null)
            {
                if(currentSet.getStoreItems().size() == 0)
                {
                    storeItemSetRepository.deleteById(currentSet.getId());
                }
            }

            return itemDTO;
        }

        return null;
    }

    public Boolean deleteStoreItem(Long id)
    {
        Optional<StoreItem> i = storeItemRepository.findById(id);

        if(i.isPresent())
        {
            StoreItem item = i.get();

            if(item.getStoreItemSet() != null)
            {
                StoreItemSet set = item.getStoreItemSet();

                if(set.getStoreItems().size() <= 1)
                {
                    storeItemSetRepository.deleteById(set.getId());
                }
            }

            storeItemRepository.deleteById(item.getId());

            return true;
        }

        return false;
    }

    public Long addStoreItemSet(StoreItemSetDTO itemSetDTO) throws Exception
    {
        StoreItemSet item = modelMapper.map(itemSetDTO, StoreItemSet.class);

        Long id = storeItemSetRepository.saveAndFlush(item).getId();

        return id;
    }

    public List<StoreCategoryDTO> getStoreCategories() {

        List<StoreCategory> categories = storeCategoryRepository.findAll();

        return convertAllStoreCategoryDTO(categories);
    }

    public StoreCategoryDTO getStoreCategory(Long id)
    {
        Optional<StoreCategory> category = storeCategoryRepository.findById(id);

        return modelMapper.map(category.get(), StoreCategoryDTO.class);
    }

    public Long addStoreCategory(StoreCategoryDTO categoryDTO)
    {
        StoreCategory category = modelMapper.map(categoryDTO, StoreCategory.class);

        return storeCategoryRepository.save(category).getId();
    }

    public StoreCategoryDTO updateStoreCategory(StoreCategoryDTO categoryDTO)
    {
        Optional<StoreCategory> i = storeCategoryRepository.findById(categoryDTO.getId());
        if(i.isPresent())
        {
            StoreCategory category = i.get();
            StoreCategory updateItem = modelMapper.map(categoryDTO, StoreCategory.class);

            category.setName(updateItem.getName());
            category.setType(updateItem.getType());

            storeCategoryRepository.save(category);

            return modelMapper.map(category, StoreCategoryDTO.class);
        }

        return null;
    }

    public Boolean deleteStoreCategory(Long id)
    {
        Optional<StoreCategory> c = storeCategoryRepository.findById(id);

        if(c.isPresent())
        {
            storeCategoryRepository.deleteById(c.get().getId());

            return true;
        }

        return false;
    }
}
