

export interface AbStateView {
   id: number;
   inxname: string;
   icon: number;
   icon_file: string;
   description: string;
   border_color: string;
}

export interface AbState {
   id: number;
   inxname: string;
   view: AbStateView;
   sub_ab_state: SubAbState;
   party_ab_state: AbState[];
   index: number;
   keep_time_ratio: number;
   keep_time_power: number;
   grade: number;
   dispel_index: number;
   sub_dispel_index: number;
   save_type: number;
   main_state_index: string;
   stackable: number;
}

export interface PassiveAbState {
   id: number;
   condition: number;
   condition_rate: number;
   ab_states: AbState[];
}

export interface SubAbStateEffect {
   effect_id: number;
   effect_value: number;
}

export interface SubAbState {
   id: number;
   inxname: string;
   strength: number;
   type: number;
   sub_type: number;
   duration: number;
   effects: SubAbStateEffect[];
}
