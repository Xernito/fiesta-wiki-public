import { AfterViewInit, Directive } from '@angular/core';
import { loadScript } from './tools';
import { environment } from '@environments/environment';

type EmscriptenModuleDecorator<M extends EmscriptenModule> = (module: M) => void;

const noopModuleDecorator = (mod: EmscriptenModule) => mod;

@Directive()
export abstract class EmscriptenWasmDirective<M extends EmscriptenModule = EmscriptenModule> implements AfterViewInit {

    private resolvedModule: M;

    protected moduleDecorator: EmscriptenModuleDecorator<M>;

    readonly STATIC_ASSETS = environment.static_assets;

    protected constructor(private moduleExportName: string, private wasmJavaScriptLoader: string) { }

    get module(): M {
        return this.resolvedModule;
    }

    ngAfterViewInit(): void {
        this.resolveModule();
    }

    protected resolveModule(): void {
        loadScript(this.moduleExportName, `${environment.common.wasmAssetsPath}/${this.wasmJavaScriptLoader}`)
            .then(() => {
                const module = {
                    locateFile: (file: string) => {
                        return `${environment.common.wasmAssetsPath}/${file}`;
                    },
                };
                const moduleDecorator: EmscriptenModuleDecorator<M> = this.moduleDecorator || noopModuleDecorator;
                moduleDecorator(module as M);

                return window[this.moduleExportName](module);
            })
            .then((mod) => {
                this.resolvedModule = mod;
            });
    }
}
