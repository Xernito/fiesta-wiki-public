package com.fiestawiki.quest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fiestawiki.quest.dao.endItem.QuestEndItemDTO;
import com.fiestawiki.quest.dao.endNpc.QuestMobKillDTO;
import com.fiestawiki.quest.dao.reward.ItemRewardDTO;

import java.util.ArrayList;
import java.util.List;

public class QuestDataDTO
{
    @JsonProperty("id")
    private Long id;

    @JsonProperty("quest_type")
    private int questType;

    @JsonProperty("title")
    private String title;

    @JsonProperty("description")
    private String description;

    @JsonProperty("type")
    private int type;

    @JsonProperty("min_level")
    private int minLevel;

    @JsonProperty("max_level")
    private int maxLevel;

    @JsonProperty("starting_npc_id")
    private Long startingNpcId;

    @JsonProperty("end_npc_id")
    private Long endNpcId;

    @JsonProperty("starting_item_id")
    private Long startingItemId;

    @JsonProperty("starting_item_quantity")
    private Long startingItemQuantity;

    @JsonProperty("exp")
    private Long exp;

    @JsonProperty("money")
    private Long money;

    @JsonProperty("fame")
    private Long fame;

    @JsonProperty("item_rewards")
    private List<ItemRewardDTO> itemRewards = new ArrayList<>();

    @JsonProperty("selectable_item_rewards")
    private List<ItemRewardDTO> selectableItemRewards = new ArrayList<>();

    @JsonProperty("start_class")
    private int startClass;

    @JsonProperty("previous_quest")
    private QuestDTO previousQuest;

    @JsonProperty("next_quests")
    private List<QuestDTO> nextQuests = new ArrayList<>();

    @JsonProperty("mob_kill")
    private List<QuestMobKillDTO> mobKill = new ArrayList<>();

    @JsonProperty("item_gathering")
    private List<QuestEndItemDTO> itemGathering = new ArrayList<>();

    @JsonProperty("repeat")
    private boolean repeat;

    @JsonProperty("remote_turn_in")
    private boolean remoteTurnIn;

    @JsonProperty("remote_pick_up")
    private boolean remotePickUp;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getQuestType() {
        return questType;
    }

    public void setQuestType(int questType) {
        this.questType = questType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getType() { return type; }

    public void setType(int type) { this.type = type; }

    public int getMinLevel() {
        return minLevel;
    }

    public void setMinLevel(int minLevel) {
        this.minLevel = minLevel;
    }

    public int getMaxLevel() {
        return maxLevel;
    }

    public void setMaxLevel(int maxLevel) {
        this.maxLevel = maxLevel;
    }

    public Long getStartingNpcId() { return startingNpcId; }

    public void setStartingNpcId(Long startingNpcId) { this.startingNpcId = startingNpcId; }

    public Long getEndNpcId() { return endNpcId; }

    public void setEndNpcId(Long endNpcId) { this.endNpcId = endNpcId; }

    public Long getStartingItemId() { return startingItemId; }

    public void setStartingItemId(Long startingItemId) { this.startingItemId = startingItemId; }

    public Long getStartingItemQuantity() { return startingItemQuantity; }

    public void setStartingItemQuantity(Long startingItemQuantity) { this.startingItemQuantity = startingItemQuantity; }

    public Long getExp() {
        return exp;
    }

    public void setExp(Long exp) {
        this.exp = exp;
    }

    public Long getMoney() {
        return money;
    }

    public void setMoney(Long money) {
        this.money = money;
    }

    public Long getFame() {
        return fame;
    }

    public void setFame(Long fame) {
        this.fame = fame;
    }

    public List<ItemRewardDTO> getItemRewards() {
        return itemRewards;
    }

    public void setItemRewards(List<ItemRewardDTO> itemRewards) {
        this.itemRewards = itemRewards;
    }

    public List<ItemRewardDTO> getSelectableItemRewards() { return selectableItemRewards; }

    public void setSelectableItemRewards(List<ItemRewardDTO> selectableItemRewards) { this.selectableItemRewards = selectableItemRewards; }

    public int getStartClass() { return startClass; }

    public void setStartClass(int startClass) { this.startClass = startClass; }

    public QuestDTO getPreviousQuest() { return previousQuest; }

    public void setPreviousQuest(QuestDTO previousQuest) { this.previousQuest = previousQuest; }

    public List<QuestDTO> getNextQuests() { return nextQuests; }

    public void setNextQuests(List<QuestDTO> nextQuests) { this.nextQuests = nextQuests; }

    public List<QuestMobKillDTO> getMobKill() { return mobKill; }

    public void setMobKill(List<QuestMobKillDTO> mobKill) { this.mobKill = mobKill; }

    public List<QuestEndItemDTO> getItemGathering() { return itemGathering; }

    public void setItemGathering(List<QuestEndItemDTO> itemGathering) { this.itemGathering = itemGathering; }

    public boolean getRepeat() { return repeat; }

    public void setRepeat(boolean repeat) { this.repeat = repeat; }

    public boolean getRemoteTurnIn() { return remoteTurnIn; }

    public void setRemoteTurnIn(boolean remoteTurnIn) { this.remoteTurnIn = remoteTurnIn; }

    public boolean getRemotePickUp() { return remotePickUp; }

    public void setRemotePickUp(boolean remotePickUp) { this.remotePickUp = remotePickUp; }
}
