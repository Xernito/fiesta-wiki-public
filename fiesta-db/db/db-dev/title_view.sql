CREATE TABLE title_view
(
  type bigint NOT NULL,
  unlock_type int,
  obtainable int,
  description character varying(255)
);

\copy title_view FROM '/docker-entrypoint-initdb.d/csv/title_view.csv' DELIMITER ',' CSV HEADER;