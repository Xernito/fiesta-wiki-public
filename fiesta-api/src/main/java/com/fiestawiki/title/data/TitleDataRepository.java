package com.fiestawiki.title.data;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TitleDataRepository extends JpaRepository<TitleData, Long> {
}
