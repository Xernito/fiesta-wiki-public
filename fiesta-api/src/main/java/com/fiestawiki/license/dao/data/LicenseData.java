package com.fiestawiki.license.dao.data;

import com.fiestawiki.mobs.mobinfo.MobInfo;
import com.fiestawiki.mobs.mobspecies.MobSpecies;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "license_data")
@EntityListeners(AuditingEntityListener.class)
public class LicenseData
{
    @Id
    @Column(name = "unique_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "mobid")
    private MobInfo mob;

    @Column(name = "level")
    private short level;

    @Column(name = "prefix")
    private String prefix;

    @Column(name = "suffix")
    private String suffix;

    @Column(name = "mobkillcount")
    private Long mobKillCount;

    @Column(name = "minadd")
    private Long minAdd;

    @Column(name = "maxadd")
    private Long maxAdd;

    @Column(name = "sp1_reference")
    private short reference;

    @Column(name = "sp1_type")
    private short type;

    @Column(name = "sp1_value")
    private Long value;

    @ManyToOne
    @JoinColumn(name = "species_id")
    private MobSpecies species;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MobInfo getMob() {
        return mob;
    }

    public void setMob(MobInfo mob) {
        this.mob = mob;
    }

    public short getLevel() {
        return level;
    }

    public void setLevel(short level) {
        this.level = level;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public Long getMobKillCount() {
        return mobKillCount;
    }

    public void setMobKillCount(Long mobKillCount) {
        this.mobKillCount = mobKillCount;
    }

    public Long getMinAdd() { return minAdd - 1000; }

    public void setMinAdd(Long minAdd) {
        this.minAdd = minAdd;
    }

    public Long getMaxAdd() {
        return maxAdd - 1000;
    }

    public void setMaxAdd(Long maxAdd) {
        this.maxAdd = maxAdd;
    }

    public short getReference() {
        return reference;
    }

    public void setReference(short reference) {
        this.reference = reference;
    }

    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public MobSpecies getSpecies() { return species; }

    public void setSpecies(MobSpecies species) { this.species = species; }
}
