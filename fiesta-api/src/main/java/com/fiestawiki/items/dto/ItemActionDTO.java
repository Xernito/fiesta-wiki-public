package com.fiestawiki.items.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fiestawiki.items.dao.itemactioncondition.ItemActionConditionDTO;
import com.fiestawiki.items.dao.itemactioneffect.ItemActionEffectDTO;

public class ItemActionDTO
{
    @JsonProperty("item_action_id")
    private int itemActionId;

    @JsonProperty("condition")
    private ItemActionConditionDTO condition;

    @JsonProperty("effect")
    private ItemActionEffectDTO effect;

    @JsonProperty("cooltime")
    private int cooltime;

    @JsonProperty("cooltime_group_action_id")
    private int cooltimeGroupActionId;

    public int getItemActionId() {
        return itemActionId;
    }

    public void setItemActionId(int itemActionId) {
        this.itemActionId = itemActionId;
    }

    public ItemActionConditionDTO getCondition() { return condition; }

    public void setCondition(ItemActionConditionDTO condition) { this.condition = condition; }

    public ItemActionEffectDTO getEffect() {
        return effect;
    }

    public void setEffect(ItemActionEffectDTO effect) {
        this.effect = effect;
    }

    public int getCooltime() {
        return cooltime;
    }

    public void setCooltime(int cooltime) {
        this.cooltime = cooltime;
    }

    public int getCooltimeGroupActionId() {
        return cooltimeGroupActionId;
    }

    public void setCooltimeGroupActionId(int cooltimeGroupActionId) { this.cooltimeGroupActionId = cooltimeGroupActionId; }
}
