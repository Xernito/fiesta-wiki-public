import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingStatus } from '@enums/loading-return';
import { Page } from '@fiesta-common/dto/items/item-info-page';
import { License } from 'license/dto/licenses';
import { PageDescriptionService } from '@services/page-description.service';
import { ROUTES_URL } from '@app/routing/routes';
import { LicensesService } from 'license/services/licenses.service';
import { switchMap } from 'rxjs/operators';

@Component({
    selector: 'app-license-page',
    templateUrl: './license-page.component.html',
    styleUrls: ['./license-page.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({ height: '0px', minHeight: '0', borderBottomWidth: '0px' })),
            state('expanded', style({ height: '*', borderBottomWidth: '1px' })),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ]),
    ],
})
export class LicensePageComponent implements OnInit {

    readonly ROUTES = ROUTES_URL;

    page = new Page<License>();

    dataSource: MatTableDataSource<License>;

    displayedColumns: string[] =
        [
            'icon',
            'name',
            'killCount',
            'damage',
            'crit'
        ];

    loadingErr: LoadingStatus = 'loading';

    expanded_element: License | null;

    pageSizeOptions: number[] = [10, 20, 50, 100];

    readonly defaultPageSize = 20;

    constructor(
        protected router: Router,
        protected route: ActivatedRoute,
        protected pageDescriptionService: PageDescriptionService,
        private licenseService: LicensesService
    ) { }

    ngOnInit(): void {
        this.pageDescriptionService.setPageDescription({
            title: $localize`Licenses`,
            description: 'Licenses can be put on weapons and increase damage and crit rate to its mob.'
        });

        this.route.queryParamMap.pipe(
            switchMap(queryParam => {

                let page = 0;
                let size = this.defaultPageSize;

                if (queryParam.has('page')) {
                    page = +queryParam.get('page');
                }

                if (queryParam.has('size')) {
                    size = +queryParam.get('size');
                }

                return this.licenseService.getLicensePage({ page, size, sort: ['level'], active: true });
            })
        ).subscribe(
            page => {
                this.loadingErr = 'ok';
                this.page = page;
                this.dataSource = new MatTableDataSource(page.content);
            },
            err => {
                this.loadingErr = 'error';
            });
    }

    onPageChange(pageEvent: PageEvent) {
        this.router.navigate(
            ['/', ROUTES_URL.licenses],
            {
                queryParams:
                {
                    ...this.route.snapshot.queryParams,
                    ...{
                        page: pageEvent.pageIndex !== 0 ? pageEvent.pageIndex : undefined,
                        size: pageEvent.pageSize !== this.defaultPageSize ? pageEvent.pageSize : undefined
                    }
                },
                queryParamsHandling: 'merge'
            });
    }

}
