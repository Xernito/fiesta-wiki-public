package com.fiestawiki.items.dao.info.repository;

import com.fiestawiki.items.dao.info.ItemInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;

import java.util.List;
import java.util.Optional;

public interface ItemInfoRepositoryCustom {

    Page<ItemInfo> findJewels(Long jewelType, Optional<List<Long>> rarities, Optional<String> search, @PageableDefault(page = 0, value = Integer.MAX_VALUE) Pageable pageable, Optional<Long> min_level, Optional<Long> max_level);

    Page<ItemInfo> findTalismans(Optional<String> search, @PageableDefault(page = 0, value = Integer.MAX_VALUE) Pageable pageable, Optional<Long> min_level, Optional<Long> max_level);

    Page<ItemInfo> findBracelets(Optional<String> search, @PageableDefault(page = 0, value = Integer.MAX_VALUE) Pageable pageable, Optional<Long> min_level, Optional<Long> max_level);

    Page<ItemInfo> findGears(Long theClass, Optional<List<Long>> rarities, Optional<String> search, @PageableDefault(page = 0, value = Integer.MAX_VALUE) Pageable pageable, Optional<Long> min_level, Optional<Long> max_level);

    Page<ItemInfo> findShields(Long shieldType, Optional<List<Long>> rarities, Optional<String> search, @PageableDefault(page = 0, value = Integer.MAX_VALUE) Pageable pageable, Optional<Long> min_level, Optional<Long> max_level);

    Page<ItemInfo> findWeapons(Long weaponType, Optional<List<Long>> rarities, Optional<String> search, @PageableDefault(page = 0, value = Integer.MAX_VALUE) Pageable pageable, Optional<Long> min_level, Optional<Long> max_level);

    Page<ItemInfo> findPremium(@PageableDefault(page = 0, value = Integer.MAX_VALUE) Pageable pageable);

    List<ItemInfo> findPremiumByName(String search);
}
