import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SessionSettingsService } from './session-settings.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [SessionSettingsService]
})
export class SessionSettingsModule { }
