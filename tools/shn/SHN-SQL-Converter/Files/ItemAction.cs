﻿
namespace SHN_SQL_Converter.Files
{
    class ItemAction : SHNFileBase
    {
        public override void CreateConstraints()
        {
            Utils.AddForeignKeyColumn(
                DataSet,
                Table.TableName, new string[] { "EffectID" },
                "item_action_effect", new string[] { "EffectID" },
                "item_action_effect_id");

            Utils.AddForeignKey(
                DataSet,
                Tablename, "conditionid",
                "item_action_condition",
                "action_condition_fk");
        }
    }
}
