package com.fiestawiki.items.dao.itemactioneffect;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ItemActionEffectDTO
{
    @JsonProperty("effect_id")
    private int effectId;

    @JsonProperty("effect_target")
    private List<Integer> effectTarget;

    @JsonProperty("effect_activity")
    private List<Integer> effectActivity;

    @JsonProperty("value")
    private int value;

    @JsonProperty("area")
    private int area;

    public int getEffectId() {
        return effectId;
    }

    public void setEffectId(int effectId) {
        this.effectId = effectId;
    }

    public List<Integer> getEffectTarget() {
        return effectTarget;
    }

    public void setEffectTarget(List<Integer> effectTarget) {
        this.effectTarget = effectTarget;
    }

    public List<Integer> getEffectActivity() {
        return effectActivity;
    }

    public void setEffectActivity(List<Integer> effectActivity) {
        this.effectActivity = effectActivity;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }
}
