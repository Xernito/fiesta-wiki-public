#!/bin/bash

inputPath="$1/resmenu"

outputPath="./icons"

if [ ! -d "$outputPath" ]; then
	mkdir icons
fi

if [ ! -d "${outputPath}/mob-portraits" ]; then
	mkdir ${outputPath}/mob-portraits
fi

mogrify -crop 38x38+0+0 +repage -format webp -path "${outputPath}/mob-portraits" "${inputPath}/game/MobPortrait/*.png" 	

cd "${outputPath}/mob-portraits"

rename -f 'y/A-Z/a-z/' *