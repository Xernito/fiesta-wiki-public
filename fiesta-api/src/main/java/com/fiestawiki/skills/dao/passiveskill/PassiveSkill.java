package com.fiestawiki.skills.dao.passiveskill;

import com.fiestawiki.skills.dao.activeskill.ActiveSkill;
import com.fiestawiki.skills.dao.passiveskillview.PassiveSkillView;
import com.fiestawiki.states.passiveskillabstate.PassiveSkillAbState;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "passive_skill")
@EntityListeners(AuditingEntityListener.class)
public class PassiveSkill implements Serializable {

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "inxname")
    private String inxname;

    @Column(name = "name")
    private String name;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    private PassiveSkillView view;

    @ManyToOne
    @JoinColumn(name = "ab_state_fk")
    private PassiveSkillAbState abState;

    @Column(name = "weaponmastery")
    private Long weaponMastery;

    @Column(name = "demandsk")
    private String previousSkillInxname;

    @OneToOne
    @JoinColumn(name = "previous_skill_fk")
    private PassiveSkill previousSkill;

    @OneToOne(mappedBy = "previousSkill")
    private PassiveSkill nextSkill;

    @Column(name = "mstrtsword1")
    private Long masterySwordDmgRate;

    @Column(name = "mstrthammer1")
    private Long masteryHammerDmgRate;

    @Column(name = "mstrtsword2")
    private Long masteryTwoHandDmgRate;

    @Column(name = "mstrtaxe2")
    private Long masteryAxeDmgRate;

    @Column(name = "mstrtmace1")
    private Long masteryMaceDmgRate;

    @Column(name = "mstrtbow2")
    private Long masteryBowDmgRate;

    @Column(name = "mstrtcrossbow2")
    private Long masteryCrossbowDmgRate;

    @Column(name = "mstrtwand2")
    private Long masteryWandDmgRate;

    @Column(name = "mstrtstaff2")
    private Long masteryStaffDmgRate;

    @Column(name = "mstrtclaw")
    private Long masteryClawDmgRate;

    @Column(name = "mstrtdsword")
    private Long masteryDSwordDmgRate;

    @Column(name = "mstplsword1")
    private Long masterySwordDmg;

    @Column(name = "mstplhammer1")
    private Long masteryHammerDmg;

    @Column(name = "mstplsword2")
    private Long masteryTwoHandDmg;

    @Column(name = "mstplaxe2")
    private Long masteryAxeDmg;

    @Column(name = "mstplmace1")
    private Long masteryMaceDmg;

    @Column(name = "mstplbow2")
    private Long masteryBowDmg;

    @Column(name = "mstplcrossbow2")
    private Long masteryCrossbowDmg;

    @Column(name = "mstplwand2")
    private Long masteryWandDmg;

    @Column(name = "mstplstaff2")
    private Long masteryStaffDmg;

    @Column(name = "mstplclaw")
    private Long masteryClawDmg;

    @Column(name = "mstpldsword")
    private Long masteryDSwordDmg;

    @Column(name = "tb")
    private Long evasion;

    @Column(name = "maxsp")
    private Long SPIncrease;

    @Column(name = "maxlp")
    private Long LPIncrease;

    @Column(name = "wcrateup")
    private Long phyAtkUpRate;

    @Column(name = "marateup")
    private Long magAtkUpRate;

    @Column(name = "hpdowndamegeup")
    private int dmgUpPerHPDown;

    @Column(name = "downdamegehp")
    private int dmgUpPerHPDownRatio;

    @Column(name = "hpdownacup")
    private int defUpPerHPDown;

    @Column(name = "downachp")
    private int defUpPerHPDownRatio;

    @Column(name = "movetbupplus")
    private int movementIncreaseEvasionUp;

    @Column(name = "healuprate")
    private int healUpRate;

    @Column(name = "keeptimebuffuprate")
    private int buffDurationUpRate;

    @Column(name = "cridmguprate")
    private int critDmgUpRate;

    @Column(name = "activeskillgroupinx")
    private Long activeSkillGroupIndex;

    @Column(name = "dmg_minusrate")
    private int dmgReceiveMinusRate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInxname() {
        return inxname;
    }

    public void setInxname(String inxname) {
        this.inxname = inxname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PassiveSkillView getView() {
        return view;
    }

    public void setView(PassiveSkillView view) {
        this.view = view;
    }

    public PassiveSkillAbState getAbState() { return abState; }

    public void setAbState(PassiveSkillAbState abState) { this.abState = abState; }

    public Long getWeaponMastery() {
        return weaponMastery;
    }

    public void setWeaponMastery(Long weaponMastery) {
        this.weaponMastery = weaponMastery;
    }

    public String getPreviousSkillInxname() {
        return previousSkillInxname;
    }

    public void setPreviousSkillInxname(String previousSkillInxname) {
        this.previousSkillInxname = previousSkillInxname;
    }

    public PassiveSkill getPreviousSkill() {
        return previousSkill;
    }

    public void setPreviousSkill(PassiveSkill previousSkill) {
        this.previousSkill = previousSkill;
    }

    public PassiveSkill getNextSkill() { return nextSkill; }

    public void setNextSkill(PassiveSkill nextSkill) { this.nextSkill = nextSkill; }

    public Long getMasterySwordDmgRate() {
        return masterySwordDmgRate;
    }

    public void setMasterySwordDmgRate(Long masterySwordDmgRate) {
        this.masterySwordDmgRate = masterySwordDmgRate;
    }

    public Long getMasteryHammerDmgRate() {
        return masteryHammerDmgRate;
    }

    public void setMasteryHammerDmgRate(Long masteryHammerDmgRate) {
        this.masteryHammerDmgRate = masteryHammerDmgRate;
    }

    public Long getMasteryTwoHandDmgRate() {
        return masteryTwoHandDmgRate;
    }

    public void setMasteryTwoHandDmgRate(Long masteryTwoHandDmgRate) {
        this.masteryTwoHandDmgRate = masteryTwoHandDmgRate;
    }

    public Long getMasteryAxeDmgRate() {
        return masteryAxeDmgRate;
    }

    public void setMasteryAxeDmgRate(Long masteryAxeDmgRate) {
        this.masteryAxeDmgRate = masteryAxeDmgRate;
    }

    public Long getMasteryMaceDmgRate() {
        return masteryMaceDmgRate;
    }

    public void setMasteryMaceDmgRate(Long masteryMaceDmgRate) {
        this.masteryMaceDmgRate = masteryMaceDmgRate;
    }

    public Long getMasteryBowDmgRate() {
        return masteryBowDmgRate;
    }

    public void setMasteryBowDmgRate(Long masteryBowDmgRate) {
        this.masteryBowDmgRate = masteryBowDmgRate;
    }

    public Long getMasteryCrossbowDmgRate() {
        return masteryCrossbowDmgRate;
    }

    public void setMasteryCrossbowDmgRate(Long masteryCrossbowDmgRate) {
        this.masteryCrossbowDmgRate = masteryCrossbowDmgRate;
    }

    public Long getMasteryWandDmgRate() {
        return masteryWandDmgRate;
    }

    public void setMasteryWandDmgRate(Long masteryWandDmgRate) {
        this.masteryWandDmgRate = masteryWandDmgRate;
    }

    public Long getMasteryStaffDmgRate() {
        return masteryStaffDmgRate;
    }

    public void setMasteryStaffDmgRate(Long masteryStaffDmgRate) {
        this.masteryStaffDmgRate = masteryStaffDmgRate;
    }

    public Long getMasteryClawDmgRate() {
        return masteryClawDmgRate;
    }

    public void setMasteryClawDmgRate(Long masteryClawDmgRate) {
        this.masteryClawDmgRate = masteryClawDmgRate;
    }

    public Long getMasteryDSwordDmgRate() {
        return masteryDSwordDmgRate;
    }

    public void setMasteryDSwordDmgRate(Long masteryDSwordDmgRate) {
        this.masteryDSwordDmgRate = masteryDSwordDmgRate;
    }

    public Long getMasterySwordDmg() {
        return masterySwordDmg;
    }

    public void setMasterySwordDmg(Long masterySwordDmg) {
        this.masterySwordDmg = masterySwordDmg;
    }

    public Long getMasteryHammerDmg() {
        return masteryHammerDmg;
    }

    public void setMasteryHammerDmg(Long masteryHammerDmg) {
        this.masteryHammerDmg = masteryHammerDmg;
    }

    public Long getMasteryTwoHandDmg() {
        return masteryTwoHandDmg;
    }

    public void setMasteryTwoHandDmg(Long masteryTwoHandDmg) {
        this.masteryTwoHandDmg = masteryTwoHandDmg;
    }

    public Long getMasteryAxeDmg() {
        return masteryAxeDmg;
    }

    public void setMasteryAxeDmg(Long masteryAxeDmg) {
        this.masteryAxeDmg = masteryAxeDmg;
    }

    public Long getMasteryMaceDmg() {
        return masteryMaceDmg;
    }

    public void setMasteryMaceDmg(Long masteryMaceDmg) {
        this.masteryMaceDmg = masteryMaceDmg;
    }

    public Long getMasteryBowDmg() {
        return masteryBowDmg;
    }

    public void setMasteryBowDmg(Long masteryBowDmg) {
        this.masteryBowDmg = masteryBowDmg;
    }

    public Long getMasteryCrossbowDmg() {
        return masteryCrossbowDmg;
    }

    public void setMasteryCrossbowDmg(Long masteryCrossbowDmg) {
        this.masteryCrossbowDmg = masteryCrossbowDmg;
    }

    public Long getMasteryWandDmg() {
        return masteryWandDmg;
    }

    public void setMasteryWandDmg(Long masteryWandDmg) {
        this.masteryWandDmg = masteryWandDmg;
    }

    public Long getMasteryStaffDmg() {
        return masteryStaffDmg;
    }

    public void setMasteryStaffDmg(Long masteryStaffDmg) {
        this.masteryStaffDmg = masteryStaffDmg;
    }

    public Long getMasteryClawDmg() {
        return masteryClawDmg;
    }

    public void setMasteryClawDmg(Long masteryClawDmg) {
        this.masteryClawDmg = masteryClawDmg;
    }

    public Long getMasteryDSwordDmg() {
        return masteryDSwordDmg;
    }

    public void setMasteryDSwordDmg(Long masteryDSwordDmg) {
        this.masteryDSwordDmg = masteryDSwordDmg;
    }

    public Long getEvasion() {
        return evasion;
    }

    public void setEvasion(Long evasion) {
        this.evasion = evasion;
    }

    public Long getSPIncrease() {
        return SPIncrease;
    }

    public void setSPIncrease(Long SPIncrease) {
        this.SPIncrease = SPIncrease;
    }

    public Long getLPIncrease() {
        return LPIncrease;
    }

    public void setLPIncrease(Long LPIncrease) {
        this.LPIncrease = LPIncrease;
    }

    public Long getPhyAtkUpRate() {
        return phyAtkUpRate;
    }

    public void setPhyAtkUpRate(Long phyAtkUpRate) {
        this.phyAtkUpRate = phyAtkUpRate;
    }

    public Long getMagAtkUpRate() {
        return magAtkUpRate;
    }

    public void setMagAtkUpRate(Long magAtkUpRate) {
        this.magAtkUpRate = magAtkUpRate;
    }

    public int getDmgUpPerHPDown() {
        return dmgUpPerHPDown;
    }

    public void setDmgUpPerHPDown(int dmgUpPerHPDown) {
        this.dmgUpPerHPDown = dmgUpPerHPDown;
    }

    public int getDmgUpPerHPDownRatio() {
        return dmgUpPerHPDownRatio;
    }

    public void setDmgUpPerHPDownRatio(int dmgUpPerHPDownRatio) {
        this.dmgUpPerHPDownRatio = dmgUpPerHPDownRatio;
    }

    public int getDefUpPerHPDown() {
        return defUpPerHPDown;
    }

    public void setDefUpPerHPDown(int defUpPerHPDown) {
        this.defUpPerHPDown = defUpPerHPDown;
    }

    public int getDefUpPerHPDownRatio() {
        return defUpPerHPDownRatio;
    }

    public void setDefUpPerHPDownRatio(int defUpPerHPDownRatio) {
        this.defUpPerHPDownRatio = defUpPerHPDownRatio;
    }

    public int getMovementIncreaseEvasionUp() {
        return movementIncreaseEvasionUp;
    }

    public void setMovementIncreaseEvasionUp(int movementIncreaseEvasionUp) {
        this.movementIncreaseEvasionUp = movementIncreaseEvasionUp;
    }

    public int getHealUpRate() {
        return healUpRate;
    }

    public void setHealUpRate(int healUpRate) {
        this.healUpRate = healUpRate;
    }

    public int getBuffDurationUpRate() {
        return buffDurationUpRate;
    }

    public void setBuffDurationUpRate(int buffDurationUpRate) {
        this.buffDurationUpRate = buffDurationUpRate;
    }

    public int getCritDmgUpRate() {
        return critDmgUpRate;
    }

    public void setCritDmgUpRate(int critDmgUpRate) {
        this.critDmgUpRate = critDmgUpRate;
    }

    public Long getActiveSkillGroupIndex() {
        return activeSkillGroupIndex;
    }

    public void setActiveSkillGroupIndex(Long activeSkillGroupIndex) {
        this.activeSkillGroupIndex = activeSkillGroupIndex;
    }

    public int getDmgReceiveMinusRate() {
        return dmgReceiveMinusRate;
    }

    public void setDmgReceiveMinusRate(int dmgReceiveMinusRate) {
        this.dmgReceiveMinusRate = dmgReceiveMinusRate;
    }

    public String getGroupName() {
        return inxname.replaceAll("(\\d+)$", "");
    }
}
