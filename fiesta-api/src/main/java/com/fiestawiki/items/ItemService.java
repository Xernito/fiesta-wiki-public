package com.fiestawiki.items;

import com.fiestawiki.ClassEnum;
import com.fiestawiki.items.dao.info.ItemInfo;
import com.fiestawiki.items.dao.info.ItemInfo_;
import com.fiestawiki.items.dao.itemaction.ItemAction;
import com.fiestawiki.items.dao.itemactioncondition.ItemActionCondition;
import com.fiestawiki.items.dao.itemset.ItemSet;
import com.fiestawiki.items.dao.seteffect.SetEffect;
import com.fiestawiki.items.dto.*;
import com.fiestawiki.items.dao.info.repository.ItemInfoRepository;
import com.fiestawiki.items.dao.itemaction.ItemActionRepository;
import com.fiestawiki.items.dao.itemactioneffectdesc.ItemActionEffectDescRepository;
import com.fiestawiki.items.dao.iteminfoserver.ItemInfoServer;
import com.fiestawiki.items.dao.seteffect.SetEffectRepository;
import com.fiestawiki.items.dao.itemset.ItemSetRepository;
import com.fiestawiki.items.dao.stats.ItemStatsRepository;
import com.fiestawiki.items.dao.upgrade.*;
import com.fiestawiki.items.dao.itemview.ItemViewInfo;
import com.fiestawiki.items.dao.itemview.ItemViewInfoRepository;
import com.fiestawiki.production.Production;
import com.fiestawiki.production.ProductionRepository;
import com.fiestawiki.skills.dao.activeskillgroup.ActiveSkillGroup;
import org.modelmapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.domain.Sort.*;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ItemService {

    @Autowired
    private ItemInfoRepository itemInfoRepository;

    @Autowired
    private ItemViewInfoRepository itemViewInfoRepository;

    @Autowired
    private SetEffectRepository setEffectRepository;

    @Autowired
    private ItemActionRepository itemActionRepository;

    @Autowired
    private ItemActionEffectDescRepository itemActionEffectDescRepository;

    @Autowired
    private ItemSetRepository itemSetRepository;

    @Autowired
    private ItemStatsRepository itemStatsRepository;

    @Autowired
    private ItemUpgradeInfoRepository itemUpgradeInfoRepository;

    @Autowired
    private ProductionRepository productionRepository;

    @Autowired
    private BraceletsUpgradeInfoRepository braceletsUpgradeInfoRepository;

    @Autowired
    private RandomOptionService randomOptionService;

    @Autowired
    private ModelMapper modelMapper;

    @PostConstruct
    public void postConstruct() {

        modelMapper.emptyTypeMap(ItemInfo.class, ItemDTO.class).addMappings(mapper -> {
           mapper.map(ItemInfo::getId, ItemDTO::setId);
           mapper.map(ItemInfo::getName, ItemDTO::setName);
           mapper.map(ItemInfo::getItemGradeType, ItemDTO::setItemGradeType);
        });

        modelMapper.emptyTypeMap(ItemSet.class, ItemSetDTO.class).addMappings(mapper -> {
            mapper.map(ItemSet::getUniqueId, ItemSetDTO::setId);
            mapper.map(ItemSet::getSetName, ItemSetDTO::setSetName);
            mapper.map(ItemSet::getSetItemsId, ItemSetDTO::setSetItems);
            mapper.map(ItemSet::getItemSetEffectsId, ItemSetDTO::setSetEffects);
        });

        modelMapper.emptyTypeMap(SetEffect.class, SetEffectDTO.class).addMappings(mapper -> {
            mapper.map(SetEffect::getCount, SetEffectDTO::setCount);
            mapper.map(src -> src.getItemSet().getUniqueId(), SetEffectDTO::setItemSet);
            mapper.map(SetEffect::getItemActionsId, SetEffectDTO::setItemActionsId);
            mapper.map(SetEffect::getItemActionEffectDescs, SetEffectDTO::setItemActionEffectDescs);
        });

        ///
        Converter<ItemViewInfo, String> getBorderColor = new AbstractConverter<ItemViewInfo, String>() {
            public String convert(ItemViewInfo src) {

                if(src.getRed() == 0 && src.getGreen() == 0 && src.getBlue() == 0)
                    return "#FFFFFF";

                String out = "#";

                out += String.format("%02X", src.getRed());
                out += String.format("%02X", src.getGreen());
                out += String.format("%02X", src.getBlue());

                return out;
            }
        };

        ///
        Converter<ItemViewInfo, String> getBackgroundColor = new AbstractConverter<ItemViewInfo, String>() {
            public String convert(ItemViewInfo src) {

                if(src.getBackgroundRed() == 0 && src.getBackgroundGreen() == 0 && src.getBackgroundBlue() == 0)
                    return "#00000000";

                String out = "#";

                out += String.format("%02X", src.getBackgroundRed());
                out += String.format("%02X", src.getBackgroundGreen());
                out += String.format("%02X", src.getBackgroundBlue());

                return out;
            }
        };

        ///
        TypeMap<ItemViewInfo, ItemViewDTO> typeMap =
                modelMapper.createTypeMap(ItemViewInfo.class, ItemViewDTO.class);

        typeMap.addMappings( mapper -> {
            mapper.using(getBorderColor).map(src -> src,
                    ItemViewDTO::setBorderColor);
            mapper.using(getBackgroundColor).map(src -> src,
                    ItemViewDTO::setBackgroundColor);
        });

        ///
        Converter<ItemInfo, UpgradeInfoDTO> getUpgradeinfo = new AbstractConverter<ItemInfo, UpgradeInfoDTO>() {
            public UpgradeInfoDTO convert(ItemInfo item)
            {
                if(item == null)
                    return null;

                // Bracelet
                if(item.getItemCategory() == 38) {
                    Optional<BraceletsUpgradeInfo> upgrade = braceletsUpgradeInfoRepository.findById(item.getUpgradeId());

                    if(upgrade.isPresent())
                    {
                        return modelMapper.map(upgrade.get(), UpgradeInfoDTO.class);
                    }
                }
                else {
                    Optional<ItemUpgradeInfo> upgrade = itemUpgradeInfoRepository.findById(item.getUpgradeId());

                    if(upgrade.isPresent())
                    {
                        return modelMapper.map(upgrade.get(), UpgradeInfoDTO.class);
                    }
                }

                return null;
            }
        };
        ///
        Converter<ItemInfo, Long> getProductionSkillNeeded = new AbstractConverter<ItemInfo, Long>() {
            public Long convert(ItemInfo item)
            {
                if(item == null)
                    return 0L;

                Optional<Production> p = productionRepository.findByRecipe(item.getInxname());

                if(p.isPresent())
                {
                    return p.get().getNeededMastery();
                }

                return 0L;
            }
        };

        ///
        TypeMap<ItemInfo, ItemInfoDTO> itemInfoTypeMap =
                modelMapper.createTypeMap(ItemInfo.class, ItemInfoDTO.class);

        itemInfoTypeMap.addMappings( mapper -> {
            mapper.map(src -> src.getUseClass().getId(), ItemInfoDTO::setUseClass);
            mapper.map(src -> src.getItemSetId(), ItemInfoDTO::setItemSetId);
            mapper.using(getUpgradeinfo).map(src -> src,
                    ItemInfoDTO::setUpgradeInfo);
            mapper.using(getProductionSkillNeeded).map(src -> src,
                    ItemInfoDTO::setProductionSkillRequired);
            mapper.skip(ItemInfoDTO::setRandomStats);
            mapper.using(ctx -> {
                ItemInfoServer s = ((ItemInfoServer) ctx.getSource());
                if(s != null) return randomOptionService.convertRandomOptions(s.getRandomStats());
                return new RandomOptionDTO();
            }).map(src -> src.getServerInfo(), ItemInfoDTO::setRandomStats);
            mapper.map(src -> src.getView().getDescription(), ItemInfoDTO::setDescription);
            mapper.map(src -> src.getServerInfo().getValueUnsure(), ItemInfoDTO::setStatValuesUnsure);
            });

        ///
        Converter<ItemUpgradeInfo, List<Long>> itemUpgradeInfoToUpgradeList = new AbstractConverter<ItemUpgradeInfo, List<Long>>() {
            public List<Long> convert(ItemUpgradeInfo upgrade_info)
            {
                List<Long> upgrades = new ArrayList<>();

                upgrades.add(0L); // Corresponding to +0 needed because bracelets starts at +0
                upgrades.add(upgrade_info.getUpgrade1());
                upgrades.add(upgrade_info.getUpgrade2());
                upgrades.add(upgrade_info.getUpgrade3());
                upgrades.add(upgrade_info.getUpgrade4());
                upgrades.add(upgrade_info.getUpgrade5());
                upgrades.add(upgrade_info.getUpgrade6());
                upgrades.add(upgrade_info.getUpgrade7());
                upgrades.add(upgrade_info.getUpgrade8());
                upgrades.add(upgrade_info.getUpgrade9());
                upgrades.add(upgrade_info.getUpgrade10());
                upgrades.add(upgrade_info.getUpgrade11());
                upgrades.add(upgrade_info.getUpgrade12());

                return upgrades;
            }
        };

        ///
        TypeMap<ItemUpgradeInfo, UpgradeInfoDTO> itemUpgradeInfoTypeMap =
                modelMapper.createTypeMap(ItemUpgradeInfo.class, UpgradeInfoDTO.class);

        itemUpgradeInfoTypeMap.addMappings( mapper -> {
            mapper.using(itemUpgradeInfoToUpgradeList).map(src -> src,
                    UpgradeInfoDTO::setUpgrades);
        });

        ///
        Converter<BraceletsUpgradeInfo, List<Long>> braceletsUpgradeInfoToUpgradeList = new AbstractConverter<BraceletsUpgradeInfo, List<Long>>() {
            public List<Long> convert(BraceletsUpgradeInfo upgrade_info)
            {
                List<Long> upgrades = new ArrayList<>();

                upgrades.add(upgrade_info.getUpgrade0());
                upgrades.add(upgrade_info.getUpgrade1());
                upgrades.add(upgrade_info.getUpgrade2());
                upgrades.add(upgrade_info.getUpgrade3());
                upgrades.add(upgrade_info.getUpgrade4());
                upgrades.add(upgrade_info.getUpgrade5());
                upgrades.add(upgrade_info.getUpgrade6());
                upgrades.add(upgrade_info.getUpgrade7());
                upgrades.add(upgrade_info.getUpgrade8());
                upgrades.add(upgrade_info.getUpgrade9());
                upgrades.add(upgrade_info.getUpgrade10());
                upgrades.add(upgrade_info.getUpgrade11());
                upgrades.add(upgrade_info.getUpgrade12());
                upgrades.add(upgrade_info.getUpgrade13());
                upgrades.add(upgrade_info.getUpgrade14());
                upgrades.add(upgrade_info.getUpgrade15());
                upgrades.add(upgrade_info.getUpgrade16());
                upgrades.add(upgrade_info.getUpgrade17());
                upgrades.add(upgrade_info.getUpgrade18());
                upgrades.add(upgrade_info.getUpgrade19());
                upgrades.add(upgrade_info.getUpgrade20());

                return upgrades;
            }
        };

        ///
        TypeMap<BraceletsUpgradeInfo, UpgradeInfoDTO> braceletsUpgradeInfoTypeMap =
                modelMapper.createTypeMap(BraceletsUpgradeInfo.class, UpgradeInfoDTO.class);

        braceletsUpgradeInfoTypeMap.addMappings( mapper -> {
            mapper.using(braceletsUpgradeInfoToUpgradeList).map(src -> src,
                    UpgradeInfoDTO::setUpgrades);
        });
    }

    public ItemDTO convertItemDTO(ItemInfo item_info)
    {
        return modelMapper.map(item_info, ItemDTO.class);
    }

    private ItemViewDTO convertItemViewDTO(ItemViewInfo item_view)
    {
        return modelMapper.map(item_view, ItemViewDTO.class);
    }

    public ItemInfoDTO convertItemInfoDTO(ItemInfo item_info)
    {
        return modelMapper.map(item_info, ItemInfoDTO.class);
    }

    private ItemSetDTO convertItemSetDTO(ItemSet item_set)
    {
        return modelMapper.map(item_set, ItemSetDTO.class);
    }

    private String numColToColName(int col_num) throws Exception {
        switch(col_num)
        {
            case 0:
            case 1:
                return "id";
            case 2:
            case 3:
                return "name";
            case 4:
            case 5:
                return "requiredLevel";
            case 6:
            case 7:
                return "maxPhyAtk";
            case 8:
            case 9:
                return "maxMagAtk";
            case 10:
            case 11:
                return "aim";
            case 12:
            case 13:
                return "criticalRate";
            case 14:
            case 15:
                return "attackSpeed";
            case 16:
            case 17:
                return "buyPrice";
            case 18:
            case 19:
                return "sellPrice";
            case 20:
            case 21:
                return "itemGradeType";
            case 22:
            case 23:
                return "grade";
            case 24:
            case 25:
                return "defense";
            case 26:
            case 27:
                return "magicalDefense";
            case 28:
            case 29:
                return "evasion";
            case 30:
            case 31:
                return "defenseRate";
            case 32:
            case 33:
                return "magicalDefenseRate";
            case 34:
            case 35:
                return "blockRate";
            case 36:
            case 37:
                return "stats.poisonResist";
            case 38:
            case 39:
                return "stats.diseaseResist";
            case 40:
            case 41:
                return "stats.curseResist";
            case 42:
            case 43:
                return "stats.aimRate";
            case 44:
            case 45:
                return "stats.evaRate";
        }

        throw new Exception("Couldn't find column " + col_num);
    }

    public ItemDTO getItem(Long id)
    {
        ItemInfo item_info = itemInfoRepository
                .findByItemId(id)
                .orElseThrow(() -> new EntityNotFoundException());

        return convertItemDTO(item_info);
    }

    public ItemViewDTO getItemView(Long id) throws Exception
    {
        ItemViewInfo item_view_info = itemViewInfoRepository
                .findByItemId(id)
                .orElseThrow(() -> new Exception("not found"));

        return convertItemViewDTO(item_view_info);
    }

    public ItemInfoDTO getItemInfo(Long id) throws Exception
    {
        ItemInfo item_info = itemInfoRepository
                .findByItemId(id)
                .orElseThrow(() -> new EntityNotFoundException());

        return convertItemInfoDTO(item_info);
    }

    public ItemSetDTO getItemSet(Long id)
    {
        ItemSet item_set = itemSetRepository
                .findById(id)
                .orElseThrow(() -> new EntityNotFoundException());

        return convertItemSetDTO(item_set);
    }

    public ItemInfo getItemInfoRaw(Long id) throws Exception {
        return itemInfoRepository
                .findByItemId(id)
                .orElseThrow(() -> new Exception("not found"));
    }

    public List<ItemDTO> findItemByName(String name)
    {
        return itemInfoRepository
                .findPremiumByName(name)
                .stream()
                .map(i -> convertItemDTO(i))
                .collect(Collectors.toList());
    }

    public Page<ItemDTO> getItemPage(int page_index)
    {
        Pageable item_pageable = PageRequest.of(page_index, 50, Sort.by("id"));

        return itemInfoRepository
                .findAll(item_pageable)
                .map(i -> convertItemDTO(i));
    }

    public Page<ItemDTO> getWeaponsPage(
            Long weaponType,
            Optional<List<Long>> rarities,
            Optional<Integer> theOrder,
            Optional<String> search,
            int page_index,
            Optional<Long> min_level,
            Optional<Long> max_level
    ) throws Exception {

        List<Order> orders = new ArrayList<>();

        if(theOrder.isPresent()) {
            Order order = new Order(
                    (theOrder.get() % 2 == 0) ? Sort.Direction.DESC : Sort.Direction.ASC,
                    numColToColName(theOrder.get())
            );
            orders.add(order);
        }
        else
        {
            orders.add(new Sort.Order(Direction.DESC, ItemInfo_.requiredLevel.getName()));
        }

        // By default also order by dmg & rarity in case 2 weapons have same order
        orders.add(new Sort.Order(Direction.DESC, ItemInfo_.maxMagAtk.getName()));
        orders.add(new Sort.Order(Direction.DESC, ItemInfo_.maxPhyAtk.getName()));
        orders.add(new Sort.Order(Direction.DESC, ItemInfo_.itemGradeType.getName()));

        Pageable item_pageable = PageRequest.of(page_index, 50, Sort.by(orders));

        return itemInfoRepository
                .findWeapons(weaponType, rarities, search, item_pageable, min_level, max_level)
                .map(i -> convertItemDTO(i));
    }

    public Page<ItemDTO> getShieldsPage(
            Long shieldType,
            Optional<List<Long>> rarities,
            Optional<Integer> theOrder,
            Optional<String> search,
            int page_index,
            Optional<Long> min_level,
            Optional<Long> max_level
    ) throws Exception {

        List<Order> orders = new ArrayList<>();

        if(theOrder.isPresent()) {
            Order order = new Order(
                    (theOrder.get() % 2 == 0) ? Sort.Direction.DESC : Sort.Direction.ASC,
                    numColToColName(theOrder.get())
            );
            orders.add(order);
        }
        else
        {
            orders.add(new Sort.Order(Direction.DESC, ItemInfo_.requiredLevel.getName()));
        }

        // By default also order by defense & rarity in case 2 shields have same order
        orders.add(new Sort.Order(Direction.DESC, ItemInfo_.defense.getName()));
        orders.add(new Sort.Order(Direction.DESC, ItemInfo_.itemGradeType.getName()));

        Pageable item_pageable = PageRequest.of(page_index, 50, Sort.by(orders));

        return itemInfoRepository
                .findShields(shieldType, rarities, search, item_pageable, min_level, max_level)
                .map(i -> convertItemDTO(i));
    }

    public Page<ItemDTO> getGearsPage(
            Long theClass,
            Optional<List<Long>> rarities,
            Optional<Integer> theOrder,
            Optional<String> search,
            int page_index,
            Optional<Long> min_level,
            Optional<Long> max_level
    ) throws Exception
    {
        List<Order> orders = new ArrayList<>();

        if(theOrder.isPresent()) {
            Order order = new Order(
                    (theOrder.get() % 2 == 0) ? Sort.Direction.DESC : Sort.Direction.ASC,
                    numColToColName(theOrder.get())
            );
            orders.add(order);
        }
        else
        {
            orders.add(new Sort.Order(Direction.DESC, ItemInfo_.requiredLevel.getName()));
        }

        // By default also order by defense & rarity in case 2 shields have same order
        orders.add(new Sort.Order(Direction.DESC, ItemInfo_.itemGradeType.getName()));
        orders.add(new Sort.Order(Direction.DESC, ItemInfo_.defense.getName()));

        Pageable item_pageable = PageRequest.of(page_index, 50, Sort.by(orders));

        return itemInfoRepository
                .findGears(theClass, rarities, search, item_pageable, min_level, max_level)
                .map(i -> convertItemDTO(i));
    }

    public Page<ItemDTO> getJewelsPage(
            Long jewelType,
            Optional<Integer> theOrder,
            Optional<List<Long>> rarities,
            Optional<String> search,
            int page_index,
            Optional<Long> min_level,
            Optional<Long> max_level
    ) throws Exception
    {
        List<Order> orders = new ArrayList<>();

        if(theOrder.isPresent()) {
            Order order = new Order(
                    (theOrder.get() % 2 == 0) ? Sort.Direction.DESC : Sort.Direction.ASC,
                    numColToColName(theOrder.get())
            );
            orders.add(order);
        }
        else
        {
            orders.add(new Sort.Order(Direction.DESC, ItemInfo_.requiredLevel.getName()));
        }

        // By default also order by rarity in case 2 jewels have same order
        orders.add(new Sort.Order(Direction.DESC, ItemInfo_.itemGradeType.getName()));

        Pageable item_pageable = PageRequest.of(page_index, 50, Sort.by(orders));

        return itemInfoRepository
                .findJewels(jewelType, rarities, search, item_pageable, min_level, max_level)
                .map(i -> convertItemDTO(i));
    }

    public Page<ItemDTO> getBraceletsPage(
            Optional<Integer> theOrder,
            Optional<String> search,
            int page_index,
            Optional<Long> min_level,
            Optional<Long> max_level
    ) throws Exception
    {
        List<Order> orders = new ArrayList<>();

        if(theOrder.isPresent()) {
            Order order = new Order(
                    (theOrder.get() % 2 == 0) ? Sort.Direction.DESC : Sort.Direction.ASC,
                    numColToColName(theOrder.get())
            );
            orders.add(order);
        }
        else
        {
            orders.add(new Sort.Order(Direction.DESC, ItemInfo_.requiredLevel.getName()));
        }

        // By default also order by name in case 2 bracelets have same order
        orders.add(new Sort.Order(Direction.ASC, ItemInfo_.name.getName()));

        Pageable item_pageable = PageRequest.of(page_index, 50, Sort.by(orders));

        return itemInfoRepository
                .findBracelets(search, item_pageable, min_level, max_level)
                .map(i -> convertItemDTO(i));
    }

    public Page<ItemDTO> getTalismansPage(
            Optional<Integer> theOrder,
            Optional<String> search,
            int page_index,
            Optional<Long> min_level,
            Optional<Long> max_level
    ) throws Exception
    {
        List<Order> orders = new ArrayList<>();

        if(theOrder.isPresent()) {
            Order order = new Order(
                    (theOrder.get() % 2 == 0) ? Sort.Direction.DESC : Sort.Direction.ASC,
                    numColToColName(theOrder.get())
            );
            orders.add(order);
        }
        else
        {
            orders.add(new Sort.Order(Direction.DESC, ItemInfo_.requiredLevel.getName()));
        }

        // By default also order by name in case 2 talismans have same order
        orders.add(new Sort.Order(Direction.ASC, ItemInfo_.name.getName()));

        Pageable item_pageable = PageRequest.of(page_index, 50, Sort.by(orders));

        return itemInfoRepository
                .findTalismans(search, item_pageable, min_level, max_level)
                .map(i -> convertItemDTO(i));
    }

    public List<Long> getItemSources(Long id) throws Exception {
        ItemInfo item = itemInfoRepository
                .findByItemId(id)
                .orElseThrow(() -> new Exception("not found"));

        List<Long> ids = new ArrayList<>();

        ItemInfoServer item_info_server = item.getServerInfo();
        if(item_info_server != null)
        {
            ids = item_info_server.getDropGroups()
                    .stream()
                    .flatMap(drop -> drop.getDropGroupInfo()
                            .stream()
                            .map(drop_group -> drop_group.getMobDropTable().getMobInfo().getId()))
                            .distinct()
                    .collect(Collectors.toList());
        }

        return ids;
    }

    public ItemActionDTO getItemAction(Long id) throws Exception
    {
        ItemAction itemAction = itemActionRepository
                .findById(id)
                .orElseThrow(() -> new Exception("Can't find item action"));

        return modelMapper.map(itemAction, ItemActionDTO.class);
    }

    public SetEffectDTO getSetEffect(Long id) throws Exception {
        SetEffect setEffect = setEffectRepository
                .findById(id)
                .orElseThrow(() -> new Exception("Can't find item action"));

        return modelMapper.map(setEffect, SetEffectDTO.class);
    }

    public List<Long> getSkillItemSet(Long useClass, Long id)
    {
        // Include all to add all the jewel sets
        List<Long> listClass = ClassEnum.fromFlagToUseClass(useClass, true);

        List<SetEffect> effects = setEffectRepository.findAll();

        return effects.stream()
                .filter(effect -> {

                    ItemSet itemSet = effect.getItemSet();
                    if(itemSet != null)
                    {
                        List<ItemInfo> itemInfos = itemSet.getSetItems();
                        if(itemInfos.isEmpty())
                            return false;
                        if(!listClass.contains(itemInfos.get(0).getUseClass().getId()))
                            return false;
                    }
                    return effect.getItemActions().stream()
                            .filter(action -> {
                                ItemActionCondition condition = action.getCondition();
                                if(condition != null)
                                {
                                    // Second value of conditionActivity is the skill_group
                                    return condition.getConditionActivity().get(1).longValue() == id;
                                }
                                return false;
                            })
                            .count() > 0;
                })
                .map(effect -> effect.getUniqueId())
                .collect(Collectors.toList());
    }
}
