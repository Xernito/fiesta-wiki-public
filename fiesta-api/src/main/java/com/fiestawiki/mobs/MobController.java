package com.fiestawiki.mobs;

import com.fiestawiki.mobs.dto.MobSpeciesDTO;
import com.fiestawiki.mobs.mobdroptable.MobDropGroupInfoDTO;
import com.fiestawiki.mobs.mobinfo.MobInfoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class MobController
{
    @Autowired
    private MobService mobService;

    @GetMapping("mob/{id}")
    public ResponseEntity<MobInfoDTO> getMobInfo(@PathVariable(value = "id") Long id)
            throws Exception
    {
        MobInfoDTO mob_info = mobService.getMobInfo(id);

        return ResponseEntity.ok(mob_info);
    }

    @GetMapping("species/{id}")
    public ResponseEntity<MobSpeciesDTO> getSpecies(@PathVariable(value = "id") Long id)
            throws Exception
    {
        MobSpeciesDTO species = mobService.getSpecies(id);

        return ResponseEntity.ok(species);
    }

    @GetMapping("species")
    public ResponseEntity<Page<Long>> getMobsSpecies(
            @PageableDefault(size = 20)
            @SortDefault.SortDefaults({
                @SortDefault(sort = "level", direction = Sort.Direction.ASC)
            }) Pageable pageable,
            @RequestParam(defaultValue = "0") Long min,
            @RequestParam(defaultValue = "135") Long max,
            @RequestParam() Optional<Boolean> npc,
            @RequestParam() Optional<Boolean> active,
            @RequestParam() Optional<String> search,
            @RequestParam() Optional<Long> location
            )
    {
        Page<Long> species = mobService.getMobSpeciesPage(pageable, min, max, npc, active, search, location);

        return ResponseEntity.ok(species);
    }

    @GetMapping("mob-drops/{id}")
    public ResponseEntity<List<MobDropGroupInfoDTO>> getMobDrops(@PathVariable(value = "id") Long id)
            throws Exception
    {
        List<MobDropGroupInfoDTO> mob_drops = mobService.getMobDrops(id);

        return ResponseEntity.ok(mob_drops);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("mob")
    public ResponseEntity<MobInfoDTO> updateMobInfo(@RequestBody MobInfoDTO mob)
    {
        MobInfoDTO updated = mobService.updateMobInfo(mob);

        return ResponseEntity.ok(updated);
    }
}
