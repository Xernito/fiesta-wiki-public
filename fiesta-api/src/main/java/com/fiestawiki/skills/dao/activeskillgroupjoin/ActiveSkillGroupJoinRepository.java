package com.fiestawiki.skills.dao.activeskillgroupjoin;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ActiveSkillGroupJoinRepository extends JpaRepository<ActiveSkillGroupJoin, Long> {

    @Query("SELECT sg FROM ActiveSkillGroupJoin sg WHERE sg.skill.useClass IN(:useClasses) AND sg.skill.step = 1")
    List<ActiveSkillGroupJoin> findByClass(List<Long> useClasses);
}
