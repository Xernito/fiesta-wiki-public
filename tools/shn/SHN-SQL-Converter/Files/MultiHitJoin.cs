﻿namespace SHN_SQL_Converter.JoinTables
{
    class MultiHitJoin : JoinTableBase
    {
        public override void Create()
        {
            Table = Utils.CreateJoinTable(
                DataSet,
                   DataSet.Tables["multi_hit_type"],
                   DataSet.Tables["active_skill"],
                   Tablename,
                   "public"
                   );
        }

        public override void Load()
        {
            Utils.AddJoinColumn<ushort>(
                DataSet,
                DataSet.Tables["multi_hit_type"],
                DataSet.Tables["active_skill"],
                "ID",
                "HitID",
                Tablename
                );
        }
    }
}
