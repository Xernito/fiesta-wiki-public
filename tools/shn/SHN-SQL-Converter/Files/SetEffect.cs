﻿using iQuest;
using System.IO;

namespace SHN_SQL_Converter.Files
{
    class SetEffect : SHNFileBase
    {
        public override void CreateConstraints()
        {
            Utils.AddForeignKeyColumn(
                DataSet,
                Table.TableName, new string[] { "SetItemIndex" },
                "item_set", new string[] { "SetItemIndex" },
                "item_set_id");
        }
    }
}
