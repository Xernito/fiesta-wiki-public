package com.fiestawiki.quest.dao.reward;

import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestRewardRepository extends JpaRepository<QuestReward, Long> {

}