package com.fiestawiki.states.passiveskillabstate;

import com.fiestawiki.states.abstate.AbState;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "passive_skill_ab_state")
@EntityListeners(AuditingEntityListener.class)
public class PassiveSkillAbState implements Serializable {
    @Id
    @Column(name = "unique_id", nullable = false)
    private Long id;

    @Column(name = "ps_condition")
    private Long condition;

    @Column(name = "ps_conditiorate")
    private Long conditionRate;

    @ManyToOne
    @JoinColumn(name = "ab_state_fk")
    private AbState abState;

    @Column(name = "strength")
    private Long strength;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCondition() {
        return condition;
    }

    public void setCondition(Long condition) {
        this.condition = condition;
    }

    public Long getConditionRate() {
        return conditionRate;
    }

    public void setConditionRate(Long conditionRate) {
        this.conditionRate = conditionRate;
    }

    public AbState getAbState() {
        return abState;
    }

    public void setAbState(AbState abState) {
        this.abState = abState;
    }

    public Long getStrength() {
        return strength;
    }

    public void setStrength(Long strength) {
        this.strength = strength;
    }
}
