import { Component, OnInit, Input } from '@angular/core';
import { AbState } from '@fiesta-common/dto/skills/ab-state';

@Component({
   selector: 'app-ab-state',
   templateUrl: './ab-state.component.html',
   styleUrls: ['./ab-state.component.scss']
})
export class AbStateComponent implements OnInit {

   @Input()
   abState: AbState;

   constructor() { }

   ngOnInit() {
   }

}
