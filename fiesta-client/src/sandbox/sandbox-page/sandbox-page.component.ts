import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-sandbox-page',
    templateUrl: './sandbox-page.component.html',
    styleUrls: ['./sandbox-page.component.scss']
})
export class SandboxPageComponent implements OnInit {

    searchFormGroup = new FormGroup({
        search: new FormControl(''),
        type: new FormControl('item')
    });

    constructor(
        protected route: ActivatedRoute,
        protected router: Router) { }

    ngOnInit(): void {
    }

    onSubmit() {
        if (this.searchFormGroup.controls.type.value == 'item') {
            this.router.navigate(['item', this.searchFormGroup.controls.search.value],  { relativeTo: this.route });
        }
        else if (this.searchFormGroup.controls.type.value == 'mob') {
            this.router.navigate(['mob', this.searchFormGroup.controls.search.value],  { relativeTo: this.route });
        }
    }
}
