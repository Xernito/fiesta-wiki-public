import { Component, OnInit, HostListener, Inject, PLATFORM_ID } from '@angular/core';
import { TitleService } from '@services/title.service';
import { PageDescriptionService } from '@services/page-description.service';
import { Title } from '@dto/titles/title';
import { LoadingStatus } from '@enums/loading-return';
import { MatTableDataSource } from '@angular/material/table';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ROUTES_URL } from '@app/routing/routes';
import { isPlatformBrowser } from '@angular/common';
import { PAGES_NAME } from '@app/global-strings';

@Component({
    selector: 'app-titles-page',
    templateUrl: './titles-page.component.html',
    styleUrls: ['./titles-page.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({ height: '0px', minHeight: '0', borderBottomWidth: '0px' })),
            state('expanded', style({ height: '*', borderBottomWidth: '1px' })),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ]),
    ],
})
export class TitlesPageComponent implements OnInit {

    readonly ROUTES = ROUTES_URL;

    titles: Title[];

    displayedColumns = [
        'title',
        'fame',
        'stats'
    ];

    dataSource: MatTableDataSource<Title>;
    expandedElement: Title | null;

    loadingErr: LoadingStatus = 'loading';

    readonly PAGE_TITLE = PAGES_NAME.titles;

    @HostListener('window:resize', ['$event'])
    onResize(event?) {

        let screenWidth = 1000;

        if (isPlatformBrowser(this.platformId)) {
            screenWidth = window.innerWidth;
        }

        this.displayedColumns = ['title'];

        if (screenWidth > 450) {
            this.displayedColumns.push('fame');
        }

        this.displayedColumns.push('stats');
    }

    constructor(
        @Inject(PLATFORM_ID) private platformId: any,
        protected pageDescriptionService: PageDescriptionService,
        private titleService: TitleService,
    ) {
        this.onResize();
    }

    ngOnInit() {
        this.pageDescriptionService.setPageDescription({
            title: this.PAGE_TITLE,
            description: 'Titles in Fiesta Online, titles can be obtained by achieving some exploits or by collecting cards.'
        });

        this.titleService.getTitles()
            .subscribe(
                titles => {

                    this.loadingErr = 'ok';

                    this.titles = titles;
                    this.dataSource = new MatTableDataSource(this.titles);
                },
                err => {
                    this.loadingErr = 'error';
                });
    }

}
