package com.fiestawiki.states.abstateview;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ab_state_view")
@EntityListeners(AuditingEntityListener.class)
public class AbStateView implements Serializable {

    @Id
    @Column(name = "unique_id")
    private Long id;

    @Column(name = "inxname")
    private String inxname;

    @Column(name = "icon")
    private Long icon;

    @Column(name = "iconfile")
    private String iconFile;

    @Column(name = "descript")
    private String description;

    @Column(name = "r")
    private short red;

    @Column(name = "g")
    private short green;

    @Column(name = "b")
    private short blue;

    @Column(name = "aniindex")
    private String animationIndex;

    @Column(name = "effname")
    private String effectName;

    @Column(name = "effnamepos")
    private Long effectNamePosition;

    @Column(name = "effrefresh")
    private short effectRefresh;

    @Column(name = "loopeffect")
    private String loopEffect;

    @Column(name = "loopeffpos")
    private Long loopEffectPosition;

    @Column(name = "doteffect")
    private String dotEffect;

    @Column(name = "doteffectpos")
    private Long dotEffectPos;

    @Column(name = "iconsort")
    private String iconSort;

    @Column(name = "typesort")
    private Long typesort;

    @Column(name = "view")
    private short view;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInxname() {
        return inxname;
    }

    public void setInxname(String inxname) {
        this.inxname = inxname;
    }

    public Long getIcon() {
        return icon;
    }

    public void setIcon(Long icon) {
        this.icon = icon;
    }

    public String getIconFile() {
        return iconFile;
    }

    public void setIconFile(String iconFile) {
        this.iconFile = iconFile;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public short getRed() {
        return red;
    }

    public void setRed(short red) {
        this.red = red;
    }

    public short getGreen() {
        return green;
    }

    public void setGreen(short green) {
        this.green = green;
    }

    public short getBlue() {
        return blue;
    }

    public void setBlue(short blue) {
        this.blue = blue;
    }

    public String getAnimationIndex() {
        return animationIndex;
    }

    public void setAnimationIndex(String animationIndex) {
        this.animationIndex = animationIndex;
    }

    public String getEffectName() {
        return effectName;
    }

    public void setEffectName(String effectName) {
        this.effectName = effectName;
    }

    public Long getEffectNamePosition() {
        return effectNamePosition;
    }

    public void setEffectNamePosition(Long effectNamePosition) {
        this.effectNamePosition = effectNamePosition;
    }

    public short getEffectRefresh() {
        return effectRefresh;
    }

    public void setEffectRefresh(short effectRefresh) {
        this.effectRefresh = effectRefresh;
    }

    public String getLoopEffect() {
        return loopEffect;
    }

    public void setLoopEffect(String loopEffect) {
        this.loopEffect = loopEffect;
    }

    public Long getLoopEffectPosition() {
        return loopEffectPosition;
    }

    public void setLoopEffectPosition(Long loopEffectPosition) {
        this.loopEffectPosition = loopEffectPosition;
    }

    public String getDotEffect() {
        return dotEffect;
    }

    public void setDotEffect(String dotEffect) {
        this.dotEffect = dotEffect;
    }

    public Long getDotEffectPos() {
        return dotEffectPos;
    }

    public void setDotEffectPos(Long dotEffectPos) {
        this.dotEffectPos = dotEffectPos;
    }

    public String getIconSort() {
        return iconSort;
    }

    public void setIconSort(String iconSort) {
        this.iconSort = iconSort;
    }

    public Long getTypesort() {
        return typesort;
    }

    public void setTypesort(Long typesort) {
        this.typesort = typesort;
    }

    public short getView() {
        return view;
    }

    public void setView(short view) {
        this.view = view;
    }
}
