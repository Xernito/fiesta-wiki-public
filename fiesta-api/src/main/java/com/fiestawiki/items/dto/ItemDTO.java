package com.fiestawiki.items.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ItemDTO {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("item_grade_type")
    private int itemGradeType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getItemGradeType() {
        return itemGradeType;
    }

    public void setItemGradeType(int itemGradeType) {
        this.itemGradeType = itemGradeType;
    }
}
