﻿using System.Data;
using System.Linq;

namespace SHN_SQL_Converter.Files
{
    class QuestReward : SHNFileBase
    {
        public override void Patch(string patch_folder)
        {
            /////////////////////////////////////////////////////////////////////////////////
            // Remove when quest unavailable
            (from row in Table.AsEnumerable()
             select row["ID"])
                        .Except(from row in DataSet.Tables["quest_data"].AsEnumerable()
                                select row["ID"])
                        .ToList()
                        .ForEach(id =>
                        {
                            Table.Select(string.Format("ID = {0}", (ushort)id)).ToList().ForEach(row => Table.Rows.Remove(row));
                        });
            /////////////////////////////////////////////////////////////////////////////////
        }

        public override void CreateConstraints()
        {
            Utils.AddForeignKey(
                DataSet,
                Tablename, "id",
                "quest_data",
                "quest_reward_quest_fk"
                );
        }
    }
}
