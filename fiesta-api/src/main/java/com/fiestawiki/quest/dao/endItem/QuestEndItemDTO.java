package com.fiestawiki.quest.dao.endItem;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fiestawiki.quest.dao.questitemdropinfo.QuestItemDropInfoDTO;

import java.util.ArrayList;
import java.util.List;

public class QuestEndItemDTO
{
    @JsonProperty("item_id")
    private Long itemId;

    @JsonProperty("count")
    private int count;

    @JsonProperty("drop_info")
    private List<QuestItemDropInfoDTO> questItemDropInfos = new ArrayList<>();

    public Long getItemId() { return itemId; }

    public void setItemId(Long itemId) { this.itemId = itemId; }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<QuestItemDropInfoDTO> getQuestItemDropInfos() { return questItemDropInfos; }

    public void setQuestItemDropInfos(List<QuestItemDropInfoDTO> questItemDropInfos) { this.questItemDropInfos = questItemDropInfos; }
}
