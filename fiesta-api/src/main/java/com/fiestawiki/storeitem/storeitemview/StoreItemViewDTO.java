package com.fiestawiki.storeitem.storeitemview;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fiestawiki.storeitem.storeitem.StoreItemDTO;
import com.fiestawiki.storeitem.storeitemimage.StoreItemImageDTO;

import java.util.ArrayList;
import java.util.List;

public class StoreItemViewDTO
{
    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("icon")
    private String icon;

    @JsonProperty("image_previews")
    private List<StoreItemImageDTO> imagePreviews = new ArrayList<>();

    @JsonProperty("description")
    private String description;

    @JsonProperty("store_items")
    List<StoreItemDTO> storeItems = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() { return icon; }

    public void setIcon(String icon) { this.icon = icon; }

    public List<StoreItemImageDTO> getImagePreviews() { return imagePreviews; }

    public void setImagePreviews(List<StoreItemImageDTO> imagePreviews) { this.imagePreviews = imagePreviews; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public List<StoreItemDTO> getStoreItems() { return storeItems; }

    public void setStoreItems(List<StoreItemDTO> storeItems) { this.storeItems = storeItems; }
}
