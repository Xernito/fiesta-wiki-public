import { Component, Input, OnInit } from '@angular/core';
import { ItemInfo } from '@fiesta-common/dto/items/item';

@Component({
    selector: 'app-item-upgrade',
    templateUrl: './item-upgrade.component.html',
    styleUrls: ['./item-upgrade.component.scss']
})
export class ItemUpgradeComponent implements OnInit {

    @Input()
    item_info: ItemInfo;
    
    @Input()
    stat: number;

    private _enhancement = 0;

    @Input()
    set enhancement(tier: number) {
        this._enhancement = tier;
    }

    get enhancement(): number {
        if (this._enhancement != null)
            return this._enhancement > this.item_info.upgrade_max ? this.item_info.upgrade_max : this._enhancement;

        return this._enhancement;
    }

    constructor() { }

    ngOnInit(): void {
    }
}
