import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MobsPageComponent } from './mobs-page.component';

describe('MobsPageComponent', () => {
  let component: MobsPageComponent;
  let fixture: ComponentFixture<MobsPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MobsPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MobsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
