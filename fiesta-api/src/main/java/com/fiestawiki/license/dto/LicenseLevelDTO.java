package com.fiestawiki.license.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LicenseLevelDTO {

    @JsonProperty("level")
    private short level;

    @JsonProperty("suffix")
    private String suffix;

    @JsonProperty("mob_kill_count")
    private Long mobKillCount;

    @JsonProperty("dmg_ratio")
    private Long maxAdd;

    @JsonProperty("crit_ratio")
    private Long value;

    public short getLevel() {
        return level;
    }

    public void setLevel(short level) {
        this.level = level;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public Long getMobKillCount() {
        return mobKillCount;
    }

    public void setMobKillCount(Long mobKillCount) {
        this.mobKillCount = mobKillCount;
    }

    public Long getMaxAdd() {
        return maxAdd;
    }

    public void setMaxAdd(Long maxAdd) {
        this.maxAdd = maxAdd;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }
}
