#!/bin/bash

if [ "$1" = "up" ] ; then
        docker container stop fiesta-maintenance-$2
elif [ "$1" = "down" ] ; then
        docker container start fiesta-maintenance-$2
else
	echo "first argument should be 'up' or 'down'"
	exit 1
fi
