SET input-path=F:\FiestaOnlineNA\resmenu

SET output-path=F:\LostTemple\fiesta\wip\tools\icons

if not exist "%output-path%\mob-portraits\" mkdir %output-path%\mob-portraits

magick mogrify -crop 38x38+0+0 +repage -format webp -path "%output-path%\mob-portraits" "%input-path%\game\MobPortrait\*.png" 	

pushd "%output-path%\mob-portraits\"

for /f "Tokens=*" %%f in ('dir /l/b/a-d') do (rename "%%f" "%%f")