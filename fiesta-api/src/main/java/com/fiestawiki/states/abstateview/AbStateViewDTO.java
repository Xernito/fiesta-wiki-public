package com.fiestawiki.states.abstateview;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AbStateViewDTO
{
    @JsonProperty("id")
    private Long id;

    @JsonProperty("inxname")
    private String inxname;

    @JsonProperty("icon")
    private Long icon;

    @JsonProperty("icon_file")
    private String iconFile;

    @JsonProperty("description")
    private String description;

    @JsonProperty("border_color")
    private String borderColor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInxname() {
        return inxname;
    }

    public void setInxname(String inxname) {
        this.inxname = inxname;
    }

    public Long getIcon() {
        return icon;
    }

    public void setIcon(Long icon) {
        this.icon = icon;
    }

    public String getIconFile() {
        return iconFile.toLowerCase();
    }

    public void setIconFile(String iconFile) {
        this.iconFile = iconFile;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }
}
