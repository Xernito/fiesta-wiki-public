import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RarityComboboxComponent } from './rarity-combobox.component';

describe('RarityComboboxComponent', () => {
  let component: RarityComboboxComponent;
  let fixture: ComponentFixture<RarityComboboxComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RarityComboboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RarityComboboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
