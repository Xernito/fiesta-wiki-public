package com.fiestawiki.cards.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CardGroupDTO
{
    @JsonProperty("cards")
    private List<Long> cards;

    public List<Long> getCards() {
        return cards;
    }

    public void setCards(List<Long> cards) {
        this.cards = cards;
    }
}
