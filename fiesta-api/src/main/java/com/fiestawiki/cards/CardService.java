package com.fiestawiki.cards;

import com.fiestawiki.cards.dao.card.Card;
import com.fiestawiki.cards.dao.collectcardmobgroup.CollectCardMobGroup;
import com.fiestawiki.cards.dao.groupdesc.CardGroupDesc;
import com.fiestawiki.cards.dao.groupdesc.CardGroupDescRepository;
import com.fiestawiki.cards.dto.CardDTO;
import com.fiestawiki.cards.dto.CardDTOMinimal;
import com.fiestawiki.cards.dao.card.CardRepository;
import com.fiestawiki.cards.dao.title.CardTitle;
import com.fiestawiki.cards.dto.CardGroupDTO;
import com.fiestawiki.title.TitleService;
import com.fiestawiki.title.data.TitleData;
import com.fiestawiki.title.data.TitleDataCompositeDTO;
import com.fiestawiki.title.data.TitleDataDTO;
import org.modelmapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.lang.reflect.Type;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CardService {

    @Autowired
    private TitleService titleService;

    @Autowired
    private CardRepository cardRepository;

    @Autowired
    private CardGroupDescRepository cardGroupRepository;

    @Autowired
    private ModelMapper modelMapper;

    @PostConstruct
    public void postConstruct() {

        ///
        Converter<CardTitle, TitleDataDTO> getCardTitleDataDTO = new AbstractConverter<CardTitle, TitleDataDTO>() {
            public TitleDataDTO convert(CardTitle src)
            {
                if(src == null)
                    return null;

                TitleData title = src.getTitle();

                if(title == null)
                    return null;

                TitleDataCompositeDTO composite = modelMapper.map(title, TitleDataCompositeDTO.class);

                TitleDataDTO titleDTO = composite.getTitle(0);
                titleDTO.setStats(composite.getStat(0));

                return titleDTO;
            }
        };

        ///
        Converter<List<CollectCardMobGroup>, List<Long>> getMobSpecies = new AbstractConverter<List<CollectCardMobGroup>, List<Long>>() {
            public List<Long> convert(List<CollectCardMobGroup> src)
            {
                return src.stream()
                        .map(mob -> mob.getMobInfo())
                        .filter(mob -> mob.getActive())
                        .map(mob -> mob.getSpecies().getId())
                        .distinct()
                        .collect(Collectors.toList());
            }
        };

        ///
        TypeMap<Card, CardDTOMinimal> typeMapMinimal =
                modelMapper.createTypeMap(Card.class, CardDTOMinimal.class);

        ///
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        TypeMap<Card, CardDTO> typeMap =
                modelMapper.createTypeMap(Card.class, CardDTO.class);

        typeMap.addMappings( mapper -> {
            mapper.using(getCardTitleDataDTO).map(src -> src.getCardTitle(),
                    CardDTO::setTitle);
            mapper.using(getMobSpecies).map(src -> src.getCardGroup().getMobGroup(),
                    CardDTO::setMobSpecies);
            mapper.map(src -> src.getGetRate().getGetRate(), CardDTO::setGetRate);
            mapper.map(src -> src.getCardGroup().getMobGroupId(), CardDTO::setCardGroup);
        });

        ///
        Converter<List<Card>, List<Long>> getCrads = new AbstractConverter<List<Card>, List<Long>>() {
            public List<Long> convert(List<Card> src)
            {
                return src.stream()
                        .map(c -> c.getId())
                        .collect(Collectors.toList());
            }
        };

        TypeMap<CardGroupDesc, CardGroupDTO> cardGroupTypeMap =
                modelMapper.createTypeMap(CardGroupDesc.class, CardGroupDTO.class);

        cardGroupTypeMap.addMappings( mapper -> {
            mapper.using(getCrads).map(src -> src.getCards(),
                    CardGroupDTO::setCards);
        });
    }

    public List<CardDTO> convertCards(List<Card> cards)
    {
        Type listType = new TypeToken<List<CardDTO>>(){}.getType();
        List<CardDTO> cardsDTO = modelMapper.map(cards, listType);

        return cardsDTO;
    }

    public CardDTO convertCard(Card card)
    {
        CardDTO cardDTO = modelMapper.map(card, CardDTO.class);

        return cardDTO;
    }

    public CardDTOMinimal convertCardMinimal(Card card)
    {
        CardDTOMinimal cardDTO = modelMapper.map(card, CardDTOMinimal.class);

        return cardDTO;
    }

    public CardGroupDTO convertCardGroup(CardGroupDesc card_group)
    {
        return modelMapper.map(card_group, CardGroupDTO.class);
    }

    public List<Long> getCards() {

        List<Card> cards = cardRepository.findAll();

        return cards.stream()
                .map(c -> c.getId())
                .collect(Collectors.toList());
    }

    public CardDTO getCard(Long id) throws Exception
    {
        Card card = cardRepository
                        .findById(id)
                        .orElseThrow(() -> new Exception("Card not found"));

        return convertCard(card);
    }

    public CardGroupDTO getCardGroup(Long id) throws Exception
    {
        CardGroupDesc card_group = cardGroupRepository
                .findById(id)
                .orElseThrow(() -> new Exception("Card group not found"));

        return convertCardGroup(card_group);
    }
}
