package com.fiestawiki.mobs.mobinfo;

import com.fiestawiki.mobs.mobcoordinate.MobCoordinate;
import com.fiestawiki.mobs.mobdroptable.MobDropTable;
import com.fiestawiki.mobs.mobinfoserver.MobInfoServer;
import com.fiestawiki.mobs.mobspecies.MobSpecies;
import com.fiestawiki.mobs.mobviewinfo.MobViewInfo;
import com.fiestawiki.quest.dao.endNpc.QuestEndNpc;
import com.fiestawiki.quest.dao.quest.QuestData;
import com.fiestawiki.quest.dao.questitemdropinfo.QuestItemDropInfo;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "mob_info")
@EntityListeners(AuditingEntityListener.class)
public class MobInfo implements Serializable
{
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "inxname")
    private String inxname;

    @Column(name = "name")
    private String name;

    @Column(name = "level")
    private Long level;

    @Column(name = "maxhp")
    private Long maxHP;

    @Column(name = "walkspeed")
    private Long walkSpeed;

    @Column(name = "runspeed")
    private Long runSpeed;

    @Column(name = "isnpc")
    private Integer isNPC;

    @Column(name = "weapontype")
    private Long weaponType;

    @Column(name = "armortype")
    private Long armorType;

    /*
    0: General Monster
    1: Chief Monster
    2: World Boss
    3: Field Boss
    4: Elite Monster
    5: NPC
     */
    @Column(name = "gradetype")
    private Long gradeType;

    /*
    0: Human
    1: Elemental
    2: Spirit
    3: Beast
    4: Summoned Beast
    5: Undead
    6: NPC
    7: ???
    8: Ores
    9: Collectible
    10: Collectible
    11: GT building
    12: Summoned
    13: Projectile
    14: Guild Item Merchant
    15: GT building
    16: Devil Honeying / Demonic Honeying
    17: Transformation (bird / slime / zombie)
    18: Goldmine Miner
    19: GT Box
    20: Buildings
    21: Snowbank
    22: -
    23: Dice Game
    24: ???
    25: NPC helper
    26: Coin Slot Machine
    27: Protection Field (Saint's Protection)
    28: Devil Monster
    29: Event? (Halloween buildings...)
    30: Halloween Transform
    31: Ark Monster
    32: Ark Mecha
    33: Teleport Gate
    34: Mailbox
     */
    @Column(name = "type")
    private Long type;

    @Column(name = "location")
    private Long location;

    @Column(name = "active")
    private Boolean active;

    @OneToOne(mappedBy = "mob")
    private MobViewInfo view;

    @Column(name = "mob_species_fk", insertable = false, updatable = false)
    private Long mobSpeciesFK;

    @ManyToOne
    @JoinColumn(name = "mob_species_fk")
    private MobSpecies species;

    @OneToOne(mappedBy = "mobInfo")
    private MobInfoServer serverInfo;

    @OneToMany(mappedBy = "mob")
    private List<MobCoordinate> coordinates = new ArrayList<>();

    @OneToMany(mappedBy = "mob")
    private List<QuestEndNpc> killQuests = new ArrayList<>();

    @OneToMany(mappedBy = "mob")
    private List<QuestItemDropInfo> dropQuests = new ArrayList<>();

    @OneToOne(mappedBy = "mobInfo")
    private MobDropTable dropTable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInxname() {
        return inxname;
    }

    public void setInxname(String inxname) {
        this.inxname = inxname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getLevel() { return level; }

    public void setLevel(Long level) { this.level = level; }

    public Long getMaxHP() { return maxHP; }

    public void setMaxHP(Long maxHP) { this.maxHP = maxHP; }

    public Long getWalkSpeed() { return walkSpeed; }

    public void setWalkSpeed(Long walkSpeed) { this.walkSpeed = walkSpeed; }

    public Long getRunSpeed() { return runSpeed; }

    public void setRunSpeed(Long runSpeed) { this.runSpeed = runSpeed; }

    public Boolean getIsNPC() { return isNPC == 1; }

    public void setIsNPC(Integer isNPC) { this.isNPC = isNPC; }

    public Long getWeaponType() { return weaponType; }

    public void setWeaponType(Long weaponType) { this.weaponType = weaponType; }

    public Long getArmorType() { return armorType; }

    public void setArmorType(Long armorType) { this.armorType = armorType; }

    public Long getGradeType() { return gradeType; }

    public void setGradeType(Long gradeType) { this.gradeType = gradeType; }

    public Long getType() { return type; }

    public void setType(Long type) { this.type = type; }

    public Long getLocation() { return location; }

    public void setLocation(Long location) { this.location = location; }

    public Boolean getActive() { return active; }

    public void setActive(Boolean active) { this.active = active; }

    public MobViewInfo getView() { return view; }

    public void setView(MobViewInfo view) { this.view = view; }

    public MobSpecies getSpecies() { return species; }

    public void setSpecies(MobSpecies species) { this.species = species; }

    public MobInfoServer getServerInfo() { return serverInfo; }

    public void setServerInfo(MobInfoServer serverInfo) { this.serverInfo = serverInfo; }

    public List<MobCoordinate> getCoordinates() { return coordinates; }

    public void setCoordinates(List<MobCoordinate> coordinates) { this.coordinates = coordinates; }

    public List<QuestEndNpc> getKillQuests() { return killQuests; }

    public void setKillQuests(List<QuestEndNpc> killQuests) { this.killQuests = killQuests; }

    public List<QuestItemDropInfo> getDropQuests() { return dropQuests; }

    public void setDropQuests(List<QuestItemDropInfo> dropQuests) { this.dropQuests = dropQuests; }

    public MobDropTable getDropTable() { return dropTable; }

    public void setDropTable(MobDropTable dropTable) { this.dropTable = dropTable; }

    public List<Long> getRelatedQuests()
    {
        List<QuestData> quests = new ArrayList<>();

        for(MobInfo mob : species.getMobs())
        {
            for (QuestEndNpc kill_quest : getKillQuests())
            {
                QuestData quest = kill_quest.getQuest();
                if (quest != null && quest.getIsActive() && kill_quest.getIsEnabled()) {
                    quests.add(kill_quest.getQuest());
                }
            }
            for (QuestItemDropInfo drop_quest : getDropQuests()) {
                if (drop_quest.getQuest().getIsActive()) {
                    quests.add(drop_quest.getQuest());
                }
            }
        }

        return quests
                .stream()
                .sorted(Comparator.comparingInt(QuestData::getMinLevel))
                .map(QuestData::getId)
                .distinct()
                .collect(Collectors.toList());
    }
}
