import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppModule } from './app.module';
import { AppComponent } from '@components/app.component';


@NgModule({
    imports: [
        AppModule,
        BrowserModule.withServerTransition({ appId: 'fiesta' }),
    ],
    bootstrap: [AppComponent]
})
export class BrowserAppModule {}
