import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment';
import { StoreItemView, StoreItem, StoreCategory, StoreItemSet, StoreItemDTO, StoreCategoryDTO } from '@fiesta-common/dto/store-items/store-item';
import { map, switchMap } from 'rxjs/operators';
import { StoreItemService } from '@fiesta-common/services/store-item.service';

@Injectable({
   providedIn: 'root'
})
export class StoreItemAdminService {

   constructor(
      private http: HttpClient,
      private storeItemViews: StoreItemService
   ) { }

   createStoreItemView(item: StoreItemView): Observable<StoreItemView> {
      return this.http.post<number>(environment.api + 'store-item-view', item).pipe(
         switchMap(id => {
            return this.storeItemViews.getStoreItemView(id);
         })
      );
   }

   updateStoreItemView(item: StoreItemView): Observable<StoreItemView> {
      return this.http.put<StoreItemView>(environment.api + 'store-item-view', item);
   }

   deleteStoreItemView(id: number): Observable<boolean> {
      return this.http.delete<boolean>(environment.api + 'store-item-view/' + id);
   }

   checkStoreItemView(search: string): Observable<StoreItemView[]> {
      const params =
         new HttpParams()
            .set('search', search);

      return this.http.get<StoreItemView[]>(environment.api + 'store-item-view', { params });
   }

   createStoreItem(item: StoreItem, view: StoreItemView): Observable<StoreItem> {
      return this.http.post<number>(environment.api + 'store-item/' + view.id, item).pipe(
         switchMap(id => {
            return this.storeItemViews.getStoreItem(id);
         })
      );
   }

   updateStoreItem(item: StoreItem): Observable<StoreItem> {
      return this.http.put<StoreItemDTO>(environment.api + 'store-item', item).pipe(
         map(result => this.storeItemViews.convertStoreItem(result))
      );
   }

   deleteStoreItem(id: number): Observable<boolean> {
      return this.http.delete<boolean>(environment.api + 'store-item/' + id);
   }

   createStoreItemSet(item: StoreItemSet): Observable<StoreItemSet> {
      return this.http.post<number>(environment.api + 'store-item-set', item).pipe(
         switchMap(id => {
            return this.storeItemViews.getStoreItemSet(id);
         })
      );
   }

   addStoreItemToSet(item: StoreItem, set: StoreItemSet): Observable<StoreItem> {
      return this.http.post<number>(environment.api + 'store-item-set/' + set.id, item).pipe(
         switchMap(id => {
            return this.storeItemViews.getStoreItem(id);
         })
      );
   }

   getStoreCategory(id: number): Observable<StoreCategory> {
      return this.http.get<StoreCategoryDTO>(environment.api + 'store-category/' + id).pipe(
         map(result => this.storeItemViews.convertStoreCategory(result))
      );
   }

   createStoreCategory(category: StoreCategory): Observable<StoreCategory> {
      return this.http.post<number>(environment.api + 'store-category', category).pipe(
         switchMap(id => {
            return this.getStoreCategory(id);
         })
      );
   }

   updateStoreCategory(category: StoreCategory): Observable<StoreCategory> {
      return this.http.put<StoreCategory>(environment.api + 'store-category', category);
   }

   deleteStoreCategory(id: number): Observable<boolean> {
      return this.http.delete<boolean>(environment.api + 'store-category/' + id);
   }
}
