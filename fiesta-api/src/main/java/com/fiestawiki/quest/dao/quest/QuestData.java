package com.fiestawiki.quest.dao.quest;

import com.fiestawiki.quest.dao.dialog.QuestDialog;
import com.fiestawiki.quest.dao.endItem.QuestEndItem;
import com.fiestawiki.quest.dao.endNpc.QuestEndNpc;
import com.fiestawiki.quest.dao.reward.QuestReward;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "quest_data")
@EntityListeners(AuditingEntityListener.class)
public class QuestData
{
    @Id
    @Column (name = "id", nullable = false)
    private Long id;

    // normal = 0 - 5 - 8
    // epic = 1 - 6 - 7
    // class = 2
    // event = 3
    // chaos = 4
    // dailies = 10
    @Column(name = "q_type")
    private int questType;

    @OneToOne
    @JoinColumn(name = "nameid")
    private QuestDialog title;

    @OneToOne
    @JoinColumn(name = "descid")
    private QuestDialog description;

    @Column(name = "type")
    private int type;

    @Column(name = "lvlstart")
    private Integer minLevel;

    @Column(name = "lvlend")
    private int maxLevel;

    @Column(name = "npcid")
    private Long startingNpcId;

    @Column(name = "itemid")
    private Long startingItemId;

    @Column(name = "itemlot")
    private Long startingItemQuantity;

    @OneToMany(mappedBy = "quest")
    private List<QuestReward> rewards = new ArrayList<>();

    @OneToMany
    @JoinColumn(name = "id", referencedColumnName="id")
    private List<QuestEndItem> itemGathering = new ArrayList<>();

    @OneToMany(mappedBy = "quest")
    private List<QuestEndNpc> endNpc = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "prevquestid")
    private QuestData previousQuest;

    @Column(name = "startclassid")
    private int startClass;

    @OneToMany(mappedBy = "previousQuest")
    private List<QuestData> nextQuests;

    @Column(name = "isactive")
    private short isActive;

    @Column(name = "exp")
    private Long exp;

    @Column(name = "fame")
    private Long fame;

    @Column(name = "money")
    private Long money;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getQuestType() {
        return questType;
    }

    public void setQuestType(int questType) {
        this.questType = questType;
    }

    public String getTitle() {
        return title.getDialog();
    }

    public void setTitle(QuestDialog title) {
        this.title = title;
    }

    public String getDescription() {
        return description.getDialog();
    }

    public void setDescription(QuestDialog description) {
        this.description = description;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Integer getMinLevel() {
        return minLevel;
    }

    public void setMinLevel(Integer minLevel) {
        this.minLevel = minLevel;
    }

    public int getMaxLevel() {
        return maxLevel;
    }

    public void setMaxLevel(int maxLevel) {
        this.maxLevel = maxLevel;
    }

    public Long getStartingNpcId() { return startingNpcId; }

    public void setStartingNpcId(Long startingNpcId) { this.startingNpcId = startingNpcId;}

    public Long getStartingItemId() { return startingItemId; }

    public void setStartingItemId(Long startingItemId) { this.startingItemId = startingItemId; }

    public Long getStartingItemQuantity() { return startingItemQuantity; }

    public void setStartingItemQuantity(Long startingItemQuantity) { this.startingItemQuantity = startingItemQuantity; }

    public List<QuestReward> getRewards() {
        return rewards;
    }

    public void setRewards(List<QuestReward> rewards) {
        this.rewards = rewards;
    }

    public List<QuestEndNpc> getEndNpc() { return endNpc; }

    public void setEndNpc(List<QuestEndNpc> endNpc) { this.endNpc = endNpc; }

    public List<QuestEndItem> getItemGathering() { return itemGathering; }

    public void setItemGathering(List<QuestEndItem> itemGathering) { this.itemGathering = itemGathering; }

    public QuestData getPreviousQuest() {
        return previousQuest;
    }

    public void setPreviousQuest(QuestData previousQuest) {
        this.previousQuest = previousQuest;
    }

    public int getStartClass() { return startClass; }

    public void setStartClass(int startClass) { this.startClass = startClass; }

    public List<QuestData> getNextQuests() {
        return nextQuests;
    }

    public boolean getRepeat() { return getType() %2 != 0; }

    public boolean getRemoteTurnPickUp() { return (getType() & 0x4) != 0; }

    public boolean getRemoteTurnIn() { return (getType() & 0x8) != 0; }

    public List<QuestReward> getSelectableItemRewards()
    {
        return rewards
                .stream()
                .filter(q -> q.getRewardType() == 2 && q.getSelectable() == 2)
                .collect(Collectors.toList());
    }

    public List<QuestReward> getItemRewards()
    {
        return rewards
                .stream()
                .filter(q -> q.getRewardType() == 2 && q.getSelectable() == 1)
                .collect(Collectors.toList());
    }

    public boolean getIsActive() {
        return isActive > 0;
    }

    public Long getExp() { return exp; }

    public void setExp(Long exp) { this.exp = exp; }

    public Long getFame() {  return fame; }

    public void setFame(Long fame) { this.fame = fame; }

    public Long getMoney() { return money; }

    public void setMoney(Long money) { this.money = money; }
}
