﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace SHN_SQL_Converter
{
    public class Utils
    {
        public static byte ShiftLeft(byte value, int count)
        {
            return (byte)((value << count) | (value >> (8 - count)) & 0xff);
        }

        public static uint ShiftRight(uint value, int count)
        {
            return (byte)((value >> count) | (value << (8 - count)) & 0xff);
        }

        public static ushort[] GetFiestaVersion(string folder)
        {
            FileInfo version_fileinfo = new FileInfo(Path.Combine(folder, "Fiesta.vdt"));

            if (version_fileinfo.Exists)
            {
                using (BinaryReader file = new BinaryReader(File.OpenRead(version_fileinfo.FullName)))
                {
                    byte key = file.ReadByte();
                    byte unk1 = file.ReadByte();
                    ushort major = file.ReadUInt16();
                    ushort minor = file.ReadUInt16();
                    ushort patch = file.ReadUInt16();

                    key = ShiftLeft(key, 4);

                    ushort version_major = (ushort)((~(key ^ ((byte)major)) & 0xff) | (0x0f00 & major));
                    ushort version_minor = (ushort)((~(key ^ ((byte)minor)) & 0xff) | (0x0f00 & minor));
                    ushort version_patch = (ushort)((~(key ^ ((byte)patch)) & 0xff) | (0x0f00 & patch));

                    return new ushort[] { version_major, version_minor, version_patch };
                }
            }

            return null;
        }

        public static string MySqlEscape(string usString)
        {
            if (usString == null)
            {
                return null;
            }
            // SQL Encoding for MySQL Recommended here:
            // http://au.php.net/manual/en/function.mysql-real-escape-string.php
            // it escapes \r, \n, \x00, \x1a, baskslash, single quotes, and double quotes
            //string temp = Regex.Replace(usString, @"[\r\n\x00\x1a\\""]", @"\$0");
            //temp = Regex.Replace(temp, @"[\u0085]", System.Environment.NewLine);
            string temp = Regex.Replace(usString, @"[']", @"''");
            temp = Regex.Replace(temp, @"[\u0085]", @"");
            temp = Regex.Replace(temp, @"[\u0092]", @"");
            return temp;
        }


        public static void RemoveEmptyString(DataRow dr, string x)
        {
            string entry = dr[x] as string;
            if (entry == "" || entry == "-")
            {
                dr[x] = DBNull.Value;
            }
        }

        public static void RemoveEmptyUShort(DataRow dr, string x)
        {
            if (((ushort)dr[x]) == 0)
            {
                dr[x] = DBNull.Value;
            }
        }

        public static void CreateTABLE(DataTable table, StreamWriter file)
        {
            string sqlsc;
            sqlsc = "CREATE TABLE ";

            sqlsc += $"\"{table.Namespace}\".\"{table.TableName}\"(";
            //sqlsc += "\n unique_id  int  PRIMARY KEY,";

            for (int i = 0; i < table.Columns.Count; i++)
            {
                string col_name = table.Columns[i].ColumnName;
                sqlsc += "\n " + table.Columns[i].ColumnName + " ";
                string columnType = table.Columns[i].DataType.ToString();
                switch (columnType)
                {
                    case "System.UInt16":
                        sqlsc += " int";
                        break;
                    case "System.Int32":
                        sqlsc += " int";
                        break;
                    case "System.UInt32":
                        sqlsc += " bigint";
                        break;
                    case "System.Int64":
                        sqlsc += " bigint";
                        break;
                    case "System.Int16":
                    case "System.Byte":
                    case "System.SByte":
                        sqlsc += " smallint";
                        break;
                    case "System.Decimal":
                        sqlsc += " decimal";
                        break;
                    case "System.DateTime":
                        sqlsc += " datetime";
                        break;
                    case "System.Boolean":
                        sqlsc += " boolean";
                        break;
                    case "System.UInt32[]":
                        sqlsc += " integer[]";
                        break;
                    case "System.String":
                    default:
                        sqlsc += string.Format(" VARCHAR(2000)");
                        break;
                }

                if (table.Columns[i].Unique)
                {
                    sqlsc += " UNIQUE";
                }
                if (table.PrimaryKey.Length > 0 &&
                    table.Columns[i] == table.PrimaryKey[0])
                {
                    sqlsc += " PRIMARY KEY";
                }

                //if (table.Columns[i].AutoIncrement)
                //    sqlsc += " IDENTITY(" + table.Columns[i].AutoIncrementSeed.ToString() + "," + table.Columns[i].AutoIncrementStep.ToString() + ") ";
                //if (!table.Columns[i].AllowDBNull)
                //    sqlsc += " NOT NULL ";
                sqlsc += ",";
            }

            file.WriteLine(sqlsc.Substring(0, sqlsc.Length - 1) + "\n);");
        }

        public static void InsertTable(DataTable table, StreamWriter file)
        {
            for (int row_index = 0; row_index < table.Rows.Count; row_index++)
            {
                DataRow row = table.Rows[row_index];

                string sqlsc = "INSERT INTO ";

                if (table.Namespace.Length > 0)
                {
                    sqlsc += $"\"{table.Namespace}\".";
                }
                sqlsc += $"\"{table.TableName}\"(";

                for (int c = 0; c < table.Columns.Count; c++)
                {
                    sqlsc += table.Columns[c].ColumnName;

                    if (c + 1 < table.Columns.Count)
                        sqlsc += ", ";
                }

                sqlsc += ") VALUES (";

                //query += row_index + ", ";

                for (int c = 0; c < table.Columns.Count; c++)
                {
                    DataColumn col = table.Columns[c];

                    if (row[col] == System.DBNull.Value)
                    {
                        sqlsc += "null";
                    }
                    else
                    {
                        string columnType = col.DataType.ToString();
                        switch (columnType)
                        {
                            case "System.UInt16":
                                sqlsc += row[col];
                                break;
                            case "System.Int32":
                                sqlsc += row[col];
                                break;
                            case "System.UInt32":
                                sqlsc += row[col];
                                break;
                            case "System.Int64":
                                sqlsc += row[col];
                                break;
                            case "System.Int16":
                                sqlsc += row[col];
                                break;
                            case "System.Byte":
                            case "System.SByte":
                                sqlsc += row[col];
                                break;
                            case "System.Decimal":
                                sqlsc += row[col];
                                break;
                            case "System.DateTime":
                                //query += row[quest_dialog.columns[c].name];
                                break;
                            case "System.Boolean":
                                sqlsc += row[col];
                                break;
                            case "System.UInt32[]":
                                {
                                    sqlsc += $"'{{{string.Join(",", row[col] as uint[])}}}'";
                                    break;
                                }
                            case "System.String":
                            default:
                                {
                                    string s = row[col] as string;
                                    sqlsc += "E'" + MySqlEscape(s) + "'";
                                }
                                break;
                        }
                    }


                    if (c + 1 < table.Columns.Count)
                        sqlsc += ", ";
                }

                sqlsc += ");";

                file.WriteLine(sqlsc);
            }
        }

        public static void CreateConstraints(DataSet data_set, StreamWriter file)
        {
            foreach (DataTable table in data_set.Tables)
            {
                if (table.ParentRelations.Count > 0)
                {

                    string sqlsc = $"\nALTER TABLE \"{table.Namespace}\".\"{table.TableName}\"";
                    foreach (DataRelation fk in table.ParentRelations)
                    {
                        sqlsc += $"\n ADD CONSTRAINT \"{fk.RelationName}\"";
                        sqlsc += $"\n\t FOREIGN KEY ({fk.ChildColumns[0].ColumnName})";
                        sqlsc += $"\n\t\t REFERENCES \"{fk.ParentTable.Namespace}\".\"{fk.ParentTable.TableName}\"({fk.ParentColumns[0].ColumnName}),";
                    }

                    file.WriteLine(sqlsc.Substring(0, sqlsc.Length - 1) + ";");
                }
            }
        }

        public static void AddForeignKeyColumn(
            DataSet data_set,
            string table_name,
            string[] column_name,
            string referenced_table_name,
            string[] referenced_column_name,
            string fk_name,
            bool keep_on_not_found = true)
        {
            DataTable table = data_set.Tables[table_name];
            DataTable referenced_table = data_set.Tables[referenced_table_name];

            DataColumn primary_key_column = referenced_table.PrimaryKey[0];

            DataColumn fk_col = table.Columns.Add(fk_name, primary_key_column.DataType);

            AddForeignKey(data_set, table_name, fk_name, referenced_table_name, fk_name);

            for (int i = table.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr1 = table.Rows[i];

                DataRow[] selectedRows =
                    referenced_table
                        .AsEnumerable()
                        .Where(r =>
                        {
                            foreach (var col in column_name.Zip(referenced_column_name, (col_name, ref_col_name) => new { col_name, ref_col_name }))
                            {
                                if (!Compare(r[col.ref_col_name], dr1[col.col_name]))
                                    return false;
                            }
                            return true;
                        })
                        .ToArray();

                if (selectedRows.Length == 1)
                {
                    dr1.SetField(fk_name, selectedRows[0][primary_key_column]);
                }
                else if (selectedRows.Length > 1)
                {
                    throw new Exception();
                }
                else if (!keep_on_not_found)
                {
                    table.Rows.RemoveAt(i);
                }
            }
        }

        // Remove rows in table_name that does not fit the constraint
        public static void PrepareForeignKey(
            DataSet data_set,
            string table_name,
            string fk_col_name,
            string referenced_table_name)
        {
            DataTable table = data_set.Tables[table_name];
            DataTable referenced_table = data_set.Tables[referenced_table_name];

            DataColumn primary_key_column = referenced_table.PrimaryKey[0];

            for (int i = table.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr1 = table.Rows[i];

                DataRow[] selectedRows =
                    referenced_table
                        .AsEnumerable()
                        .Where(r =>
                        {
                            return r[primary_key_column.ColumnName].Equals(dr1[fk_col_name]);
                        })
                        .ToArray();

                if (selectedRows.Length == 1)
                {
                    // Ok !
                }
                else if (selectedRows.Length > 1)
                {
                    throw new Exception();
                }
                else
                {
                    // Since this is a foreign key on column already existing we don't have the choice but to remove this row
                    table.Rows.RemoveAt(i);
                }
            }
        }

        public static void AddForeignKey(
            DataSet data_set,
            string table_name,
            string fk_col_name,
            string referenced_table_name,
            string fk_name)
        {
            DataTable table = data_set.Tables[table_name];
            DataTable referenced_table = data_set.Tables[referenced_table_name];

            DataColumn primary_key_column = referenced_table.PrimaryKey[0];
            DataColumn fk_col = table.Columns[fk_col_name];

            data_set.Relations.Add(new DataRelation(string.Format("{0}_{1}_{2}", table_name, referenced_table_name, fk_name), primary_key_column, fk_col));
        }

        public static void PatchTable_Update(DataTable table, DataTable table_patch, string id, string[] col_to_update)
        {
            Type primary_key_type = table.Columns[id].DataType;

            var results = from t1 in table.AsEnumerable()
                          join t2 in table_patch.AsEnumerable() on Convert.ChangeType(t1[id], primary_key_type) equals Convert.ChangeType(t2[id], primary_key_type)
                          select new { t1, t2 };

            foreach (var item in results)
            {
                foreach (var col in col_to_update)
                {
                    item.t1[col] = item.t2[col];
                }
            }
        }

        public static void PatchTable_Delete(DataTable table, DataTable table_patch, string id)
        {
            Type key_type = table.Columns[id].DataType;

            (from t1 in table.AsEnumerable()
             join t2 in table_patch.AsEnumerable() on Convert.ChangeType(t1[id], key_type) equals Convert.ChangeType(t2[id], key_type)
             select t1).ToList().ForEach(row => table.Rows.Remove(row));
        }

        public static DataTable CreateJoinTable(
            DataSet data_set,
            DataTable table1,
            DataTable table2,
            string join_table_name,
            string namespace_name,
            bool generate_id = false
            )
        {
            DataTable join_table = new DataTable(join_table_name, namespace_name);

            if (generate_id)
            {
                var pk_col = join_table.Columns.Add("id", typeof(uint));
                pk_col.AutoIncrement = true;
                pk_col.AutoIncrementSeed = 1;
                pk_col.AutoIncrementStep = 1;

                DataColumn[] primary_keys = new DataColumn[1];
                primary_keys[0] = pk_col;
                join_table.PrimaryKey = primary_keys;
            }

            join_table.Columns.Add("pk1", table1.PrimaryKey[0].DataType);
            join_table.Columns.Add("pk2", table2.PrimaryKey[0].DataType);

            data_set.Tables.Add(join_table);

            AddForeignKey(data_set, join_table.TableName, "pk1", table1.TableName, "pk1_fk");
            AddForeignKey(data_set, join_table.TableName, "pk2", table2.TableName, "pk2_fk");

            return join_table;
        }

        private static bool Compare(object a, object b)
        {
            if (a is string && b is string)
            {
                return string.Equals(a as string, b as string, StringComparison.OrdinalIgnoreCase);
            }
            else
            {
                return a.Equals(b);
            }
        }

        public delegate bool Comparator(object a, object b);

        public static void AddJoinColumn<T>(
            DataSet data_set,
            DataTable table1,
            DataTable table2,
            string join_column_1,
            string join_column_2,
            string join_table_name,
            string[] additional_columns_origin = null,
            string[] additional_columns_dest = null,
            Comparator comparator = null
            )
        {
            DataTable join_table = data_set.Tables[join_table_name];
            if (join_table == null)
            {
                throw new UnknownShnTable();
            }

            var primary_key_1 = table1.PrimaryKey[0];
            var primary_key_2 = table2.PrimaryKey[0];

            foreach (DataRow dr1 in table1.Rows)
            {
                T value = dr1.Field<T>(join_column_1);
                if (value != null)
                {
                    if (comparator == null)
                    {
                        comparator = Compare;
                    }

                    var selectedRows = (from r in table2.AsEnumerable()
                                        where comparator(value, r[join_column_2])
                                        select r).ToList();

                    foreach (DataRow dr2 in selectedRows)
                    {
                        DataRow join = join_table.NewRow();
                        join["pk1"] = dr1[primary_key_1.ColumnName];
                        join["pk2"] = dr2[primary_key_2.ColumnName];

                        if (additional_columns_origin != null && additional_columns_dest != null)
                        {
                            foreach (var col_name in additional_columns_origin.Zip(additional_columns_dest, (o, d) => new { origine = o, destination = d }))
                            {
                                join[col_name.destination] = dr1[col_name.origine];
                            }
                        }

                        join_table.Rows.Add(join);
                    }
                }
            }
        }
    }
}
