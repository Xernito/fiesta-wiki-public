package com.fiestawiki.quest.dao.quest;

import java.util.List;
import java.util.Optional;

public interface QuestRepositoryCustom
{
    List<QuestData> findQuests(int min, int max, Boolean hideEvents, List<Integer> classes);

    List<QuestData> getAvailableQuestsForLevel(
            Integer level,
            Optional<Integer> minLevel,
            Boolean hideEvents,
            List<Integer> classes
    );
}
