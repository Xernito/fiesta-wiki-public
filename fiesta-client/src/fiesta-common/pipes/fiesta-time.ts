import { Pipe, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Pipe({ name: 'fiestaSecondsTime' })
export class FiestaSecondsTimePipe extends DecimalPipe implements PipeTransform {
   transform(value: any, args?: any): any {

      let ret = '';
      let seconds = value / 1000;

      if (seconds >= 60) {
         let minutes = Math.floor(seconds / 60);
         seconds = seconds - minutes * 60;

         if (minutes >= 60) {
            let hours = Math.floor(minutes / 60);
            minutes = minutes - hours * 60;

            if (hours >= 24) {
               const days = Math.floor(hours / 24);
               hours = hours - days * 24;

               if (days > 10) {
                  return 'Infinite';
               }

               if (days > 0) {
                  ret += super.transform(days, args) + (days > 1 ? ' days' : ' day');
               }
            }

            if (ret.length) {
               ret += ' ';
            }

            if (hours > 0) {
               ret += super.transform(hours, args) + (hours > 1 ? ' hours' : ' hour');
            }
         }

         if (ret.length) {
            ret += ' ';
         }

         if (minutes > 0) {
            ret += super.transform(minutes, args) + (minutes > 1 ? ' minutes' : ' minute');
         }
      }

      if (ret.length) {
         ret += ' ';
      }

      if (seconds > 0) {
         ret += super.transform(seconds, args) + (seconds > 1 ? ' seconds' : ' second');
      }

      return ret;
   }
}

@Pipe({ name: 'fiestaMinutesTime' })
export class FiestaMinutesTimePipe extends DecimalPipe implements PipeTransform {
   transform(value: any, args?: any): any {

      let ret = '';
      let minutes = value;

      if (minutes >= 60) {
         let hours = Math.floor(minutes / 60);
         minutes = minutes - hours * 60;

         if (hours >= 24) {
            const days = Math.floor(hours / 24);
            hours = hours - days * 24;

            if (days > 0) {
               ret += super.transform(days, args) + (days > 1 ? ' days' : ' day');
            }
         }

         if (ret.length && hours > 0) {
            ret += ' ';
         }

         if (hours > 0) {
            ret += super.transform(hours, args) + (hours > 1 ? ' hours' : ' hour');
         }
      }

      if (ret.length && minutes > 0) {
         ret += ' ';
      }

      if (minutes > 0) {
         ret += super.transform(minutes, args) + (minutes > 1 ? ' minutes' : ' minute');
      }

      return ret;
   }
}
