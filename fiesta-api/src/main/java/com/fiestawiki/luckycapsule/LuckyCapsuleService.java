package com.fiestawiki.luckycapsule;

import com.fiestawiki.items.dao.info.ItemInfo;
import com.fiestawiki.items.dto.ItemDTO;
import com.fiestawiki.luckycapsule.lcgrouprate.LuckyCapsuleGroupRate;
import com.fiestawiki.luckycapsule.lcgrouprate.LuckyCapsuleGroupRateDTO;
import com.fiestawiki.luckycapsule.lcgrouprate.LuckyCapsuleGroupRateRepository;
import com.fiestawiki.luckycapsule.lcreward.LuckyCapsuleRewardRepository;
import com.fiestawiki.luckycapsule.luckycapsule.LuckyCapsule;
import com.fiestawiki.luckycapsule.luckycapsule.LuckyCapsuleDTO;
import com.fiestawiki.luckycapsule.luckycapsule.LuckyCapsuleRepository;
import org.modelmapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.lang.reflect.Type;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LuckyCapsuleService
{
    @Autowired
    private LuckyCapsuleRepository luckyCapsuleRepository;

    @Autowired
    private LuckyCapsuleGroupRateRepository luckyCapsuleGroupRateRepository;

    @Autowired
    private LuckyCapsuleRewardRepository luckyCapsuleRewardRepository;

    @Autowired
    private ModelMapper modelMapper;

    @PostConstruct
    public void postConstruct()
    {
        modelMapper.emptyTypeMap(LuckyCapsule.class, LuckyCapsuleDTO.class).addMappings(mapper -> {
            mapper.map(src -> src.getCapsule().getId(), LuckyCapsuleDTO::setCapsuleId);
            mapper.map(LuckyCapsule::getServerId, LuckyCapsuleDTO::setServerId);
        });
    }

    private List<LuckyCapsuleGroupRateDTO> convertAllCapsuleGroup(List<LuckyCapsuleGroupRate> groups)
    {
        Type listType = new TypeToken<List<LuckyCapsuleGroupRateDTO>>(){}.getType();
        return  modelMapper.map(groups, listType);
    }

    public List<LuckyCapsuleGroupRateDTO> getCapsuleReward(Long itemId) {

        List<LuckyCapsuleGroupRate> groups = luckyCapsuleGroupRateRepository.findByItemId(itemId);

        return convertAllCapsuleGroup(groups);
    }

    public List<LuckyCapsuleDTO> getCapsules(Byte type)
    {
        return luckyCapsuleRepository
                .findByType(type)
                .stream()
                .map(lc -> modelMapper.map(lc, LuckyCapsuleDTO.class))
                .collect(Collectors.toList());
    }
}
