package com.fiestawiki.items;

import com.fiestawiki.items.dao.info.ItemInfo;
import com.fiestawiki.items.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class ItemsController {

    @Autowired
    private ItemService itemService;

    @GetMapping("item/{id}")
    public ResponseEntity<ItemDTO> getItem(@PathVariable(value = "id") Long id)
            throws Exception
    {
        ItemDTO item = itemService.getItem(id);

        return ResponseEntity.ok(item);
    }

    @GetMapping("item-view/{id}")
    public ResponseEntity<ItemViewDTO> getItemViewInfo(@PathVariable(value = "id") Long id)
            throws Exception
    {
        ItemViewDTO item_view = itemService.getItemView(id);

        return ResponseEntity.ok(item_view);
    }

    @GetMapping("item-info/{id}")
    public ResponseEntity<ItemInfoDTO> getItemInfo(@PathVariable(value = "id") Long id)
            throws Exception
    {
        ItemInfoDTO item_info = itemService.getItemInfo(id);

        return ResponseEntity.ok(item_info);
    }

    @GetMapping("item-set/{id}")
    public ResponseEntity<ItemSetDTO> getItemSet(@PathVariable(value = "id") Long id)
            throws Exception
    {
        ItemSetDTO item_set = itemService.getItemSet(id);

        return ResponseEntity.ok(item_set);
    }

    @GetMapping("set-effect/{id}")
    public ResponseEntity<SetEffectDTO> getSetEffect(@PathVariable(value = "id") Long id)
            throws Exception
    {
        SetEffectDTO setEffect = itemService.getSetEffect(id);

        return ResponseEntity.ok(setEffect);
    }

    @GetMapping("item-sources/{id}")
    public ResponseEntity<List<Long>> getItemSources(@PathVariable(value = "id") Long id)
            throws Exception
    {
        List<Long> sources = itemService.getItemSources(id);

        return ResponseEntity.ok(sources);
    }

    @GetMapping("item-raw/{id}")
    public ResponseEntity<ItemInfo> getItemInfoRaw(@PathVariable(value = "id") Long id)
            throws Exception
    {
        ItemInfo item_info = itemService.getItemInfoRaw(id);

        return ResponseEntity.ok(item_info);
    }

    @GetMapping("item-action/{id}")
    public ResponseEntity<ItemActionDTO> getItemAction(@PathVariable(value = "id") Long id)
            throws Exception
    {
        ItemActionDTO itemAction = itemService.getItemAction(id);

        return ResponseEntity.ok(itemAction);
    }

    @GetMapping("find-item")
    public ResponseEntity<List<ItemDTO>> getItemInfoRaw(@RequestParam(name = "search") Optional<String> name)
            throws Exception
    {
        List<ItemDTO> items = new ArrayList<>();

        if(name.isPresent())
        {
            items = itemService.findItemByName(name.get());
        }

        return ResponseEntity.ok(items);
    }

    @GetMapping("items/{page}")
    public ResponseEntity<Page<ItemDTO>> getItemPage(@PathVariable(value = "page") int page)
            throws Exception
    {
        Page<ItemDTO> item_info_page = itemService.getItemPage(page);

        return ResponseEntity.ok(item_info_page);
    }

    @GetMapping("skill-item-set/{class}/{id}")
    public ResponseEntity<List<Long>> getSkillItemSet(
            @PathVariable(value = "class") Long useClass,
            @PathVariable(value = "id") Long id
    )
            throws Exception
    {
        List<Long> item_info = itemService.getSkillItemSet(useClass, id);

        return ResponseEntity.ok(item_info);
    }

    @GetMapping("weapons/{weapon-type}/{page}")
    public ResponseEntity<Page<ItemDTO>> getWeaponsPage(
            @PathVariable(value = "weapon-type") Long weaponType,
            @PathVariable(value = "page") int page,
            @RequestParam(name = "rarity") Optional<List<Long>> rarities,
            @RequestParam(name = "order") Optional<Integer> theOrder,
            @RequestParam(name = "search") Optional<String> search,
            @RequestParam(name = "min") Optional<Long> min_level,
            @RequestParam(name = "max") Optional<Long> max_level
            ) throws Exception
    {
        Page<ItemDTO> item_info_page = itemService.getWeaponsPage(weaponType, rarities, theOrder, search, page, min_level, max_level);

        return ResponseEntity.ok(item_info_page);
    }

    @GetMapping("shields/{shield-type}/{page}")
    public ResponseEntity<Page<ItemDTO>> getShieldsPage(
            @PathVariable(value = "shield-type") Long shieldType,
            @PathVariable(value = "page") int page,
            @RequestParam(name = "rarity") Optional<List<Long>> rarities,
            @RequestParam(name = "order") Optional<Integer> theOrder,
            @RequestParam(name = "search") Optional<String> search,
            @RequestParam(name = "min") Optional<Long> min_level,
            @RequestParam(name = "max") Optional<Long> max_level
    ) throws Exception
    {
        Page<ItemDTO> item_info_page = itemService.getShieldsPage(shieldType, rarities, theOrder, search, page, min_level, max_level);

        return ResponseEntity.ok(item_info_page);
    }

    @GetMapping("gears/{class-type}/{page}")
    public ResponseEntity<Page<ItemDTO>> getGearsPage(
            @PathVariable(value = "class-type") Long theClass,
            @PathVariable(value = "page") int page,
            @RequestParam(name = "rarity") Optional<List<Long>> rarities,
            @RequestParam(name = "order") Optional<Integer> theOrder,
            @RequestParam(name = "search") Optional<String> search,
            @RequestParam(name = "min") Optional<Long> min_level,
            @RequestParam(name = "max") Optional<Long> max_level
    ) throws Exception
    {
        Page<ItemDTO> item_info_page = itemService.getGearsPage(theClass, rarities, theOrder, search, page, min_level, max_level);

        return ResponseEntity.ok(item_info_page);
    }

    @GetMapping("jewels/{jewel-type}/{page}")
    public ResponseEntity<Page<ItemDTO>> getJewelsPage(
            @PathVariable(value = "jewel-type") Long jewelType,
            @PathVariable(value = "page") int page,
            @RequestParam(name = "rarity") Optional<List<Long>> rarities,
            @RequestParam(name = "order") Optional<Integer> theOrder,
            @RequestParam(name = "search") Optional<String> search,
            @RequestParam(name = "min") Optional<Long> min_level,
            @RequestParam(name = "max") Optional<Long> max_level
    ) throws Exception
    {
        Page<ItemDTO> item_info_page = itemService.getJewelsPage(jewelType, theOrder, rarities, search, page, min_level, max_level);

        return ResponseEntity.ok(item_info_page);
    }

    @GetMapping("bracelets/{page}")
    public ResponseEntity<Page<ItemDTO>> getBraceletsPage(
            @PathVariable(value = "page") int page,
            @RequestParam(name = "order") Optional<Integer> theOrder,
            @RequestParam(name = "search") Optional<String> search,
            @RequestParam(name = "min") Optional<Long> min_level,
            @RequestParam(name = "max") Optional<Long> max_level
    ) throws Exception
    {
        Page<ItemDTO> item_info_page = itemService.getBraceletsPage(theOrder, search, page, min_level, max_level);

        return ResponseEntity.ok(item_info_page);
    }

    @GetMapping("talismans/{page}")
    public ResponseEntity<Page<ItemDTO>> getJewelsPage(
            @PathVariable(value = "page") int page,
            @RequestParam(name = "order") Optional<Integer> theOrder,
            @RequestParam(name = "search") Optional<String> search,
            @RequestParam(name = "min") Optional<Long> min_level,
            @RequestParam(name = "max") Optional<Long> max_level
    ) throws Exception
    {
        Page<ItemDTO> item_info_page = itemService.getTalismansPage(theOrder, search, page, min_level, max_level);

        return ResponseEntity.ok(item_info_page);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<?> notFound()
    {
        return ResponseEntity.notFound().build();
    }

}
