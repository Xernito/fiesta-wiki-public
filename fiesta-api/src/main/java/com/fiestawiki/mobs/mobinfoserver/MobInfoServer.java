package com.fiestawiki.mobs.mobinfoserver;

import com.fiestawiki.mobs.mobinfo.MobInfo;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "mob_info_server")
@EntityListeners(AuditingEntityListener.class)
public class MobInfoServer  implements Serializable
{
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "inxname")
    private String inxname;

    @Column(name = "visible")
    private Integer visible;

    @Column(name = "ac")
    private Integer defense;

    @Column(name = "tb")
    private Integer evasion;

    @Column(name = "mr")
    private Integer magicalDefense;

    @Column(name = "mb")
    private Integer mb; // ???

    @Column(name = "enemydetecttype")
    private Long aggroType;

    @Column(name = "detectcha")
    private Integer aggroRadius;

    @Column(name = "followcha")
    private Integer aggroRange;

    @Column(name = "monexp")
    private Long exp;

    @Column(name = "exprange")
    private Integer expRange;

    @Column(name = "str")
    private Integer strength;

    @Column(name = "dex")
    private Integer dexterity;

    @Column(name = "con")
    private Integer endurance;

    @Column(name = "int")
    private Integer intelligence;

    @Column(name = "men")
    private Integer spirit;

    @Column(name = "mobracetype")
    private Long raceType;

    @Column(name = "rank")
    private Integer rank;

    @Column(name = "bloodingresi")
    private Integer bloodingResistance;

    @Column(name = "stunresi")
    private Integer stunResistance;

    @Column(name = "movespeedresi")
    private Integer moveSpeedResistance;

    @Column(name = "fearresi")
    private Integer fearResistance;

    @OneToOne
    @JoinColumn(name = "mob_info_fk")
    private MobInfo mobInfo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInxname() {
        return inxname;
    }

    public void setInxname(String inxname) {
        this.inxname = inxname;
    }

    public Integer getVisible() {
        return visible;
    }

    public void setVisible(Integer visible) {
        this.visible = visible;
    }

    public Integer getDefense() {
        return defense;
    }

    public void setDefense(Integer defense) {
        this.defense = defense;
    }

    public Integer getEvasion() {
        return evasion;
    }

    public void setEvasion(Integer evasion) {
        this.evasion = evasion;
    }

    public Integer getMagicalDefense() {
        return magicalDefense;
    }

    public void setMagicalDefense(Integer magicalDefense) {
        this.magicalDefense = magicalDefense;
    }

    public Integer getMb() {
        return mb;
    }

    public void setMb(Integer mb) {
        this.mb = mb;
    }

    public Long getAggroType() {
        return aggroType;
    }

    public void setAggroType(Long aggroType) {
        this.aggroType = aggroType;
    }

    public Integer getAggroRadius() {
        return aggroRadius;
    }

    public void setAggroRadius(Integer aggroRadius) {
        this.aggroRadius = aggroRadius;
    }

    public Integer getAggroRange() {
        return aggroRange;
    }

    public void setAggroRange(Integer aggroRange) {
        this.aggroRange = aggroRange;
    }

    public Long getExp() {
        return exp;
    }

    public void setExp(Long exp) {
        this.exp = exp;
    }

    public Integer getExpRange() {
        return expRange;
    }

    public void setExpRange(Integer expRange) {
        this.expRange = expRange;
    }

    public Integer getStrength() {
        return strength;
    }

    public void setStrength(Integer strength) {
        this.strength = strength;
    }

    public Integer getDexterity() {
        return dexterity;
    }

    public void setDexterity(Integer dexterity) {
        this.dexterity = dexterity;
    }

    public Integer getEndurance() {
        return endurance;
    }

    public void setEndurance(Integer endurance) {
        this.endurance = endurance;
    }

    public Integer getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(Integer intelligence) {
        this.intelligence = intelligence;
    }

    public Integer getSpirit() {
        return spirit;
    }

    public void setSpirit(Integer spirit) {
        this.spirit = spirit;
    }

    public Long getRaceType() {
        return raceType;
    }

    public void setRaceType(Long raceType) {
        this.raceType = raceType;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Integer getBloodingResistance() {
        return bloodingResistance;
    }

    public void setBloodingResistance(Integer bloodingResistance) {
        this.bloodingResistance = bloodingResistance;
    }

    public Integer getStunResistance() {
        return stunResistance;
    }

    public void setStunResistance(Integer stunResistance) {
        this.stunResistance = stunResistance;
    }

    public Integer getMoveSpeedResistance() {
        return moveSpeedResistance;
    }

    public void setMoveSpeedResistance(Integer moveSpeedResistance) {
        this.moveSpeedResistance = moveSpeedResistance;
    }

    public Integer getFearResistance() {
        return fearResistance;
    }

    public void setFearResistance(Integer fearResistance) {
        this.fearResistance = fearResistance;
    }

    public MobInfo getMobInfo() { return mobInfo; }

    public void setMobInfo(MobInfo mobInfo) { this.mobInfo = mobInfo; }
}
