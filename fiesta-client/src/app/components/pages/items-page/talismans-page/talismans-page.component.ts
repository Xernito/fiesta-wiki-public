import { Component, OnInit, ViewChild } from '@angular/core';
import { ItemsPageTemplateComponent } from '@fiesta-common/components/items-page-template/items-page-template.component';
import { PageDescriptionService } from '@services/page-description.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FiestaSettingsService } from '@fiesta-common/services/fiesta-settings.service';
import { ItemService } from '@fiesta-common/services/item.service';
import { combineLatest } from 'rxjs';
import { switchMap, share, map } from 'rxjs/operators';
import { PageEvent } from '@angular/material/paginator';
import { FormBuilder, FormGroup } from '@angular/forms';
import { LevelRangeFilterComponent } from '@fiesta-common/components/level-range-filter/level-range-filter.component';
import { fiestaMaxLevel } from '@fiesta-common/game-globals';
import { PAGES_NAME } from '@app/global-strings';

@Component({
    selector: 'app-talismans-page',
    templateUrl: './talismans-page.component.html',
    styleUrls: ['./talismans-page.component.scss']
})
export class TalismansPageComponent extends ItemsPageTemplateComponent implements OnInit {

    @ViewChild(LevelRangeFilterComponent, { static: true }) levelRangeForm: LevelRangeFilterComponent;

    showRandomStats = true;

    readonly PAGE_TITLE = PAGES_NAME.talismans;

    filterFormGroup: FormGroup;

    displayedColumns: string[] =
        [
            'icon',
            'name',
            'level',
            'damage',
            'mdamage',
            'defense',
            'mdefense',
            'aim'
        ];

    readonly maxEnhancement = 9;

    constructor(
        protected router: Router,
        protected route: ActivatedRoute,
        private formBuilder: FormBuilder,
        protected pageDescriptionService: PageDescriptionService,
        private itemService: ItemService,
        private fiestaSettings: FiestaSettingsService,
    ) {
        super(router, route);

        this.enhancement = this.maxEnhancement;
    }

    ngOnInit(): void {
        this.pageDescriptionService.setPageDescription({
            title: this.PAGE_TITLE,
            description: 'List of talismans available in Fiesta Online.'
        });

        this.createForm();

        this.fiestaSettings.showRandomStats.subscribe(value => this.showRandomStats = value);

        this.page$ = combineLatest(
            [this.route.paramMap,
            this.route.queryParamMap]
        ).pipe(
            switchMap(([param, queryParam]) => {
                let page = 0;
                let order;
                let search = '';
                let min;
                let max;

                if (param.has('page')) {
                    page = +param.get('page');
                }

                if (queryParam.has('order')) {
                    order = +queryParam.get('order');
                }
                if (queryParam.has('search')) {
                    search = queryParam.get('search');
                }
                if (queryParam.has('min')) {
                    min = +queryParam.get('min');
                }
                if (queryParam.has('max')) {
                    max = +queryParam.get('max');
                }

                if (this.itemTable) {
                    this.itemTable.clear();
                }

                return this.itemService.getTalismans(page, order, search, min, max);
            }),
            share()
        );

        combineLatest(
            [this.route.paramMap,
            this.route.queryParamMap]
        ).pipe(map(([param, queryParam]) => {
            let minLevel = 0;
            let maxLevel = fiestaMaxLevel;

            if (queryParam.has('min')) {
                minLevel = +queryParam.get('min');
            }
            if (queryParam.has('max')) {
                maxLevel = +queryParam.get('max');
            }

            return [minLevel, maxLevel];
        })).subscribe(([minLevel, maxLevel]: [number, number]) => {
            this.filterFormGroup.setValue({ search: '', levelRange: { min: minLevel, max: maxLevel } });
        });
    }

    createForm() {
        this.filterFormGroup = this.formBuilder.group({
            search: null,
            levelRange: this.levelRangeForm.createForm(),
        });
    }

    onSubmit(data: any) {
        this.filter({
            search: data.search ? data.search : null,
            min: data.levelRange.min ? data.levelRange.min : null,
            max: data.levelRange.max ? data.levelRange.max : null
        });
    }

    onPageChange(pageEvent: PageEvent) {
        this.router.navigate(['/talismans', pageEvent.pageIndex], { queryParamsHandling: 'merge' });
    }
}
