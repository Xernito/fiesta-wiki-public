export const commonEnvironment = {
    adsense: {
        AdSlot: '8554739992',
        CaPub: 'ca-pub-6253846474179921'
    },
    wasmAssetsPath: '/assets/wasm',
    discordInviteLink: 'https://discord.gg/YPX2CvFWXE',
    googleAnalyticsTrackingCode: 'UA-119776150-2',
};
