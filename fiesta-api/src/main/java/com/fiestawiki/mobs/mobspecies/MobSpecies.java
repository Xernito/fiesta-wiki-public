package com.fiestawiki.mobs.mobspecies;

import com.fiestawiki.license.dao.data.LicenseData;
import com.fiestawiki.mobs.mobinfo.MobInfo;
import com.fiestawiki.mobs.mobviewinfo.MobViewInfo;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "mob_species")
@EntityListeners(AuditingEntityListener.class)
public class MobSpecies implements Serializable
{
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "species_name")
    private String name;

    @OneToMany(mappedBy = "species")
    @OrderBy("level")
    private List<MobInfo> mobs = new ArrayList<>();

    @OneToMany(mappedBy = "species")
    private List<LicenseData> licenseData = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public List<MobInfo> getMobs() { return mobs; }

    public void setMobs(List<MobInfo> mobs) { this.mobs = mobs; }

    public String getMobName() {

        if(!mobs.isEmpty())
        {
            return mobs.get(0).getName();
        }

        return "Unknown";
    }

    public String getMobPortrait() {

        if(!mobs.isEmpty())
        {
            MobViewInfo mob_view_info = mobs.get(0).getView();
            if(mob_view_info != null)
            {
                return mob_view_info.getMobPortrait();
            }
        }

        return null;
    }

    public List<Long> getMobsIDs()
    {
        return mobs
                .stream()
                .map(m -> m.getId())
                .collect(Collectors.toList());
    }

    public List<LicenseData> getLicenseData() { return licenseData; }

    public void setLicenseData(List<LicenseData> licenseData) { this.licenseData = licenseData; }
}