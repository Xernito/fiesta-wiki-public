package com.fiestawiki.items.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class ItemSetDTO {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("set_name")
    private String setName;

    @JsonProperty("set_items")
    private List<Long> setItems = new ArrayList<>();

    @JsonProperty("set_effects")
    private List<Long> setEffects = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSetName() {
        return setName;
    }

    public void setSetName(String setName) {
        this.setName = setName;
    }

    public List<Long> getSetItems() {
        return setItems;
    }

    public void setSetItems(List<Long> setItems) {
        this.setItems = setItems;
    }

    public List<Long> getSetEffects() {
        return setEffects;
    }

    public void setSetEffects(List<Long> setEffects) {
        this.setEffects = setEffects;
    }
}
