package com.fiestawiki.quest.dao.quest;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class QuestRepositoryImpl implements QuestRepositoryCustom
{
    @PersistenceContext
    EntityManager entityManager;

    CriteriaBuilder criteriaBuilder;

    @PostConstruct
    private void init() {
        criteriaBuilder = entityManager.getCriteriaBuilder();
    }

    @Override
    public List<QuestData> findQuests(int min, int max, Boolean hideEvents, List<Integer> classes)
    {
        CriteriaQuery<QuestData> query = criteriaBuilder.createQuery(QuestData.class);
        {
            Root<QuestData> quest = query.from(QuestData.class);
            query.select(quest);

            List<Predicate> criteria = getCriteriaForQuest(quest, hideEvents, classes);

            criteria.add(criteriaBuilder.notEqual(quest.get(QuestData_.isActive), 0));

            criteria.add(criteriaBuilder.between(quest.get(QuestData_.minLevel), min, max));

            query.where(criteriaBuilder.and(criteria.toArray(new Predicate[0])));
        }

        TypedQuery<QuestData> q = entityManager.createQuery(query);

        return  q.getResultList();
    }

    @Override
    public List<QuestData> getAvailableQuestsForLevel(
            Integer level,
            Optional<Integer> minLevel,
            Boolean hideEvents,
            List<Integer> classes
    )
    {
        CriteriaQuery<QuestData> query = criteriaBuilder.createQuery(QuestData.class);
        {
            Root<QuestData> quest = query.from(QuestData.class);
            query.select(quest);

            List<Predicate> criteria = getCriteriaForQuest(quest, hideEvents, classes);

            criteria.add(criteriaBuilder.notEqual(quest.get(QuestData_.isActive), 0));

            Predicate lowerBoundPredicate =  criteriaBuilder.lessThanOrEqualTo(quest.get(QuestData_.minLevel), level);
            if(minLevel.isPresent())
            {
                lowerBoundPredicate = criteriaBuilder.and(
                        lowerBoundPredicate,
                        criteriaBuilder.greaterThanOrEqualTo(quest.get(QuestData_.minLevel), Math.min(minLevel.get(), level))
                );
            }

            criteria.add(
                criteriaBuilder.and(
                    lowerBoundPredicate,
                    criteriaBuilder.greaterThanOrEqualTo(quest.get(QuestData_.maxLevel), level)
                )
            );

            List<Order> orderList = new ArrayList();
            orderList.add(criteriaBuilder.asc(quest.get(QuestData_.minLevel)));
            orderList.add(criteriaBuilder.asc(quest.get(QuestData_.id)));
            query.orderBy(orderList);

            query.where(criteriaBuilder.and(criteria.toArray(new Predicate[0])));
        }

        TypedQuery<QuestData> q = entityManager.createQuery(query);

        return  q.getResultList();
    }

    private List<Predicate> getCriteriaForQuest(
            Root<QuestData> quest,
            Boolean hideEvents,
            List<Integer> classes)
    {
        List<Predicate> criteria = new ArrayList<>();

        if(!classes.isEmpty())
        {
            criteria.add(criteriaBuilder.or(
                    criteriaBuilder.equal(quest.get(QuestData_.startClass), 0),
                    quest.get(QuestData_.startClass).in(classes))
            );
        }
        else
        {
            criteria.add(criteriaBuilder.or(criteriaBuilder.equal(quest.get(QuestData_.startClass), 0)));
        }

        // if the user don't want to show events
        if(hideEvents)
        {
            criteria.add(criteriaBuilder.notEqual(quest.get(QuestData_.questType), 3));
        }

        return criteria;
    }
}
