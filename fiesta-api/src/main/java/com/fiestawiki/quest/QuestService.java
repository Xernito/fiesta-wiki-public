package com.fiestawiki.quest;

import com.fiestawiki.items.dao.info.repository.ItemInfoRepository;
import com.fiestawiki.levelinfo.LevelInfo;
import com.fiestawiki.levelinfo.LevelInfoRepository;
import com.fiestawiki.quest.dao.dialog.QuestDialog;
import com.fiestawiki.quest.dao.dialog.QuestDialogRepository;
import com.fiestawiki.quest.dao.endItem.QuestEndItem;
import com.fiestawiki.quest.dao.endItem.QuestEndItemDTO;
import com.fiestawiki.quest.dao.endNpc.QuestEndNpc;
import com.fiestawiki.quest.dao.endNpc.QuestEndNpcService;
import com.fiestawiki.quest.dao.endNpc.QuestMobKillDTO;
import com.fiestawiki.quest.dao.questlevelinfo.QuestLevelInfoDTO;
import com.fiestawiki.quest.dto.QuestDTO;
import com.fiestawiki.quest.dao.quest.QuestData;
import com.fiestawiki.quest.dao.quest.QuestRepository;
import com.fiestawiki.quest.dao.questitemdropinfo.QuestItemDropInfo;
import com.fiestawiki.quest.dao.questitemdropinfo.QuestItemDropInfoDTO;
import com.fiestawiki.quest.dao.questitemdropinfo.QuestItemDropInfoRepository;
import com.fiestawiki.quest.dao.reward.ItemRewardDTO;
import com.fiestawiki.quest.dao.reward.QuestReward;
import com.fiestawiki.quest.dto.QuestDataDTO;
import org.modelmapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class QuestService {

    @Autowired
    private QuestDialogRepository dialogRepository;

    @Autowired
    private QuestRepository questRepository;

    @Autowired
    private ItemInfoRepository itemInfoRepository;

    @Autowired
    private QuestEndNpcService questEndNpcService;

    @Autowired
    private QuestItemDropInfoRepository questItemDropInfoRepository;

    @Autowired
    private LevelInfoRepository levelInfoRepository;

    @Autowired
    private ModelMapper modelMapper;

    @PostConstruct
    public void postConstruct()
    {
        ///
        Converter<List<QuestEndNpc> , Long> endNpcToRewardNpc = new AbstractConverter<List<QuestEndNpc>, Long>() {
            public Long convert(List<QuestEndNpc> src) {
                for (QuestEndNpc q: src) {
                    if(q.getNpc_mob_action_type() == 0) {
                        return q.getMob().getId();
                    }
                }
                return 0L;
            }
        };

        ///
        Converter<List<QuestEndNpc>, List<QuestMobKillDTO>> endNpcToQuestMobs = new AbstractConverter<List<QuestEndNpc>, List<QuestMobKillDTO>>()
        {
            public List<QuestMobKillDTO> convert(List<QuestEndNpc> src)
            {
                return src
                        .stream()
                        .filter(q -> q.getNpc_mob_action_type() == 1 && q.getIsEnabled())
                        .map(q -> questEndNpcService.convertQuestMobKillDTO(q))
                        .collect(Collectors.toList());
            }
        };

        ///
        Converter<List<QuestEndItem> , List<QuestEndItemDTO>> itemGatheringToDTO = new AbstractConverter<List<QuestEndItem>, List<QuestEndItemDTO>>() {
            public List<QuestEndItemDTO> convert(List<QuestEndItem> src)
            {
                return src
                        .stream()
                        .map(i -> modelMapper.map(i, QuestEndItemDTO.class))
                        .collect(Collectors.toList());
            }
        };

        modelMapper.emptyTypeMap(QuestReward.class, ItemRewardDTO.class).addMappings(mapper -> {
            mapper.map(QuestReward::getSelectable, ItemRewardDTO::setSelectable);
            mapper.map(QuestReward::getUnkcol0, ItemRewardDTO::setQuantity);
            mapper.map(QuestReward::getFlag, ItemRewardDTO::setItemId);
        });

        ///
        TypeMap<QuestData, QuestDataDTO> typeMap =
                modelMapper.createTypeMap(QuestData.class, QuestDataDTO.class);

        typeMap.addMappings( mapper -> {
            mapper.using(endNpcToRewardNpc).map(QuestData::getEndNpc,
                    QuestDataDTO::setEndNpcId);
            mapper.using(endNpcToQuestMobs).map(QuestData::getEndNpc,
                    QuestDataDTO::setMobKill);
            mapper.using(itemGatheringToDTO).map(QuestData::getItemGathering,
                    QuestDataDTO::setItemGathering);
        });

        modelMapper.emptyTypeMap(QuestItemDropInfo.class, QuestItemDropInfoDTO.class).addMappings(mapper -> {
            mapper.map(QuestItemDropInfo::getMobId, QuestItemDropInfoDTO::setMobId);
            mapper.map(QuestItemDropInfo::getPercent, QuestItemDropInfoDTO::setPercent);
            mapper.map(QuestItemDropInfo::getCountMin, QuestItemDropInfoDTO::setCountMin);
            mapper.map(QuestItemDropInfo::getCountMax, QuestItemDropInfoDTO::setCountMax);
        });
    }

    private QuestDTO convertQuestDTO(QuestData quest)
    {
        return modelMapper.map(quest, QuestDTO.class);
    }

    private QuestDataDTO convertQuestDataDTO(QuestData quest)
    {
        return modelMapper.map(quest, QuestDataDTO.class);
    }

    private QuestItemDropInfoDTO convertQuestItemDropInfo(QuestItemDropInfo drop_item_info)
    {
        return modelMapper.map(drop_item_info, QuestItemDropInfoDTO.class);
    }

    public QuestDialog getDialog(Long id) throws Exception
    {
        return dialogRepository
                .findById(id)
                .orElseThrow(() -> new Exception("Dialog not found"));
    }

    public QuestDTO getQuest(Long id) throws Exception
    {
        QuestData quest = questRepository
                .findById(id)
                .orElseThrow(() -> new Exception("Quest not found"));

        return convertQuestDTO(quest);
    }

    public QuestDataDTO getQuestData(Long id) throws Exception
    {
        QuestData quest = questRepository
                .findById(id)
                .orElseThrow(() -> new Exception("Quest not found"));

        return convertQuestDataDTO(quest);
    }

    private List<Integer> getClasses(int iClass)
    {
        // QuestData StartClassID
        // Fighter 1
        // CleverFighter 2
        // Warrior 3
        // Gladiator 4
        // Knight 5
        // Cleric 6
        // HighCleric 7
        // Paladin 8
        // HolyKnight 9
        // Guardian 10
        // Archer 11
        // HawkArcher 12
        // Scout 13
        // SharpShooter 14
        // Ranger 15
        // Mage 16
        // WizMage 17
        // Enchanter 18
        // Warlock 19
        // Wizard 20
        // Joker 21
        // Chaser 22
        // Cruel 23
        // Closer 24
        // Assassin 25
        // Crusader 26
        // Templar 27

        if(iClass == 1)
            return Arrays.asList(1,2,3,4,5);
        if(iClass == 2)
            return Arrays.asList(6,7,8,9,10);
        if(iClass == 3)
            return Arrays.asList(11,12,13,14,15);
        if(iClass == 4)
            return Arrays.asList(16,17,18,19,20);
        if(iClass == 5)
            return Arrays.asList(21,22,23,24,25);
        if(iClass == 6)
            return Arrays.asList(26,27);

        return new ArrayList<>();
    }


    public QuestLevelInfoDTO getQuestsByLevel(Integer level, Optional<Integer> minLevel)
    {
        List<QuestData> available_quests =
                questRepository.getAvailableQuestsForLevel(
                        level,
                        minLevel,
                        true,
                        new ArrayList<>()
                );

        available_quests =
                available_quests
                    .stream()
                    .sorted((q1, q2) -> {
                        if (q1.getMaxLevel() == q2.getMaxLevel())
                            return 0;
                        else if (q1.getMaxLevel() == level)
                            return -1;
                        else if (q2.getMaxLevel() == level)
                            return 1;
                        else if (q1.getMinLevel() == q2.getMinLevel())
                            return q1.getMaxLevel() - q2.getMaxLevel();
                        else
                            return q1.getMinLevel() - q2.getMinLevel();
                    })
                    .collect(Collectors.toList());

        QuestLevelInfoDTO quest_level_info = new QuestLevelInfoDTO();

        Optional<LevelInfo> level_info = levelInfoRepository.findById(new Long(level));

        quest_level_info.setLevel(new Long(level));
        if(level_info.isPresent())
        {
            quest_level_info.setXpRequired(level_info.get().getXpRequired());
        }

        int finalLvl = level;
        quest_level_info.setTotalXp(
                available_quests
                        .stream()
                        .mapToLong(QuestData::getExp)
                        .sum());

        quest_level_info.setQuests(
                available_quests
                        .stream()
                        .map(q -> q.getId())
                        .collect(Collectors.toList())
        );

        return quest_level_info;
    }

    public List<QuestLevelInfoDTO> getQuestsPage(
            int start,
            int range,
            Boolean hideEvents,
            Optional<Integer> iClass
    ) throws Exception
    {
        if(range < 1)
            range = 1;
        else if(range > 140)
            range = 140;

        if(start < 0)
            start = 0;
        else if((start + range - 1) > 140)
            range = 140 - start + 1;

        List<QuestLevelInfoDTO> level_quests = new ArrayList<>();

        List<Integer> classes = new ArrayList<>();
        if(iClass.isPresent())
        {
            classes = getClasses(iClass.get());
        }

        for(int lvl = start; lvl < start + range; lvl++)
        {
            List<QuestData> quests = questRepository.findQuests(lvl, lvl, hideEvents, classes);

            QuestLevelInfoDTO quest_level_info = new QuestLevelInfoDTO();

            Optional<LevelInfo> level_info = levelInfoRepository.findById(new Long(lvl));

            quest_level_info.setLevel(new Long(lvl));
            if(level_info.isPresent())
            {
                quest_level_info.setXpRequired(level_info.get().getXpRequired());
            }

            quest_level_info.setTotalXp(
                    quests
                            .stream()
                            .mapToLong(QuestData::getExp)
                            .sum());

            quest_level_info.setQuests(
                    quests
                            .stream()
                            .map(q -> q.getId())
                            .collect(Collectors.toList())
            );

            level_quests.add(quest_level_info);
        }

        return level_quests;
    }
}
