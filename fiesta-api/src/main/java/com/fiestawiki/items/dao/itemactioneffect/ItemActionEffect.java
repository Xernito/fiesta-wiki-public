package com.fiestawiki.items.dao.itemactioneffect;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "item_action_effect")
@EntityListeners(AuditingEntityListener.class)
public class ItemActionEffect implements Serializable {

    @Id
    @Column(name = "unique_id")
    private Long uniqueId;

    @Column(name = "effectid")
    private int effectId;

    @Type(type = "list-array")
    @Column(
            name = "effecttarget",
            columnDefinition = "integer[]"
    )
    private List<Integer> effectTarget;

    @Type(type = "list-array")
    @Column(
            name = "effectactivity",
            columnDefinition = "integer[]"
    )
    private List<Integer> effectActivity;

    @Column(name = "value")
    private int value;

    @Column(name = "area")
    private int area;

    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public int getEffectId() {
        return effectId;
    }

    public void setEffectId(int effectId) {
        this.effectId = effectId;
    }

    public List<Integer> getEffectTarget() {
        return effectTarget;
    }

    public void setEffectTarget(List<Integer> effectTarget) {
        this.effectTarget = effectTarget;
    }

    public List<Integer> getEffectActivity() {
        return effectActivity;
    }

    public void setEffectActivity(List<Integer> effectActivity) {
        this.effectActivity = effectActivity;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }
}
