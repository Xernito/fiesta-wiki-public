package com.fiestawiki.skills.dao.multihitskill;

import com.fiestawiki.skills.dao.activeskill.ActiveSkill;
import com.fiestawiki.states.abstate.AbState;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "multi_hit_type")
@EntityListeners(AuditingEntityListener.class)
public class MultiHitSkill
{
    @Id
    @Column(name = "unique_id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "abstate_fk")
    private AbState abState;

    @Column(name = "as_step")
    private short asStep;

    @Column(name = "abstr")
    private Long abStr;

    @Column(name = "abrate")
    private short abRate;

    @Column(name = "dmgrate")
    private short dmgRate;

    @ManyToMany
    @JoinTable(
            name = "multi_hit_join",
            joinColumns = @JoinColumn(name = "pk1"),
            inverseJoinColumns = @JoinColumn(name = "pk2"))
    private List<ActiveSkill> multiHit;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AbState getAbState() {
        return abState;
    }

    public void setAbState(AbState abState) {
        this.abState = abState;
    }

    public short getAsStep() {
        return asStep;
    }

    public void setAsStep(short asStep) {
        this.asStep = asStep;
    }

    public Long getAbStr() {
        return abStr;
    }

    public void setAbStr(Long abStr) {
        this.abStr = abStr;
    }

    public short getAbRate() {
        return abRate;
    }

    public void setAbRate(short abRate) {
        this.abRate = abRate;
    }

    public short getDmgRate() {
        return dmgRate;
    }

    public void setDmgRate(short dmgRate) {
        this.dmgRate = dmgRate;
    }
}