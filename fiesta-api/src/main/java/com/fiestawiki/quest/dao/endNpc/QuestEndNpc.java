package com.fiestawiki.quest.dao.endNpc;

import com.fiestawiki.mobs.mobinfo.MobInfo;
import com.fiestawiki.quest.dao.quest.QuestData;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "quest_end_npc")
@EntityListeners(AuditingEntityListener.class)
public class QuestEndNpc {

    @Id
    @Column(name = "unique_id", nullable = false)
    private Long unique_id;

    @Column (
            name = "id",
            nullable = false,
            insertable = false,
            updatable = false
    )
    private Long id;

    @Column(name = "isenabled")
    private int isEnabled;

    @ManyToOne
    @JoinColumn(name = "mobid")
    private MobInfo mob;

    @Column (name = "npcmobactiontype")
    private int npc_mob_action_type;

    @Column (name = "count")
    private int count;

    @ManyToOne
    @JoinColumn(name = "quest_id")
    private QuestData quest;

    public Long getUnique_id() { return unique_id; }

    public void setUnique_id(Long unique_id) { this.unique_id = unique_id; }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public boolean getIsEnabled() { return isEnabled == 1; }

    public void setIsEnabled(int isEnabled) { this.isEnabled = isEnabled; }

    public MobInfo getMob() { return mob; }

    public void setMob(MobInfo mob) { this.mob = mob; }

    public int getNpc_mob_action_type() {
        return npc_mob_action_type;
    }

    public void setNpc_mob_action_type(int npc_mob_action_type) {
        this.npc_mob_action_type = npc_mob_action_type;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public QuestData getQuest() { return quest; }

    public void setQuest(QuestData quest) { this.quest = quest; }
}
