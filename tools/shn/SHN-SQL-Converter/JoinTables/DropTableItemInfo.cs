﻿namespace SHN_SQL_Converter.JoinTables
{
    class DropTableItemInfo : JoinTableBase
    {
        public override void Create()
        {
            Table = Utils.CreateJoinTable(
                DataSet,
                DataSet.Tables["item_info_server"],
                DataSet.Tables["item_drop_group"],
                Tablename,
                "public"
                );
        }

        public override void Load()
        {
            Utils.AddJoinColumn<string>(
                DataSet,
                DataSet.Tables["item_info_server"],
                DataSet.Tables["item_drop_group"],
                "DropGroupA",
                "ItemID",
                Tablename
                );
            Utils.AddJoinColumn<string>(
                DataSet,
                DataSet.Tables["item_info_server"],
                DataSet.Tables["item_drop_group"],
                "DropGroupB",
                "ItemID",
                Tablename
                );
        }
    }
}
