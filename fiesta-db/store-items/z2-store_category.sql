--
-- PostgreSQL database dump
--

-- Dumped from database version 12.3 (Debian 12.3-1.pgdg100+1)
-- Dumped by pg_dump version 12.3 (Debian 12.3-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: store_category; Type: TABLE; Schema: store_item; Owner: docker
--

CREATE TABLE store_item.store_category (
    id bigint NOT NULL,
    name character varying(255),
    type integer
);


ALTER TABLE store_item.store_category OWNER TO docker;

--
-- Name: store_category_id_seq; Type: SEQUENCE; Schema: store_item; Owner: docker
--

CREATE SEQUENCE store_item.store_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE store_item.store_category_id_seq OWNER TO docker;

--
-- Name: store_category_id_seq; Type: SEQUENCE OWNED BY; Schema: store_item; Owner: docker
--

ALTER SEQUENCE store_item.store_category_id_seq OWNED BY store_item.store_category.id;


--
-- Name: store_category id; Type: DEFAULT; Schema: store_item; Owner: docker
--

ALTER TABLE ONLY store_item.store_category ALTER COLUMN id SET DEFAULT nextval('store_item.store_category_id_seq'::regclass);


--
-- Data for Name: store_category; Type: TABLE DATA; Schema: store_item; Owner: docker
--

INSERT INTO store_item.store_category (id, name, type) VALUES (1, '1 day', 0);
INSERT INTO store_item.store_category (id, name, type) VALUES (2, '7 days', 0);
INSERT INTO store_item.store_category (id, name, type) VALUES (3, '30 days', 0);
INSERT INTO store_item.store_category (id, name, type) VALUES (4, 'Permanent', 0);
INSERT INTO store_item.store_category (id, name, type) VALUES (5, 'Head', 2);
INSERT INTO store_item.store_category (id, name, type) VALUES (6, 'Stats+', 1);
INSERT INTO store_item.store_category (id, name, type) VALUES (7, 'Critical', 1);
INSERT INTO store_item.store_category (id, name, type) VALUES (8, 'Aim', 1);
INSERT INTO store_item.store_category (id, name, type) VALUES (9, 'Evasion', 1);
INSERT INTO store_item.store_category (id, name, type) VALUES (10, 'Damage', 1);
INSERT INTO store_item.store_category (id, name, type) VALUES (11, 'Defense', 1);
INSERT INTO store_item.store_category (id, name, type) VALUES (12, 'Face', 2);
INSERT INTO store_item.store_category (id, name, type) VALUES (13, 'Stargazer01', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (14, 'Costume', 2);
INSERT INTO store_item.store_category (id, name, type) VALUES (15, 'Top', 2);
INSERT INTO store_item.store_category (id, name, type) VALUES (16, 'Bottom', 2);
INSERT INTO store_item.store_category (id, name, type) VALUES (17, 'Boots', 2);
INSERT INTO store_item.store_category (id, name, type) VALUES (18, 'Baktwerel', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (19, 'Back', 2);
INSERT INTO store_item.store_category (id, name, type) VALUES (20, 'Norika', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (21, 'MissTay', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (22, 'Amely', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (23, 'Fruity', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (24, 'Hellacious', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (25, 'Rosenight', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (26, 'NeoSilverDragoon', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (27, 'Excellence', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (28, 'Onigiri', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (29, 'Zephyr-Aryn', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (30, 'Hana', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (31, 'Laviie', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (32, 'CrackedDoll', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (33, 'Swifty', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (34, 'Immortalrae', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (35, 'MintyGreens', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (36, '90 days', 0);
INSERT INTO store_item.store_category (id, name, type) VALUES (37, 'Chiichen', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (38, 'Luna_Goddess', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (39, 'Tail', 2);
INSERT INTO store_item.store_category (id, name, type) VALUES (40, 'Mouth', 2);
INSERT INTO store_item.store_category (id, name, type) VALUES (41, 'Eloren', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (42, 'ArumLily', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (43, 'Samsalano', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (44, 'StealthElfJade', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (45, 'Nyaneko', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (46, 'FierceFang', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (47, '15 days', 0);
INSERT INTO store_item.store_category (id, name, type) VALUES (48, 'Misuko', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (49, 'Darkya', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (50, 'MysticalDragon', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (51, 'Flauschball', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (52, 'Eemi', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (53, 'Dovahcaine', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (54, 'Bianca', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (55, 'Amari', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (56, 'Sillithbloomie', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (57, 'Shambles', 3);
INSERT INTO store_item.store_category (id, name, type) VALUES (58, 'Ladany', 3);


--
-- Name: store_category_id_seq; Type: SEQUENCE SET; Schema: store_item; Owner: docker
--

SELECT pg_catalog.setval('store_item.store_category_id_seq', 58, true);


--
-- Name: store_category store_category_pkey; Type: CONSTRAINT; Schema: store_item; Owner: docker
--

ALTER TABLE ONLY store_item.store_category
    ADD CONSTRAINT store_category_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

