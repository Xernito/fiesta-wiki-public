import { Component, OnInit, Output, EventEmitter, Input, ViewEncapsulation } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';

@Component({
    selector: 'app-class-combobox',
    templateUrl: './class-combobox.component.html',
    styleUrls: ['./class-combobox.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ClassComboboxComponent implements OnInit {

    @Input()
    defaultValue = 1;

    @Output() classSelected = new EventEmitter<number>();

    constructor() { }

    ngOnInit() {
    }

    onClassChanged(change: MatSelectChange) {

        this.classSelected.emit(change.value);
    }
}
