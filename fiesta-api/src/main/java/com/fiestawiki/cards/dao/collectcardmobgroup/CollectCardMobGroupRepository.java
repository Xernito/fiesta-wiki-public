package com.fiestawiki.cards.dao.collectcardmobgroup;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CollectCardMobGroupRepository extends JpaRepository<CollectCardMobGroup, Long> {
}
