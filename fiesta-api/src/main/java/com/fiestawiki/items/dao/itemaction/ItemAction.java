package com.fiestawiki.items.dao.itemaction;

import com.fiestawiki.items.dao.itemactioncondition.ItemActionCondition;
import com.fiestawiki.items.dao.itemactioneffect.ItemActionEffect;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "item_action")
@EntityListeners(AuditingEntityListener.class)
public class ItemAction {

    @Id
    @Column(name = "unique_id")
    private Long unique_id;

    @Column(name = "itemactionid")
    private int itemActionId;

    @ManyToOne
    @JoinColumn(name = "conditionid")
    private ItemActionCondition condition;

    @ManyToOne
    @JoinColumn(name = "item_action_effect_id")
    private ItemActionEffect effect;

    @Column(name = "cooltime")
    private int cooltime;

    @Column(name = "coolgroupactionid")
    private int cooltimeGroupActionId;

    public Long getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(Long unique_id) {
        this.unique_id = unique_id;
    }

    public int getItemActionId() {
        return itemActionId;
    }

    public void setItemActionId(int itemActionId) {
        this.itemActionId = itemActionId;
    }

    public ItemActionCondition getCondition() { return condition; }

    public void setCondition(ItemActionCondition condition) { this.condition = condition; }

    public ItemActionEffect getEffect() {
        return effect;
    }

    public void setEffect(ItemActionEffect effect) {
        this.effect = effect;
    }

    public int getCooltime() {
        return cooltime;
    }

    public void setCooltime(int cooltime) {
        this.cooltime = cooltime;
    }

    public int getCooltimeGroupActionId() {
        return cooltimeGroupActionId;
    }

    public void setCooltimeGroupActionId(int cooltimeGroupActionId) {
        this.cooltimeGroupActionId = cooltimeGroupActionId;
    }
}
