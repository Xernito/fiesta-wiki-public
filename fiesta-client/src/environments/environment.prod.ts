import { commonEnvironment } from './environment.common';

export const environment = {
    common: commonEnvironment,
    locale: {
        version: 'na',
        language: 'en',
        title: 'Fiesta Wiki (NA)',
        flag: 'flag-us'
    },
    production: true,
    debug: false,
    domain: '//fiesta-wiki.com/',
    api: '//api.fiesta-wiki.com/',
    static_assets: '//static.fiesta-wiki.com/'
};
