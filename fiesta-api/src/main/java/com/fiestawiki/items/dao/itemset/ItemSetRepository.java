package com.fiestawiki.items.dao.itemset;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemSetRepository extends JpaRepository<ItemSet, Long> {
}
