﻿using System.Data;
using System.Linq;

namespace SHN_SQL_Converter.Files
{
    class QuestAction : SHNFileBase
    {
        public override void Patch(string patch_folder)
        {
            /////////////////////////////////////////////////////////////////////////////////
            // Remove when quest unavailable
            (from row in Table.AsEnumerable()
             select row["ID"])
                        .Except(from row in DataSet.Tables["quest_data"].AsEnumerable()
                                select row["ID"])
                        .ToList()
                        .ForEach(id =>
                        {
                            Table.Select(string.Format("ID = {0}", (ushort)id)).ToList().ForEach(row => Table.Rows.Remove(row));
                        });
            /////////////////////////////////////////////////////////////////////////////////

            /////////////////////////////////////////////////////////////////////////////////
            // Mob Info
            DataTable mob_info_table = DataSet.Tables["mob_info"];

            // rename because SQL 'Group' keyword is reserved
            Table.Columns["Group"].ColumnName = "group_id";

            // Get all mobs IDs
            var mobs_id =
                (from mobs in mob_info_table.AsEnumerable()
                 select mobs["ID"]);

            // Remove QuestAction that have invalid mobID (ConditionTarget)
            (from q in Table.AsEnumerable()
             where !mobs_id.Contains(q["ConditionTarget"])
             select q)
             .ToList()
             .ForEach(r => Table.Rows.Remove(r));
            /////////////////////////////////////////////////////////////////////////////////
        }

        public override void CreateConstraints()
        {
            Utils.AddForeignKeyColumn(
                DataSet,
                Tablename, new string[] { "ID", "ResultTarget" },
                "quest_end_item", new string[] { "ID", "ItemID" },
                "quest_item_fk",
                false);
            Utils.AddForeignKey(
                DataSet,
                Tablename, "ConditionTarget",
                "mob_info",
                "mob_info_fk"
                );
            Utils.AddForeignKey(
                DataSet,
                Tablename, "ID",
                "quest_data",
                "quest_data_fk"
                );
        }
    }
}
