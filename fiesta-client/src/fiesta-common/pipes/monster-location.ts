import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
   name: 'mobLocation'
})
export class MonsterLocationPipe implements PipeTransform {

   transform(location: number): string {
      switch (location) {
         case 0:
            return 'Unknown';
         case 1:
            return 'General Monster';
         default:
            return 'Unknown';
      }
   }
}