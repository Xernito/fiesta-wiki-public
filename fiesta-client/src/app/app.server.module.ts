import { NgModule } from '@angular/core';
import { ServerModule } from '@angular/platform-server';

import { AppModule } from './app.module';
import { AppComponent } from './components/app.component';
import { BrowserModule } from '@angular/platform-browser';
import { FlexLayoutServerModule } from '@angular/flex-layout/server';

@NgModule({
  imports: [
    AppModule,
    ServerModule,
    FlexLayoutServerModule,
    BrowserModule.withServerTransition({ appId: 'fiesta' })
  ],
  bootstrap: [AppComponent],
})
export class AppServerModule { }
