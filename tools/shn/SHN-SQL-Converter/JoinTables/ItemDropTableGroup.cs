﻿namespace SHN_SQL_Converter.JoinTables
{
    class ItemDropTableGroup : JoinTableBase
    {
        public override void Create()
        {
            Table = Utils.CreateJoinTable(
                DataSet,
                DataSet.Tables["item_drop_table"],
                DataSet.Tables["item_drop_group"],
                Tablename,
                "public",
                true
                );
        }

        public override void Load()
        {
            var table = DataSet.Tables["item_drop_table"];

            var columns = table.Columns;

            Table.Columns.Add("DropRate", columns["DrItem1R"].DataType);
            Table.Columns.Add("UpGradeMin", columns["UpGradeMin01"].DataType);
            Table.Columns.Add("UpGradeMax", columns["UpGradeMax01"].DataType);
            Table.Columns.Add("Rule", columns["Rule1"].DataType);
            Table.Columns.Add("Num", columns["Num1"].DataType);


            // Generate join column for the 41 drops per mob
            // 42-45 being for Event items
            for (int i = 1; i <= 41; i++)
            {
                Utils.AddJoinColumn<string>(
                    DataSet,
                    DataSet.Tables["item_drop_table"],
                    DataSet.Tables["item_drop_group"],
                    string.Format("DrItem{0}", i),
                    "ItemGroupIdx",
                    Tablename,
                    new string[]
                    {
                        string.Format("DrItem{0}R", i),
                        string.Format("UpGradeMin{0:D2}", i),
                        string.Format("UpGradeMax{0:D2}", i),
                        string.Format("Rule{0}", i),
                        string.Format("Num{0}", i)
                    },
                    new string[] { "DropRate", "UpGradeMin", "UpGradeMax", "Rule", "Num" }
                    );
            }
        }
    }
}
