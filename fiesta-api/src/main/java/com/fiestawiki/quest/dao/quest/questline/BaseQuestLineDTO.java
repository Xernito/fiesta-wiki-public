package com.fiestawiki.quest.dao.quest.questline;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BaseQuestLineDTO
{
    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;

    public BaseQuestLineDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
