CREATE TABLE "public"."upgrade_info_bracelets"(
 ID  int UNIQUE PRIMARY KEY,
 InxName  VARCHAR(2000),
 UpFactor  bigint,
 BRAccUpdata  int,
 UnkCol0  int,
 UnkCol1  int,
 UnkCol2  int,
 UnkCol3  int,
 UnkCol4  int,
 UnkCol5  int,
 UnkCol6  int,
 UnkCol7  int,
 UnkCol8  int,
 UnkCol9  int,
 UnkCol10  int,
 UnkCol11  int,
 UnkCol12  int,
 UnkCol13  int,
 UnkCol14  int,
 UnkCol15  int,
 UnkCol16  int,
 UnkCol17  int,
 UnkCol18  int,
 UnkCol19  int
);
INSERT INTO "public"."upgrade_info_bracelets"(ID, InxName, UpFactor, BRAccUpdata, UnkCol0, UnkCol1, UnkCol2, UnkCol3, UnkCol4, UnkCol5, UnkCol6, UnkCol7, UnkCol8, UnkCol9, UnkCol10, UnkCol11, UnkCol12, UnkCol13, UnkCol14, UnkCol15, UnkCol16, UnkCol17, UnkCol18, UnkCol19) VALUES (0, E'Grp1CritBijouBra', 8, 1100, 1500, 2000, 2500, 3000, 3500, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."upgrade_info_bracelets"(ID, InxName, UpFactor, BRAccUpdata, UnkCol0, UnkCol1, UnkCol2, UnkCol3, UnkCol4, UnkCol5, UnkCol6, UnkCol7, UnkCol8, UnkCol9, UnkCol10, UnkCol11, UnkCol12, UnkCol13, UnkCol14, UnkCol15, UnkCol16, UnkCol17, UnkCol18, UnkCol19) VALUES (1, E'Grp2CritBijouBra', 8, 1100, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."upgrade_info_bracelets"(ID, InxName, UpFactor, BRAccUpdata, UnkCol0, UnkCol1, UnkCol2, UnkCol3, UnkCol4, UnkCol5, UnkCol6, UnkCol7, UnkCol8, UnkCol9, UnkCol10, UnkCol11, UnkCol12, UnkCol13, UnkCol14, UnkCol15, UnkCol16, UnkCol17, UnkCol18, UnkCol19) VALUES (2, E'Grp3CritBijouBra', 8, 1100, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 0, 0, 0, 0);
INSERT INTO "public"."upgrade_info_bracelets"(ID, InxName, UpFactor, BRAccUpdata, UnkCol0, UnkCol1, UnkCol2, UnkCol3, UnkCol4, UnkCol5, UnkCol6, UnkCol7, UnkCol8, UnkCol9, UnkCol10, UnkCol11, UnkCol12, UnkCol13, UnkCol14, UnkCol15, UnkCol16, UnkCol17, UnkCol18, UnkCol19) VALUES (3, E'Grp1ACBijouBra', 0, 175, 350, 525, 700, 875, 1050, 1225, 1400, 1575, 1750, 1925, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."upgrade_info_bracelets"(ID, InxName, UpFactor, BRAccUpdata, UnkCol0, UnkCol1, UnkCol2, UnkCol3, UnkCol4, UnkCol5, UnkCol6, UnkCol7, UnkCol8, UnkCol9, UnkCol10, UnkCol11, UnkCol12, UnkCol13, UnkCol14, UnkCol15, UnkCol16, UnkCol17, UnkCol18, UnkCol19) VALUES (4, E'Grp2ACBijouBra', 0, 175, 350, 525, 700, 875, 1050, 1225, 1400, 1575, 1750, 1925, 2100, 2275, 2450, 2625, 2800, 0, 0, 0, 0, 0);
INSERT INTO "public"."upgrade_info_bracelets"(ID, InxName, UpFactor, BRAccUpdata, UnkCol0, UnkCol1, UnkCol2, UnkCol3, UnkCol4, UnkCol5, UnkCol6, UnkCol7, UnkCol8, UnkCol9, UnkCol10, UnkCol11, UnkCol12, UnkCol13, UnkCol14, UnkCol15, UnkCol16, UnkCol17, UnkCol18, UnkCol19) VALUES (5, E'Grp3ACBijouBra', 0, 175, 350, 525, 700, 875, 1050, 1225, 1400, 1575, 1750, 1925, 2100, 2275, 2450, 2625, 2800, 2975, 3150, 3325, 3500, 3700);
INSERT INTO "public"."upgrade_info_bracelets"(ID, InxName, UpFactor, BRAccUpdata, UnkCol0, UnkCol1, UnkCol2, UnkCol3, UnkCol4, UnkCol5, UnkCol6, UnkCol7, UnkCol8, UnkCol9, UnkCol10, UnkCol11, UnkCol12, UnkCol13, UnkCol14, UnkCol15, UnkCol16, UnkCol17, UnkCol18, UnkCol19) VALUES (6, E'Grp1MRBijouBra', 4, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO "public"."upgrade_info_bracelets"(ID, InxName, UpFactor, BRAccUpdata, UnkCol0, UnkCol1, UnkCol2, UnkCol3, UnkCol4, UnkCol5, UnkCol6, UnkCol7, UnkCol8, UnkCol9, UnkCol10, UnkCol11, UnkCol12, UnkCol13, UnkCol14, UnkCol15, UnkCol16, UnkCol17, UnkCol18, UnkCol19) VALUES (7, E'Grp2MRBijouBra', 4, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 0, 0, 0, 0, 0);
INSERT INTO "public"."upgrade_info_bracelets"(ID, InxName, UpFactor, BRAccUpdata, UnkCol0, UnkCol1, UnkCol2, UnkCol3, UnkCol4, UnkCol5, UnkCol6, UnkCol7, UnkCol8, UnkCol9, UnkCol10, UnkCol11, UnkCol12, UnkCol13, UnkCol14, UnkCol15, UnkCol16, UnkCol17, UnkCol18, UnkCol19) VALUES (8, E'Grp3MRBijouBra', 4, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2100);
INSERT INTO "public"."upgrade_info_bracelets"(ID, InxName, UpFactor, BRAccUpdata, UnkCol0, UnkCol1, UnkCol2, UnkCol3, UnkCol4, UnkCol5, UnkCol6, UnkCol7, UnkCol8, UnkCol9, UnkCol10, UnkCol11, UnkCol12, UnkCol13, UnkCol14, UnkCol15, UnkCol16, UnkCol17, UnkCol18, UnkCol19) VALUES (9, E'Grp3WC_MABijouBra', 6, 450, 900, 1350, 1800, 2250, 2700, 3150, 3600, 4050, 4500, 4950, 5400, 5850, 6300, 6750, 7200, 7650, 8100, 8550, 9000, 9450);
INSERT INTO "public"."upgrade_info_bracelets"(ID, InxName, UpFactor, BRAccUpdata, UnkCol0, UnkCol1, UnkCol2, UnkCol3, UnkCol4, UnkCol5, UnkCol6, UnkCol7, UnkCol8, UnkCol9, UnkCol10, UnkCol11, UnkCol12, UnkCol13, UnkCol14, UnkCol15, UnkCol16, UnkCol17, UnkCol18, UnkCol19) VALUES (10, E'Grp3AC_MRBijouBra', 7, 175, 350, 525, 700, 875, 1050, 1225, 1400, 1575, 1750, 1925, 2100, 2275, 2450, 2625, 2800, 2975, 3150, 3325, 3500, 3700);
