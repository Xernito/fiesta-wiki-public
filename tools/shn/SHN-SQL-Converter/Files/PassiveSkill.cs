﻿using iQuest;
using System.Data;
using System.IO;

namespace SHN_SQL_Converter.Files
{
    class PassiveSkill : SHNFileBase
    {
        public override void Load(string input_folder)
        {
            SHNFile shn = new SHNFile(Path.Combine(input_folder, Filename), false);

            Table = shn.table;
            Table.Namespace = Namespace;
            Table.TableName = Tablename;

            DataColumn[] primary_keys = new DataColumn[1];
            primary_keys[0] = Table.Columns["ID"];
            Table.PrimaryKey = primary_keys;

            Table.Columns["InxName"].Unique = true;

            foreach (DataRow dr in Table.Rows)
            {
                Utils.RemoveEmptyString(dr, "DemandSk");
            }
        }

        public override void Patch(string patch_folder)
        {
            CSVPatch patch = new CSVPatch();
            patch.Load(Path.Combine(patch_folder, @"passive_skill_patch.csv"), Table.Columns);

            Utils.PatchTable_Update(Table, patch.table, "id", new string[] { "demandsk" });
        }

        public override void CreateConstraints()
        {
            Utils.AddForeignKeyColumn(
                DataSet,
                Tablename, new string[] { "DemandSk" },
                Tablename, new string[] { "InxName" },
                "previous_skill_fk");
            Utils.AddForeignKeyColumn(
                DataSet,
                Tablename, new string[] { "InxName" },
                "passive_skill_ab_state", new string[] { "PS_InxName" },
                "ab_state_fk");
        }
    }
}
