import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MobSpeciesPortraitComponent } from './mob-species-portrait.component';

describe('MobSpeciesPortraitComponent', () => {
  let component: MobSpeciesPortraitComponent;
  let fixture: ComponentFixture<MobSpeciesPortraitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MobSpeciesPortraitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MobSpeciesPortraitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
