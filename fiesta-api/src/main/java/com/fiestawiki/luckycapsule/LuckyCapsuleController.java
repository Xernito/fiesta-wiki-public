package com.fiestawiki.luckycapsule;

import com.fiestawiki.luckycapsule.lcgrouprate.LuckyCapsuleGroupRateDTO;
import com.fiestawiki.luckycapsule.luckycapsule.LuckyCapsuleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class LuckyCapsuleController
{
    @Autowired
    private LuckyCapsuleService luckyCapsuleService;

    @GetMapping("lucky-capsules/{type}")
    public ResponseEntity<List<LuckyCapsuleDTO>> getLuckyCapsules(@PathVariable(value = "type") Byte type)
            throws Exception
    {
        List<LuckyCapsuleDTO> capsules = luckyCapsuleService.getCapsules(type);

        return ResponseEntity.ok(capsules);
    }

    @GetMapping("lucky-capsule-rewards/{id}")
    public ResponseEntity<List<LuckyCapsuleGroupRateDTO>> getLuckyCapsuleGroups(@PathVariable(value = "id") Long id)
            throws Exception
    {
        List<LuckyCapsuleGroupRateDTO> groups = luckyCapsuleService.getCapsuleReward(id);

        return ResponseEntity.ok(groups);
    }
}
