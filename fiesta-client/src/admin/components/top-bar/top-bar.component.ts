import { Component, OnInit } from '@angular/core';
import { LoginService } from 'admin/services/login.service';
import { Router } from '@angular/router';

@Component({
   selector: 'app-top-bar',
   templateUrl: './top-bar.component.html',
   styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit {

   get username() { return this.loginService.username; }

   constructor(
      private router: Router,
      private loginService: LoginService
   ) { }

   ngOnInit() {
   }

   onLogout() {
      this.loginService.logout();

      this.router.navigate(['/admin/login']);
   }

}
