import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MobInfo } from '@fiesta-common/dto/mobs/mob-info';
import { Observable } from 'rxjs';
import { MobEditionDialogComponent } from './mob-edition-dialog/mob-edition-dialog.component';

@Component({
    selector: 'app-mob-edition',
    templateUrl: './mob-edition.component.html',
    styleUrls: ['./mob-edition.component.scss']
})
export class MobEditionComponent {

    private mob$: Observable<MobInfo>;
    @Input()
    get mobObs(): Observable<MobInfo> { return this.mob$; }
    set mobObs(mob: Observable<MobInfo>) {
        this.mob$ = mob;
        this.mob$.subscribe(m => this.mob = m);
    }

    mob: MobInfo;

    private dialogSize = { width: '600px', height: '700px' };

    constructor(
        private dialog: MatDialog
    ) 
    {}

    onEdit() {
        this.dialog.open(MobEditionDialogComponent, {
            height: this.dialogSize.height,
            width: this.dialogSize.width,
            data: {
                mob: this.mob
            }
        })
            .afterClosed()
            .subscribe(mob => {
                if (mob) {
                }
            });
    }
}
