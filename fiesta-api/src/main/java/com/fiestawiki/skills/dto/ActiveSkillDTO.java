package com.fiestawiki.skills.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fiestawiki.skills.dao.activeskillview.ActiveSkillViewDTO;
import com.fiestawiki.states.abstate.AbStateDTO;

import java.util.ArrayList;
import java.util.List;

public class ActiveSkillDTO {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("inxname")
    private String inxname;

    @JsonProperty("name")
    private String name;

    @JsonProperty("view")
    private ActiveSkillViewDTO view;

    @JsonProperty("step")
    private Long step;

    @JsonProperty("max_step")
    private Long maxStep;

    @JsonProperty("required_weapon")
    private Long requiredWeapon;

    @JsonProperty("sp")
    private Long sp;

    @JsonProperty("sp_rate")
    private Long spRate;

    @JsonProperty("hp")
    private Long hp;

    @JsonProperty("hp_rate")
    private Long hpRate;

    @JsonProperty("lp")
    private Long lp;

    @JsonProperty("range")
    private Long range;

    @JsonProperty("casted_on")
    private Long castedOn;

    @JsonProperty("affected_target")
    private Long affectedTarget;

    @JsonProperty("cast_time")
    private Long castTime;

    @JsonProperty("cooldown")
    private Long cooldown;

    @JsonProperty("min_dmg")
    private Long minDmg;

    @JsonProperty("min_dmg_rate")
    private Long minDmgRate;

    @JsonProperty("max_dmg")
    private Long maxDmg;

    @JsonProperty("max_dmg_rate")
    private Long maxDmgRate;

    @JsonProperty("min_mdmg")
    private Long minMdmg;

    @JsonProperty("min_mdmg_rate")
    private Long minMdmgRate;

    @JsonProperty("max_mdmg")
    private Long maxMdmg;

    @JsonProperty("max_mdmg_rate")
    private Long maxMdmgRate;

    @JsonProperty("use_class")
    private Long useClass;

    @JsonProperty("ab_states")
    private List<AbStateDTO> abStates = new ArrayList<>();

    @JsonProperty("empowerment")
    private SkillEmpowermentDTO empowerment;

    @JsonProperty("skill_type")
    private Long skillType;

    @JsonProperty("effects")
    private List<ActiveSkillEffectDTO> effects = new ArrayList<>();

    @JsonProperty("soul_required")
    private Long soulRequired;

    @JsonProperty("group_name")
    private String groupName;

    @JsonProperty("groups_id")
    private List<Long> groups;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInxname() {
        return inxname;
    }

    public void setInxname(String inxname) {
        this.inxname = inxname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ActiveSkillViewDTO getView() {
        return view;
    }

    public void setView(ActiveSkillViewDTO view) {
        this.view = view;
    }

    public Long getStep() {
        return step;
    }

    public void setStep(Long step) {
        this.step = step;
    }

    public Long getMaxStep() { return maxStep; }

    public void setMaxStep(Long maxStep) { this.maxStep = maxStep; }

    public Long getRequiredWeapon() {
        return requiredWeapon;
    }

    public void setRequiredWeapon(Long requiredWeapon) {
        this.requiredWeapon = requiredWeapon;
    }

    public Long getSp() {
        return sp;
    }

    public void setSp(Long sp) {
        this.sp = sp;
    }

    public Long getSpRate() {
        return spRate;
    }

    public void setSpRate(Long spRate) {
        this.spRate = spRate;
    }

    public Long getHp() {
        return hp;
    }

    public void setHp(Long hp) {
        this.hp = hp;
    }

    public Long getHpRate() {
        return hpRate;
    }

    public void setHpRate(Long hpRate) {
        this.hpRate = hpRate;
    }

    public Long getLp() {
        return lp;
    }

    public void setLp(Long lp) {
        this.lp = lp;
    }

    public Long getRange() {
        return range;
    }

    public void setRange(Long range) {
        this.range = range;
    }

    public Long getCastedOn() {
        return castedOn;
    }

    public void setCastedOn(Long castedOn) {
        this.castedOn = castedOn;
    }

    public Long getAffectedTarget() {
        return affectedTarget;
    }

    public void setAffectedTarget(Long affectedTarget) {
        this.affectedTarget = affectedTarget;
    }

    public Long getCastTime() {
        return castTime;
    }

    public void setCastTime(Long castTime) {
        this.castTime = castTime;
    }

    public Long getCooldown() {
        return cooldown;
    }

    public void setCooldown(Long cooldown) {
        this.cooldown = cooldown;
    }

    public Long getMinDmg() {
        return minDmg;
    }

    public void setMinDmg(Long minDmg) {
        this.minDmg = minDmg;
    }

    public Long getMinDmgRate() { return minDmgRate; }

    public void setMinDmgRate(Long minDmgRate) { this.minDmgRate = minDmgRate; }

    public Long getMaxDmg() {
        return maxDmg;
    }

    public void setMaxDmg(Long maxDmg) {
        this.maxDmg = maxDmg;
    }

    public Long getMaxDmgRate() {
        return maxDmgRate;
    }

    public void setMaxDmgRate(Long maxDmgRate) {
        this.maxDmgRate = maxDmgRate;
    }

    public Long getMinMdmg() {
        return minMdmg;
    }

    public void setMinMdmg(Long minMdmg) {
        this.minMdmg = minMdmg;
    }

    public Long getMinMdmgRate() { return minMdmgRate; }

    public void setMinMdmgRate(Long minMdmgRate) { this.minMdmgRate = minMdmgRate; }

    public Long getMaxMdmg() {
        return maxMdmg;
    }

    public void setMaxMdmg(Long maxMdmg) {
        this.maxMdmg = maxMdmg;
    }

    public Long getMaxMdmgRate() {
        return maxMdmgRate;
    }

    public void setMaxMdmgRate(Long maxMdmgRate) {
        this.maxMdmgRate = maxMdmgRate;
    }

    public Long getUseClass() {
        return useClass;
    }

    public void setUseClass(Long useClass) {
        this.useClass = useClass;
    }

    public List<AbStateDTO> getAbStates() { return abStates; }

    public void setAbStates(List<AbStateDTO> abStates) { this.abStates = abStates; }

    public SkillEmpowermentDTO getEmpowerment() { return empowerment; }

    public void setEmpowerment(SkillEmpowermentDTO empowerment) { this.empowerment = empowerment; }

    public Long getSkillType() { return skillType; }

    public void setSkillType(Long skillType) { this.skillType = skillType; }

    public List<ActiveSkillEffectDTO> getEffects() { return effects; }

    public void setEffects(List<ActiveSkillEffectDTO> effects) { this.effects = effects; }

    public Long getSoulRequired() { return soulRequired; }

    public void setSoulRequired(Long soulRequired) { this.soulRequired = soulRequired; }

    public String getGroupName() { return groupName; }

    public void setGroupName(String groupName) { this.groupName = groupName; }

    public List<Long> getGroups() { return groups; }

    public void setGroups(List<Long> groups) { this.groups = groups; }
}
