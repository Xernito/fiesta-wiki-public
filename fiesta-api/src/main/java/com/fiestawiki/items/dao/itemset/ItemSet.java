package com.fiestawiki.items.dao.itemset;

import com.fiestawiki.items.dao.info.ItemInfo;
import com.fiestawiki.items.dao.seteffect.SetEffect;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "item_set")
@EntityListeners(AuditingEntityListener.class)
public class ItemSet implements Serializable {

    @Id
    @Column(name = "unique_id", nullable = false)
    private Long uniqueId;

    @Column(name = "setitemindex")
    private String setItemIndex;

    @Column(name = "setname")
    private String setName;

    @OneToMany(mappedBy = "itemSet")
    private List<ItemInfo> setItems = new ArrayList<>();

    @OneToMany(mappedBy = "itemSet")
    private List<SetEffect> itemSetEffects = new ArrayList<>();

    public Long getUniqueId() { return uniqueId; }

    public void setUniqueId(Long uniqueId) { this.uniqueId = uniqueId; }

    public String getSetItemIndex() {
        return setItemIndex;
    }

    public void setSetItemIndex(String setItemIndex) {
        this.setItemIndex = setItemIndex;
    }

    public String getSetName() {
        return setName;
    }

    public void setSetName(String setName) {
        this.setName = setName;
    }

    public List<ItemInfo> getSetItems() { return setItems; }

    public void setSetItems(List<ItemInfo> setItems) { this.setItems = setItems; }

    public List<SetEffect> getItemSetEffects()
    {
        // Return the set effects ordered by the count index (2 set effect before 3 set effect...)
        return itemSetEffects
                .stream()
                .sorted(Comparator.comparingInt(SetEffect::getCount))
                .collect(Collectors.toList());
    }

    public void setItemSetEffects(List<SetEffect> itemSetEffects) { this.itemSetEffects = itemSetEffects; }

    public List<Long> getSetItemsId() {
        return this.setItems.stream().map(item -> item.getId()).collect(Collectors.toList());
    }

    public List<Long> getItemSetEffectsId() {
        return this.itemSetEffects
                .stream()
                .sorted(Comparator.comparingInt(SetEffect::getCount))
                .map(item -> item.getUniqueId())
                .collect(Collectors.toList());
    }
}
