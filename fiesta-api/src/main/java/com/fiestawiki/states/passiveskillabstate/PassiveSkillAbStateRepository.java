package com.fiestawiki.states.passiveskillabstate;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PassiveSkillAbStateRepository extends JpaRepository<PassiveSkillAbState, Long> {
}
