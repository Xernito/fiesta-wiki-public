import { Item } from "../../fiesta-common/dto/items/item";
import { MobSpecies } from "../../fiesta-common/dto/mobs/mob-info";

export interface LicenseLevelDTO {
    level: number;
    suffix: string
    mob_kill_count: number;
    dmg_ratio: number;
    crit_ratio: number
}

export interface LicenseDTO {
    id: number;
    species_id: number;
    item_id: number;
    levels: LicenseLevelDTO[];
}

export class LicenseLevel {
    level: number;
    suffix: string
    mob_kill_count: number;
    dmg_ratio: number;
    crit_ratio: number
}

export class License {
    id: number;
    species: MobSpecies;
    item: Item;
    levels: LicenseLevel[];
}