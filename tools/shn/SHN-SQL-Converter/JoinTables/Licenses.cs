﻿using System;
using System.Data;

namespace SHN_SQL_Converter.JoinTables
{
    class Licenses : JoinTableBase
    {
        public override void Create()
        {
            Table = Utils.CreateJoinTable(
                DataSet,
                DataSet.Tables["mob_species"],
                DataSet.Tables["item_info"],
                Tablename,
                "public",
                true
                );

        }

        public override void Load()
        {
            Utils.AddJoinColumn<string>(
                DataSet,
                DataSet.Tables["mob_species"],
                DataSet.Tables["item_info"],
                "species_name",
                "InxName",
                Tablename,
                null,
                null,
                (object species, object item) => 
                { 
                    // Looks like the link between a license and the mob its linked to is by the InxName of the license 
                    // with the format WTL_XXXXX
                    return (item as string).EndsWith(string.Format("WTL{0}", (species as string)), StringComparison.Ordinal); 
                }
                );
        }
    }
}
