package com.fiestawiki.items.dao.itemactioncondition;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemActionConditionRepository extends JpaRepository<ItemActionCondition, Long> {
}
