import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PageEvent } from '@angular/material/paginator';
import { map, switchMap, share } from 'rxjs/operators';
import { combineLatest } from 'rxjs';
import { PageDescriptionService } from '@services/page-description.service';
import { ItemsPageTemplateComponent } from '@fiesta-common/components/items-page-template/items-page-template.component';
import { ItemService } from '@fiesta-common/services/item.service';
import { LevelRangeFilterComponent } from '@fiesta-common/components/level-range-filter/level-range-filter.component';
import { FormBuilder, FormGroup } from '@angular/forms';
import { fiestaMaxLevel } from '@fiesta-common/game-globals';
import { PAGES_NAME } from '@app/global-strings';

@Component({
    selector: 'app-bracelets-page',
    templateUrl: './bracelets-page.component.html',
    styleUrls: ['./bracelets-page.component.scss']
})
export class BraceletsPageComponent extends ItemsPageTemplateComponent implements OnInit {

    @ViewChild(LevelRangeFilterComponent, { static: true }) levelRangeForm: LevelRangeFilterComponent;

    readonly PAGE_TITLE = PAGES_NAME.bracelets;

    filterFormGroup: FormGroup;

    displayedColumns: string[] =
        [
            'icon',
            'name',
            'level',
            'damage',
            'mdamage',
            'defense',
            'mdefense',
            'crit'
        ];

    readonly maxEnhancement = 20;

    constructor(
        protected router: Router,
        protected route: ActivatedRoute,
        private formBuilder: FormBuilder,
        protected pageDescriptionService: PageDescriptionService,
        private itemService: ItemService,
    ) {
        super(router, route);

        this.enhancement = this.maxEnhancement;
    }

    ngOnInit() {

        this.pageDescriptionService.setPageDescription({
            title: this.PAGE_TITLE,
            description: 'List of Bracelets available in Fiesta Online.'
        });

        this.createForm();

        this.page$ = combineLatest(
            [this.route.paramMap,
            this.route.queryParamMap]
        ).pipe(
            switchMap(([param, queryParam]) => {
                let page = 0;
                let order;
                let search = '';
                let min;
                let max;

                if (param.has('page')) {
                    page = +param.get('page');
                }

                if (queryParam.has('order')) {
                    order = +queryParam.get('order');
                }
                if (queryParam.has('search')) {
                    search = queryParam.get('search');
                }
                if (queryParam.has('min')) {
                    min = +queryParam.get('min');
                }
                if (queryParam.has('max')) {
                    max = +queryParam.get('max');
                }

                if (this.itemTable) {
                    this.itemTable.clear();
                }

                return this.itemService.getBracelets(page, order, search, min, max);
            }),
            share()
        );

        combineLatest(
            [this.route.paramMap,
            this.route.queryParamMap]
        ).pipe(map(([param, queryParam]) => {
            let minLevel = 0;
            let maxLevel = fiestaMaxLevel;

            if (queryParam.has('min')) {
                minLevel = +queryParam.get('min');
            }
            if (queryParam.has('max')) {
                maxLevel = +queryParam.get('max');
            }

            return [minLevel, maxLevel];
        })).subscribe(([minLevel, maxLevel]: [number, number]) => {

            this.filterFormGroup.setValue({ search: '', levelRange: { min: minLevel, max: maxLevel } });
        });
    }

    createForm() {
        this.filterFormGroup = this.formBuilder.group({
            search: null,
            levelRange: this.levelRangeForm.createForm(),
        });
    }

    onSubmit(data: any) {
        this.filter({
            rarity: data.rarity,
            search: data.search ? data.search : null,
            min: data.levelRange.min ? data.levelRange.min : null,
            max: data.levelRange.max ? data.levelRange.max : null
        });
    }

    onPageChange(pageEvent: PageEvent) {
        this.router.navigate(['/bracelets', pageEvent.pageIndex], { queryParamsHandling: 'merge' });
    }
}
