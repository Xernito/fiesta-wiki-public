package com.fiestawiki.items.dao.info;

import com.fiestawiki.common.useclass.UseClass;
import com.fiestawiki.items.dao.iteminfoserver.ItemInfoServer;
import com.fiestawiki.items.dao.itemset.ItemSet;
import com.fiestawiki.items.dao.stats.ItemStats;
import com.fiestawiki.items.dao.itemview.ItemViewInfo;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "item_info")
@EntityListeners(AuditingEntityListener.class)
public class ItemInfo implements Serializable {

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "inxname")
    private String inxname;

    @Column(name = "name")
    private String name;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    private ItemViewInfo view;

    @ManyToOne
    @JoinColumn(name = "grade_item_option_id")
    private ItemStats stats;

    @OneToOne
    @JoinColumn(name = "item_info_server_fk")
    private ItemInfoServer serverInfo;

    // 0 : Equipable
    // 1 : Usable (click item will unlock or do something? )
    // 2 : enchantment (licenses, enchant, stones...)
    // 3 : quest items
    // 4 : nothing
    // 5 : lucky house card
    // 6 : cards
    @Column(name = "type")
    private int type;

    @Column(name = "class")
    private int itemCategory;

    @Column(name = "equip")
    private int equipSlot;

    @Column(name = "itemauctiongroup")
    private Long itemAuctionGroup;

    @Column(name = "itemgradetype")
    private int itemGradeType;

    @Column(name = "twohand")
    private int twoHand;

    @Column(name = "atkspeed")
    private Long attackSpeed;

    @Column(name = "demandlv")
    private Long requiredLevel;

    @Column(name = "grade")
    private Long grade;

    @Column(name = "minwc")
    private Long minPhyAtk;

    @Column(name = "maxwc")
    private Long maxPhyAtk;

    @Column(name = "ac")
    private Long defense;

    @Column(name = "minma")
    private Long minMagAtk;

    @Column(name = "maxma")
    private Long maxMagAtk;

    @Column(name = "mr")
    private Long magicalDefense;

    @Column(name = "th")
    private Long aim;

    @Column(name = "tb")
    private Long evasion;

    @Column(name = "wcrate")
    private Long phyAtkRate;

    @Column(name = "marate")
    private Long magAtkRate;

    @Column(name = "acrate")
    private Long defenseRate;

    @Column(name = "mrrate")
    private Long magicalDefenseRate;

    @Column(name = "crirate")
    private Long criticalRate;

    @ManyToOne
    @JoinColumn(name = "useclass")
    private UseClass useClass;

    @Column(name = "buyprice")
    private Long buyPrice;

    @Column(name = "sellprice")
    private Long sellPrice;

    @Column(name = "buygbcoin")
    private Long buyGBCoin;

    @Column(name = "weapontype")
    private Long weaponType;

    @Column(name = "uplimit")
    private int upgradeMax;

    @Column(name = "basicupinx")
    private Long upgradeId;

    @Column(name = "shieldac")
    private int blockRate;

    @ManyToOne
    @JoinColumn(name = "item_set_id")
    private ItemSet itemSet;

    public Long getId() {
        return id;
    }

    public void setId(Long id) { this.id = id; }

    public String getInxname() { return inxname; }

    public void setInxname(String inxname) {
        this.inxname = inxname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ItemViewInfo getView() {
        return view;
    }

    public void setView(ItemViewInfo view) {
        this.view = view;
    }

    public ItemStats getStats() { return stats; }

    public void setStats(ItemStats stats) { this.stats = stats; }

    public ItemInfoServer getServerInfo() { return serverInfo; }

    public void setServerInfo(ItemInfoServer serverInfo) { this.serverInfo = serverInfo; }

    public Long getRequiredLevel() { return requiredLevel; }

    public void setRequiredLevel(Long requiredLevel) { this.requiredLevel = requiredLevel; }

    public Long getAttackSpeed() { return attackSpeed; }

    public void setAttackSpeed(Long attackSpeed) { this.attackSpeed = attackSpeed; }

    public int getItemCategory() { return itemCategory; }

    public void setItemCategory(int itemCategory) { this.itemCategory = itemCategory; }

    public int getItemGradeType() { return itemGradeType; }

    public void setItemGradeType(int itemGradeType) { this.itemGradeType = itemGradeType; }

    public int getTwoHand() { return twoHand; }

    public void setTwoHand(int twoHand) { this.twoHand = twoHand; }

    public Long getGrade() { return grade; }

    public void setGrade(Long grade) { this.grade = grade; }

    public Long getMinPhyAtk() { return minPhyAtk; }

    public void setMinPhyAtk(Long minPhyAtk) { this.minPhyAtk = minPhyAtk; }

    public Long getMaxPhyAtk() { return maxPhyAtk; }

    public void setMaxPhyAtk(Long maxPhyAtk) { this.maxPhyAtk = maxPhyAtk; }

    public Long getDefense() { return defense; }

    public void setDefense(Long defense) { this.defense = defense; }

    public Long getMinMagAtk() { return minMagAtk; }

    public void setMinMagAtk(Long minMagAtk) { this.minMagAtk = minMagAtk; }

    public Long getMaxMagAtk() { return maxMagAtk; }

    public void setMaxMagAtk(Long maxMagAtk) { this.maxMagAtk = maxMagAtk; }

    public Long getMagicalDefense() { return magicalDefense; }

    public void setMagicalDefense(Long magicalDefense) { this.magicalDefense = magicalDefense; }

    public Long getAim() { return aim; }

    public void setAim(Long aim) { this.aim = aim; }

    public Long getEvasion() { return evasion; }

    public void setEvasion(Long evasion) { this.evasion = evasion; }

    public Long getPhyAtkRate() { return phyAtkRate; }

    public void setPhyAtkRate(Long phyAtkRate) { this.phyAtkRate = phyAtkRate; }

    public Long getMagAtkRate() { return magAtkRate; }

    public void setMagAtkRate(Long magAtkRate) { this.magAtkRate = magAtkRate; }

    public Long getDefenseRate() { return defenseRate; }

    public void setDefenseRate(Long defenseRate) { this.defenseRate = defenseRate; }

    public Long getMagicalDefenseRate() { return magicalDefenseRate; }

    public void setMagicalDefenseRate(Long magicalDefenseRate) { this.magicalDefenseRate = magicalDefenseRate; }

    public Long getCriticalRate() { return criticalRate; }

    public void setCriticalRate(Long criticalRate) { this.criticalRate = criticalRate; }

    public UseClass getUseClass() { return useClass; }

    public void setUseClass(UseClass useClass) { this.useClass = useClass; }

    public Long getBuyPrice() { return buyPrice; }

    public void setBuyPrice(Long buyPrice) { this.buyPrice = buyPrice; }

    public Long getSellPrice() { return sellPrice; }

    public void setSellPrice(Long sellPrice) { this.sellPrice = sellPrice; }

    public Long getBuyGBCoin() { return buyGBCoin; }

    public void setBuyGBCoin(Long buyGBCoin) { this.buyGBCoin = buyGBCoin; }

    public Long getWeaponType() { return weaponType; }

    public void setWeaponType(Long weaponType) { this.weaponType = weaponType; }

    public int getUpgradeMax() { return upgradeMax; }

    public void setUpgradeMax(int upgradeMax) { this.upgradeMax = upgradeMax; }

    public Long getUpgradeId() { return upgradeId; }

    public void setUpgradeId(Long upgradeId) { this.upgradeId = upgradeId; }

    public int getBlockRate() { return blockRate; }

    public void setBlockRate(int blockRate) { this.blockRate = blockRate; }

    public int getEquipSlot() {
        return equipSlot;
    }

    public void setEquipSlot(int equipSlot) {
        this.equipSlot = equipSlot;
    }

    public Long getItemAuctionGroup() { return itemAuctionGroup; }

    public void setItemAuctionGroup(Long itemAuctionGroup) { this.itemAuctionGroup = itemAuctionGroup; }

    public ItemSet getItemSet() {
        return itemSet;
    }

    public Long getItemSetId()
    {
        if(itemSet == null)
            return -1L;

        return itemSet.getUniqueId();
    }

    public void setItemSet(ItemSet itemSet) {
        this.itemSet = itemSet;
    }
}
