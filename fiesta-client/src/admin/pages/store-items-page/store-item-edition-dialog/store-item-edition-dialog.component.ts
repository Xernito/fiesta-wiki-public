import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { StoreItem, StoreCategory } from '@fiesta-common/dto/store-items/store-item';
import { ItemService } from '@fiesta-common/services/item.service';
import { Item } from '@fiesta-common/dto/items/item';
import { debounceTime, tap, switchMap, finalize, map, startWith, pairwise } from 'rxjs/operators';
import { StoreItemService } from '@fiesta-common/services/store-item.service';

@Component({
    selector: 'app-store-item-edition-dialog',
    templateUrl: './store-item-edition-dialog.component.html',
    styleUrls: ['./store-item-edition-dialog.component.scss']
})
export class StoreItemEditionDialogComponent implements OnInit {

    storeItemForm: FormGroup;
    nameControl = new FormControl({ value: '' });
    scControl = new FormControl({ value: 0 });
    durationControl = new FormControl({ value: 0 });
    slotControl = new FormControl({ value: 0 });
    designerControl = new FormControl({ value: 0 });
    linkControl = new FormControl({ value: '' });
    categoriesControl = new FormControl({ value: {} });

    isLoading = false;

    items: Item[];

    item: StoreItem;

    categories: StoreCategory[];

    validationButtonName = 'Create';

    get filteredCategories(): StoreCategory[] {
        if (!this.categories) {
            return [];
        }

        return this.categories.filter(c => c.type !== 0 && c.type !== 2 && c.type !== 3);
    }

    get timeCategories(): StoreCategory[] {
        if (!this.categories) {
            return [];
        }

        return this.categories.filter(c => c.type === 0);
    }

    get slotCategories(): StoreCategory[] {
        if (!this.categories) {
            return [];
        }

        return this.categories.filter(c => c.type === 2);
    }

    get designerCategories(): StoreCategory[] {
        if (!this.categories) {
            return [];
        }

        return this.categories.filter(c => c.type === 3);
    }

    @ViewChild('categoryInput', { static: true }) categoryInput: ElementRef<HTMLInputElement>;

    constructor(
        public dialogRef: MatDialogRef<StoreItemEditionDialogComponent, StoreItem>,
        private formBuilder: FormBuilder,
        private itemService: ItemService,
        private storeItemService: StoreItemService,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {

        this.storeItemForm = this.formBuilder.group({
            name: this.nameControl,
            sc: this.scControl,
            duration: this.durationControl,
            link: this.linkControl,
            categories: this.categoriesControl,
        });

        if (data && data.mode) {
            if (data.mode === 'edit') {
                this.validationButtonName = 'Edit';
            }
        }

        if (data && data.item) {
            this.item = Object.assign(new StoreItem(), data.item);
        } else {
            this.item = new StoreItem();
        }

        if (this.item.item) {
            this.nameControl.setValue({ name: this.item.item.name });
        }
        this.scControl.setValue(this.item.sc);
        this.linkControl.setValue(this.item.link);
        this.categoriesControl.setValue(this.item.categories);
    }

    ngOnInit() {

        this.nameControl.valueChanges.pipe(
            debounceTime(500),
            tap(() => {
                this.items = [];
                this.storeItemForm.controls.sc.disable();
                this.isLoading = true;
            }),
            // the observable is called again after selecting a value with object instead of string
            map(value => typeof value === 'string' ? value : value.name),
            switchMap(name => {
                return this.itemService.findItem(name).pipe(
                    finalize(() => {
                        this.isLoading = false;
                    })
                );
            })
        ).subscribe(
            data => {
                this.items = data;
                this.storeItemForm.controls.sc.enable();
            });

        this.scControl.valueChanges.subscribe(sc => { this.item.sc = sc; });
        this.linkControl.valueChanges.subscribe(link => { this.item.link = link; });

        this.storeItemService.getStoreCategories().subscribe(
            categories => {
                this.categories = categories.sort((c1, c2) => c1.type - c2.type);

                const durationCategory = this.item.categories.find(c => c.type === 0);
                if (durationCategory != null) {
                    this.durationControl.setValue(durationCategory.id);
                }
                const slotCategory = this.item.categories.find(c => c.type === 2);
                if (slotCategory != null) {
                    this.slotControl.setValue(slotCategory.id);
                }
                const designerCategory = this.item.categories.find(c => c.type === 3);
                if (designerCategory != null) {
                    this.designerControl.setValue(designerCategory.id);
                }

                this.durationControl.valueChanges.pipe(
                    // tslint:disable-next-line:deprecation
                    startWith(null),
                    pairwise()
                ).subscribe(([previousDuration, nextDuration]: [number, number]) => {
                    this.item.duration = nextDuration;
                    this.onRemoveCategory(this.getCategoryByIndex(previousDuration));
                    this.addCategory(this.getCategoryByIndex(nextDuration));
                });
                this.slotControl.valueChanges.pipe(
                    // tslint:disable-next-line:deprecation
                    startWith(null),
                    pairwise()
                ).subscribe(([previousSlot, nextSlot]: [number, number]) => {
                    this.onRemoveCategory(this.getCategoryByIndex(previousSlot));
                    this.addCategory(this.getCategoryByIndex(nextSlot));
                });
                this.designerControl.valueChanges.pipe(
                    // tslint:disable-next-line:deprecation
                    startWith(null),
                    pairwise()
                ).subscribe(([previousDesigner, nextDesigner]: [number, number]) => {
                    this.onRemoveCategory(this.getCategoryByIndex(previousDesigner));
                    this.addCategory(this.getCategoryByIndex(nextDesigner));
                });
            }
        );
    }

    getCategoryByIndex(index: number): StoreCategory {
        return this.categories.find(c => c.id === index);
    }

    getCategoryByName(name: string): StoreCategory {
        return this.categories.find(c => c.name === name);
    }

    displayItemName(item: Item) {
        return item.name;
    }

    onItemSelected(select: MatAutocompleteSelectedEvent) {
        this.item.item = select.option.value as Item;
    }

    onRemoveCategory(category: StoreCategory): void {

        const index = this.item.categories.indexOf(category);

        if (index >= 0) {
            this.item.categories.splice(index, 1);
        }
    }

    onCategorySelected(event: MatAutocompleteSelectedEvent): void {
        this.addCategory(event.option.value);
        this.categoryInput.nativeElement.value = '';
        this.categoriesControl.setValue(null);
    }

    addCategory(category: StoreCategory) {
        if (category != null) {
            this.item.categories.push(category);
        }
    }

    onRemoveSet() {
        this.item.set = null;
    }
}
