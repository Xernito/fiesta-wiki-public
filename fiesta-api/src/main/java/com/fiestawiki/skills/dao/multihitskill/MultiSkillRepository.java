package com.fiestawiki.skills.dao.multihitskill;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MultiSkillRepository extends JpaRepository<MultiHitSkill, Long> {
}
