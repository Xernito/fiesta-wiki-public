import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SkillsPageComponent } from './components/skills-page.component';
import { SkillListComponent } from './components/skill-list/skill-list.component';
import { PassiveSkillTiersComponent } from './components/passive-skill-tiers/passive-skill-tiers.component';
import { ActiveSkillTiersComponent } from './components/active-skill-tiers/active-skill-tiers.component';
import { ClassComboboxComponent } from './components/class-combobox/class-combobox.component';
import { FiestaCommonModule } from '@fiesta-common/fiesta-common.module';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { ActiveSkillTooltipComponent } from './components/skill/skill-tooltip/active/active-skill-tooltip.component';
import { PassiveSkillTooltipComponent } from './components/skill/skill-tooltip/passive/passive-skill-tooltip.component';
import { SkillIconComponent } from './components/skill/skill-icon/skill-icon.component';
import { ActiveSkillIconComponent } from './components/skill/skill-icon/active-skill-icon/active-skill-icon.component';
import { PassiveSkillIconComponent } from './components/skill/skill-icon/passive-skill-icon/passive-skill-icon.component';
import { WeaponPipe } from './pipes/weapon';
import { FiestaUnitToMeterPipe } from './pipes/fiesta-unit-to-meter';
import { TargetPipe } from './pipes/target';
import { MatInputModule } from '@angular/material/input';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';

const routes: Routes = [
    { path: '', redirectTo: '2', pathMatch: 'full' },
    { path: ':class', component: SkillsPageComponent },
    { path: ':class/:level', component: SkillsPageComponent },
];

@NgModule({
    declarations: [
        SkillsPageComponent,
        SkillListComponent,
        PassiveSkillTiersComponent,
        ActiveSkillTiersComponent,
        ClassComboboxComponent,
        SkillIconComponent,
        ActiveSkillIconComponent,
        PassiveSkillIconComponent,
        ActiveSkillTooltipComponent,
        PassiveSkillTooltipComponent,
        WeaponPipe,
        FiestaUnitToMeterPipe,
        TargetPipe,
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        FiestaCommonModule,
        MatIconModule,
        MatFormFieldModule,
        MatTableModule,
        MatSelectModule,
        MatInputModule,
        FlexLayoutModule,
        MatButtonModule,
    ],
    entryComponents: [
        ActiveSkillTooltipComponent,
        PassiveSkillTooltipComponent,
    ]
})
export class SkillsModule { }
