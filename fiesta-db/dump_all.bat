SET user=docker

SET output=dump

mkdir %output%

docker exec -t db-db-dev-1 pg_dump -U %user% docker > %output%/dump-all.sql
docker exec -t db_db-dev-de_1 pg_dump -U %user% docker > %output%/dump-de-all.sql