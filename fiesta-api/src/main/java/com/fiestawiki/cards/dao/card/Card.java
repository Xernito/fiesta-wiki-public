package com.fiestawiki.cards.dao.card;

import com.fiestawiki.cards.dao.carddroprate.CardDropRate;
import com.fiestawiki.cards.dao.groupdesc.CardGroupDesc;
import com.fiestawiki.cards.dao.title.CardTitle;
import com.fiestawiki.cards.dao.view.CardView;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "collect_card")
@EntityListeners(AuditingEntityListener.class)
public class Card implements Serializable
{
    @Id
    @Column(name = "cc_cardid", nullable = false)
    private Long id;

    @Column(name = "cc_iteminx")
    private String itemInx;

    @Column(name = "cc_cardgradetype")
    private Long gradeType;

    @ManyToOne
    @JoinColumn(name = "card_mob_group_id")
    private CardGroupDesc cardGroup;

    @OneToOne
    @JoinColumn(
            name = "cc_cardid",
            referencedColumnName = "cc_cardid",
            insertable = false,
            updatable = false,
            foreignKey = @javax.persistence.ForeignKey(value = ConstraintMode.NO_CONSTRAINT)
    )
    private CardDropRate getRate;

    @OneToOne
    @JoinColumn(
            name = "cc_cardid",
            referencedColumnName = "cc_cardid",
            insertable = false,
            updatable = false,
            foreignKey = @javax.persistence.ForeignKey(value = ConstraintMode.NO_CONSTRAINT)
    )
    private CardView view;

    @ManyToOne
    @JoinColumn(name = "card_title_id")
    private CardTitle cardTitle;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItemInx() {
        return itemInx;
    }

    public void setItemInx(String itemInx) {
        this.itemInx = itemInx;
    }

    public Long getGradeType() {
        return gradeType;
    }

    public void setGradeType(Long gradeType) {
        this.gradeType = gradeType;
    }

    public CardGroupDesc getCardGroup() { return cardGroup; }

    public void setCardGroup(CardGroupDesc cardGroup) { this.cardGroup = cardGroup; }

    public CardDropRate getGetRate() { return getRate; }

    public void setGetRate(CardDropRate getRate) { this.getRate = getRate; }

    public CardView getView() { return view; }

    public void setView(CardView view) { this.view = view; }

    public CardTitle getCardTitle() {
        return cardTitle;
    }

    public void setCardTitle(CardTitle cardTitle) {
        this.cardTitle = cardTitle;
    }
}
