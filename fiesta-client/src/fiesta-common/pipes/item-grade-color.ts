import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'itemGradeColor' })
export class ItemGradeColorPipe implements PipeTransform {
   transform(grade: number): string {
      switch (grade) {
         case 1:
            return '#00FF00'; // Green
         case 2:
            return '#00FFFF'; // Blue
         case 5:
            return '#6160cb'; // Blue set
         case 3:
            return '#FFFF00'; // Unique
         case 4:
            return '#FF00FF'; // Purple (premium items)
         case 6:
            return '#FFD800'; // Orange
         default:
            return 'white';
      }
   }
}
