package com.fiestawiki.luckycapsule.lcgrouprate;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LuckyCapsuleGroupRateRepository extends JpaRepository<LuckyCapsuleGroupRate, Long>
{
    @Query("SELECT lc FROM LuckyCapsuleGroupRate lc WHERE lc.itemId = :itemId ORDER BY lc.rate ASC")
    List<LuckyCapsuleGroupRate> findByItemId(Long itemId);
}
