import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Card, CardDTO, CardGroup, CardGroupDTO } from '@fiesta-common/dto/cards/card';
import { forkJoin, Observable } from 'rxjs';
import { environment } from '@environments/environment';
import { defaultIfEmpty, map, mergeMap, publishLast, refCount } from 'rxjs/operators';
import { MobService } from '@fiesta-common/services/mob.service';

@Injectable({
    providedIn: 'root'
})
export class CardsService {

    cardMap = new Map<number, Observable<Card>>();
    cardGroupMap = new Map<number, Observable<CardGroup>>();

    constructor(
        private http: HttpClient,
        private mobService: MobService
    ) { }

    getCards(): Observable<Card[]> {
        return this.http.get<number[]>(environment.api + 'cards').pipe(
            mergeMap(result => forkJoin(result.map(i => this.getCard(i))).pipe(
                defaultIfEmpty<Card[]>([])
            ))
        );
    }

    getCard(id: number): Observable<Card> {

        if (this.cardMap.has(id)) {
            return this.cardMap.get(id);
        }

        const obs$ = this.http.get<CardDTO>(environment.api + 'card/' + id).pipe(
            map(card => this.convertCard(card)),
            publishLast(),
            refCount()
        );

        this.cardMap.set(id, obs$);

        return obs$;
    }

    getCardGroup(id: number): Observable<CardGroup> {

        if (this.cardGroupMap.has(id)) {
            return this.cardGroupMap.get(id);
        }

        const obs$ = this.http.get<CardGroupDTO>(environment.api + 'card-group/' + id).pipe(
            map(cardGroup => this.convertCardGroup(cardGroup)),
            publishLast(),
            refCount()
        );

        this.cardGroupMap.set(id, obs$);

        return obs$;
    }

    cardRank(card: Card): string {
        if (card == null) {
            return '';
        }

        switch (card.grade_type) {
            case 0:
                return 'S';
            case 1:
                return 'A';
            case 2:
                return 'B';
            case 3:
                return 'C';
        }
    }

    convertCard(card: CardDTO): Card {
        if (!card) {
            return null;
        }

        const _ = new Card();

        _.id = card.id;
        _.itemInx = card.itemInx;
        _.grade_type = card.grade_type;
        _.mob_species = card.mob_species.map(species => this.mobService.getMobSpecies(species));
        _.get_rate = card.get_rate;
        _.view = card.view;
        _.title = card.title;
        _.group = this.getCardGroup(card.card_group);

        return _;
    }

    convertCardGroup(dto: CardGroupDTO): CardGroup {
        if (!dto) {
            return null;
        }

        const _ = new CardGroup();

        _.cards = dto.cards.map(c => this.getCard(c));

        return _;
    }
}
