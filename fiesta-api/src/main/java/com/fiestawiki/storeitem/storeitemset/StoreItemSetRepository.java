package com.fiestawiki.storeitem.storeitemset;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreItemSetRepository extends JpaRepository<StoreItemSet, Long> {
}
