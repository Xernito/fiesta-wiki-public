package com.fiestawiki.items.dao.seteffect;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SetEffectRepository extends JpaRepository<SetEffect, Long> {
}
