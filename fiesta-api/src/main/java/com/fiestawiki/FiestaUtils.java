package com.fiestawiki;

import com.fiestawiki.common.useclass.UseClass;
import com.fiestawiki.common.useclass.UseClass_;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import javax.persistence.metamodel.SingularAttribute;

@Service
public class FiestaUtils
{
    @Autowired
    private ModelMapper modelMapper;

    /**
     * Maps the Page {@code entities} of <code>T</code> type which have to be mapped as input to {@code dtoClass} Page
     * of mapped object with <code>D</code> type.
     *
     * @param <D> - type of objects in result page
     * @param <T> - type of entity in <code>entityPage</code>
     * @param entities - page of entities that needs to be mapped
     * @param dtoClass - class of result page element
     * @return page - mapped page with objects of type <code>D</code>.
     * @NB <code>dtoClass</code> must has NoArgsConstructor!
     */
    public <D, T> Page<D> mapEntityPageIntoDtoPage(Page<T> entities, Class<D> dtoClass) {
        return entities.map(objectEntity -> modelMapper.map(objectEntity, dtoClass));
    }

    public static SingularAttribute<UseClass, Short> convertClassCanUseClass(Long classID) {
        switch(classID.intValue())
        {
            case 1:
                return UseClass_.fighter;
            case 2:
                return UseClass_.cleverFighter;
            case 3:
                return UseClass_.warrior;
            case 4:
                return UseClass_.gladiator;
            case 5:
                return UseClass_.knight;
            case 6:
                return UseClass_.cleric;
            case 7:
                return UseClass_.highCleric;
            case 8:
                return UseClass_.paladin;
            case 9:
                return UseClass_.holyknight;
            case 10:
                return UseClass_.guardian;
            case 11:
                return UseClass_.archer;
            case 12:
                return UseClass_.hawkArcher;
            case 13:
                return UseClass_.scout;
            case 14:
                return UseClass_.sharpshooter;
            case 15:
                return UseClass_.ranger;
            case 16:
                return UseClass_.mage;
            case 17:
                return UseClass_.wizMage;
            case 18:
                return UseClass_.enchanter;
            case 19:
                return UseClass_.warlock;
            case 20:
                return UseClass_.wizard;
            case 21:
                return UseClass_.trickster;
            case 22:
                return UseClass_.gambit;
            case 23:
                return UseClass_.renegade;
            case 24:
                return UseClass_.spectre;
            case 25:
                return UseClass_.reaper;
            case 26:
                return UseClass_.crusader;
            case 27:
                return UseClass_.templar;
        }

        return UseClass_.fighter;
    }
}
