package com.fiestawiki.title.data;

import com.fiestawiki.cards.dao.title.CardTitle;
import com.fiestawiki.title.stats.TitleStateView;
import com.fiestawiki.title.view.TitleView;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Entity
@Table(name = "title_data")
@EntityListeners(AuditingEntityListener.class)
public class TitleData {

    @Id
    @Column(name = "type", nullable = false)
    private Long type;

    @Column(name = "permit")
    private Long permit;

    @Column(name = "refresh")
    private Long refresh;

    @Column(name = "title0")
    private String title0;

    @Column(name = "value0")
    private Long value0;

    @Column(name = "fame0")
    private Long fame0;

    @Column(name = "title1")
    private String title1;

    @Column(name = "value1")
    private Long value1;

    @Column(name = "fame1")
    private Long fame1;

    @Column(name = "title2")
    private String title2;

    @Column(name = "value2")
    private Long value2;

    @Column(name = "fame2")
    private Long fame2;

    @Column(name = "title3")
    private String title3;

    @Column(name = "value3")
    private Long value3;

    @Column(name = "fame3")
    private Long fame3;

    @OneToMany
    @JoinColumn(
            name = "type",
            referencedColumnName = "type",
            insertable = false,
            updatable = false,
            foreignKey = @javax.persistence.ForeignKey(value = ConstraintMode.NO_CONSTRAINT)
    )
    List<TitleStateView> stats = new ArrayList<>();

    @OneToMany
    @JoinColumn(
            name = "type",
            referencedColumnName = "type",
            insertable = false,
            updatable = false,
            foreignKey = @javax.persistence.ForeignKey(value = ConstraintMode.NO_CONSTRAINT)
    )
    List<TitleView> view = new ArrayList<>();

    @OneToOne(mappedBy = "title")
    private CardTitle cardTitle;

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Long getPermit() {
        return permit;
    }

    public void setPermit(Long permit) {
        this.permit = permit;
    }

    public Long getRefresh() {
        return refresh;
    }

    public void setRefresh(Long refresh) {
        this.refresh = refresh;
    }

    public String getTitle0() {
        return title0;
    }

    public void setTitle0(String title0) {
        this.title0 = title0;
    }

    public Long getValue0() {
        return value0;
    }

    public void setValue0(Long value0) {
        this.value0 = value0;
    }

    public Long getFame0() {
        return fame0;
    }

    public void setFame0(Long fame0) {
        this.fame0 = fame0;
    }

    public String getTitle1() {
        return title1;
    }

    public void setTitle1(String title1) {
        this.title1 = title1;
    }

    public Long getValue1() {
        return value1;
    }

    public void setValue1(Long value1) {
        this.value1 = value1;
    }

    public Long getFame1() {
        return fame1;
    }

    public void setFame1(Long fame1) {
        this.fame1 = fame1;
    }

    public String getTitle2() {
        return title2;
    }

    public void setTitle2(String title2) {
        this.title2 = title2;
    }

    public Long getValue2() {
        return value2;
    }

    public void setValue2(Long value2) {
        this.value2 = value2;
    }

    public Long getFame2() {
        return fame2;
    }

    public void setFame2(Long fame2) {
        this.fame2 = fame2;
    }

    public String getTitle3() {
        return title3;
    }

    public void setTitle3(String title3) {
        this.title3 = title3;
    }

    public Long getValue3() {
        return value3;
    }

    public void setValue3(Long value3) {
        this.value3 = value3;
    }

    public Long getFame3() {
        return fame3;
    }

    public void setFame3(Long fame3) {
        this.fame3 = fame3;
    }

    public List<TitleStateView> getStats() {
        return stats;
    }

    public void setStats(List<TitleStateView> stats) {
        this.stats = stats;
    }

    public Optional<TitleView> getView()
    {
        return !view.isEmpty() ? Optional.of(view.get(0)) : Optional.empty();
    }

    public void setView(List<TitleView> view) { this.view = view; }

    public CardTitle getCardTitle() { return cardTitle; }

    public void setCardTitle(CardTitle cardTitle) { this.cardTitle = cardTitle; }
}
