package com.fiestawiki.mobs.mobinfo.repository;

import com.fiestawiki.mobs.mobinfo.MobInfo;
import com.fiestawiki.mobs.mobinfo.MobInfo_;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.query.QueryUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional(readOnly = true)
public class MobInfoRepositoryImpl implements MobInfoRepositoryCustom
{
    @PersistenceContext
    EntityManager entityManager;

    CriteriaBuilder criteriaBuilder;

    @PostConstruct
    private void init()
    {
        criteriaBuilder = entityManager.getCriteriaBuilder();
    }

    @Override
    public Page<MobInfo> findMobs(
            Pageable pageable,
            Long min,
            Long max,
            Optional<String> search,
            Optional<Long> location,
            Optional<Long> original
    )
    {
        CriteriaQuery<MobInfo> query = criteriaBuilder.createQuery(MobInfo.class);
        {
            Root<MobInfo> mob = query.from(MobInfo.class);
            query.select(mob);

            List<Predicate> criteria = getCriteriaForMob(mob, min, max, search, location, original);
            criteria.add(criteriaBuilder.equal(mob.get(MobInfo_.isNPC), 0));

            query.where(criteriaBuilder.and(criteria.toArray(new Predicate[0])));

            List<Order> orders = QueryUtils.toOrders(pageable.getSort(), mob, criteriaBuilder);
            orders.add(criteriaBuilder.asc(mob.get(MobInfo_.name)));
            query.orderBy(orders);
        }

        CriteriaQuery<Long> countQuery = criteriaBuilder.createQuery(Long.class);
        {
            Root<MobInfo> mob = countQuery.from(MobInfo.class);
            countQuery.select(criteriaBuilder.count(mob));

            List<Predicate> criteria = getCriteriaForMob(mob, min, max, search, location, original);
            criteria.add(criteriaBuilder.equal(mob.get(MobInfo_.isNPC), 0));

            countQuery.where(criteriaBuilder.and(criteria.toArray(new Predicate[0])));
        }

        return getPageMobInfo(query, countQuery, pageable);
    }

    private Page<MobInfo> getPageMobInfo(CriteriaQuery<MobInfo> mobQuery, CriteriaQuery<Long> countQuery, Pageable pageable)
    {
        TypedQuery<MobInfo> typedQuery = entityManager.createQuery(mobQuery);
        typedQuery.setFirstResult((int)pageable.getOffset());
        typedQuery.setMaxResults(pageable.getPageSize());

        TypedQuery<Long> count = entityManager.createQuery(countQuery);

        return new PageImpl<>(typedQuery.getResultList(), pageable, count.getSingleResult());
    }

    private List<Predicate> getCriteriaForMob(
            Root<MobInfo> mob,
            Long min,
            Long max,
            Optional<String> search,
            Optional<Long> location,
            Optional<Long> original)
    {
        List<Predicate> criteria = new ArrayList<>();

        criteria.add(criteriaBuilder.isTrue(mob.get(MobInfo_.active)));

        criteria.add(criteriaBuilder.greaterThanOrEqualTo(mob.get(MobInfo_.level), min));
        criteria.add(criteriaBuilder.lessThanOrEqualTo(mob.get(MobInfo_.level), max));

        if(location.isPresent())
            criteria.add(criteriaBuilder.equal(mob.get(MobInfo_.location), location.get()));
        if(search.isPresent())
        {
            criteria.add(criteriaBuilder.like(criteriaBuilder.lower(mob.<String>get(MobInfo_.name)), "%" + search.get().toLowerCase() + "%"));
        }

        return criteria;
    }
}
