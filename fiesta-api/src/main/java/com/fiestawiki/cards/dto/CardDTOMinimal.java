package com.fiestawiki.cards.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CardDTOMinimal {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("itemInx")
    private String itemInx;

    @JsonProperty("grade_type")
    private Long gradeType;

    @JsonProperty("view")
    private CardViewDTO view;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItemInx() {
        return itemInx;
    }

    public void setItemInx(String itemInx) {
        this.itemInx = itemInx;
    }

    public Long getGradeType() {
        return gradeType;
    }

    public void setGradeType(Long gradeType) {
        this.gradeType = gradeType;
    }

    public CardViewDTO getView() {
        return view;
    }

    public void setView(CardViewDTO view) {
        this.view = view;
    }
}
