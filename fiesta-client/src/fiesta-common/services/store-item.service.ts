import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { StoreItemView, StoreItem, StoreItemSet, StoreCategory, StoreItemViewDTO, StoreItemDTO, StoreCategoryDTO, StoreItemSetDTO } from '@fiesta-common/dto/store-items/store-item';
import { environment } from '@environments/environment';
import { Page } from '@fiesta-common/dto/items/item-info-page';
import { map } from 'rxjs/operators';
import { ItemService } from './item.service';

export class StoreItemSearch {
   search: string;
   stats: number[];
   slots: number[];
   duration: number[];
   designer: number[];
}

@Injectable({
   providedIn: 'root'
})
export class StoreItemService {

   constructor(
      private http: HttpClient,
      private itemService: ItemService) { }

   getStoreItemView(id: number): Observable<StoreItemView> {
      return this.http.get<StoreItemViewDTO>(environment.api + 'store-item-view/' + id).pipe(
         map(result => this.convertStoreItemView(result))
      );
   }

   getStoreItemViews(pageIndex: number, search: StoreItemSearch): Observable<Page<StoreItemView>> {

      let params = new HttpParams();

      if (search.search && search.search.length > 0) {
         params = params.append('search', search.search);
      }
      if (search.stats && search.stats.length > 0) {
         for (const s of search.stats) {
            params = params.append('stats', s.toString());
         }
      }
      if (search.slots && search.slots.length > 0) {
         for (const s of search.slots) {
            params = params.append('slots', s.toString());
         }
      }
      if (search.duration && search.duration.length > 0) {
         for (const s of search.duration) {
            params = params.append('duration', s.toString());
         }
      }
      if (search.designer && search.designer.length > 0) {
         for (const s of search.designer) {
            params = params.append('designer', s.toString());
         }
      }

      return this.http.get<Page<StoreItemViewDTO>>(environment.api + 'store-item-views/' + pageIndex, { params }).pipe(
         map(results => this.convertStoreItemViewPage(results))
      );
   }

   getStoreItem(id: number): Observable<StoreItem> {
      return this.http.get<StoreItemDTO>(environment.api + 'store-item/' + id).pipe(
         map(result => this.convertStoreItem(result))
      );
   }

   getStoreItemSet(id: number): Observable<StoreItemSet> {
      return this.http.get<StoreItemSet>(environment.api + 'store-item-set/' + id).pipe(
         map(i => this.convertStoreItemSet(i))
      );
   }

   getStoreCategories(): Observable<StoreCategory[]> {
      return this.http.get<StoreCategoryDTO[]>(environment.api + 'store-categories').pipe(
         map(result => result.map(c => this.convertStoreCategory(c)))
      );
   }

   convertStoreItem(item: StoreItemDTO): StoreItem {
      
      const _ = new StoreItem();

      _.id = item.id;
      _.item = this.itemService.convertItem(item.item);
      _.sc = item.sc;
      _.duration = item.duration;
      _.link = item.link;
      _.categories = item.categories.map(c => this.convertStoreCategory(c));
      _.set = this.convertStoreItemSet(item.set);

      return _;
   }

   convertStoreCategory(category: StoreCategoryDTO): StoreCategory
   {
      const _ = new StoreCategory();

      _.id = category.id;
      _.name = category.name;
      _.type = category.type;

      return _;
   }

   convertStoreItemSet(itemSet: StoreItemSetDTO): StoreItemSet
   {
      if(itemSet == null) {
         return null;
      }

      const _ = new StoreItemSet();

      _.id = itemSet.id;
      _.name = itemSet.name;

      return _;
   }

   convertStoreItemView(itemView: StoreItemViewDTO): StoreItemView 
   {
      const _ = new StoreItemView()

      _.id = itemView.id;
      _.icon = itemView.icon;
      _.description = itemView.description;
      _.name = itemView.name;
      _.image_previews = itemView.image_previews;
      _.store_items = itemView.store_items.map(x => this.convertStoreItem(x));

      return _;
   }

   convertStoreItemViewPage(pageDTO: Page<StoreItemViewDTO>): Page<StoreItemView> {
      const pageResult = new Page<StoreItemView>();

      pageResult.totalElements = pageDTO.totalElements;
      pageResult.totalPages = pageDTO.totalPages;
      pageResult.size = pageDTO.size;
      pageResult.number = pageDTO.number;

      pageResult.content = pageDTO.content.map(result => this.convertStoreItemView(result));

      return pageResult;
   }
}
