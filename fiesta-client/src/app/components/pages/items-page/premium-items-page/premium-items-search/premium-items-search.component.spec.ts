import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PremiumItemsSearchComponent } from './premium-items-search.component';

describe('PremiumItemsSearchComponent', () => {
  let component: PremiumItemsSearchComponent;
  let fixture: ComponentFixture<PremiumItemsSearchComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PremiumItemsSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PremiumItemsSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
