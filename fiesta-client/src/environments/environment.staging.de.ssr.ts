import { commonEnvironment } from './environment.common';

export const environment = {
    common: commonEnvironment,
    locale: {
        version: 'de',
        language: 'de',
        title: 'Fiesta Wiki (EU)',
        flag: 'flag-de'
    },
    production: false,
    debug: false,
    domain: 'http://beta-de.fiesta-wiki.com/',
    api: 'http://api-beta-de.fiesta-wiki.com/',
    static_assets: 'http://static-beta-de.fiesta-wiki.com/'
};
