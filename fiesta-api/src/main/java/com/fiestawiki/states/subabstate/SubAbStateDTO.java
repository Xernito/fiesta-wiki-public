package com.fiestawiki.states.subabstate;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class SubAbStateDTO
{
    @JsonProperty("id")
    private Long id;

    @JsonProperty("inxname")
    private String inxname;

    @JsonProperty("strength")
    private Long strength;

    @JsonProperty("type")
    private Long type;

    @JsonProperty("sub_type")
    private int subType;

    @JsonProperty("duration")
    private Long duration;

    @JsonProperty("effects")
    private List<SubAbStateEffectDTO> effects = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInxname() {
        return inxname;
    }

    public void setInxname(String inxname) {
        this.inxname = inxname;
    }

    public Long getStrength() {
        return strength;
    }

    public void setStrength(Long strength) {
        this.strength = strength;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public int getSubType() {
        return subType;
    }

    public void setSubType(int subType) {
        this.subType = subType;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public List<SubAbStateEffectDTO> getEffects() { return effects; }

    public void setEffects(List<SubAbStateEffectDTO> effects) { this.effects = effects; }
}
