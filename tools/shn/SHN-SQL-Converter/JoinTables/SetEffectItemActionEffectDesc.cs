﻿namespace SHN_SQL_Converter.JoinTables
{
    class SetEffectItemActionEffectDesc : JoinTableBase
    {
        public override void Create()
        {
            Table = Utils.CreateJoinTable(
                DataSet,
                DataSet.Tables["set_effect"],
                DataSet.Tables["item_action_effect_desc"],
                Tablename,
                "public"
                );
        }

        public override void Load()
        {
            Utils.AddJoinColumn<ushort>(
                DataSet,
                DataSet.Tables["set_effect"],
                DataSet.Tables["item_action_effect_desc"],
                "ItemActionID",
                "ItemActionID",
                Tablename
                );
        }
    }
}
