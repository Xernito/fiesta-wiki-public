package com.fiestawiki.states.abstate;

import com.fiestawiki.states.abstateview.AbStateView;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ab_state")
@EntityListeners(AuditingEntityListener.class)
public class AbState implements Serializable {

    @Id
    @Column(name = "unique_id")
    private Long id;

    @Column(name = "inxname")
    private String inxname;

    @ManyToOne
    @JoinColumn(
            name = "unique_id",
            referencedColumnName = "unique_id",
            updatable = false,
            insertable = false,
            foreignKey = @javax.persistence.ForeignKey(value = ConstraintMode.NO_CONSTRAINT)
    )
    private AbStateView view;

    @Column(name = "abstataindex")
    private Long index;

    @Column(name = "keeptimeratio")
    private Long keepTimeRatio;

    @Column(name = "keeptimepower")
    private Long keepTimePower;

    @Column(name = "stategrade")
    private int grade;

    @ManyToOne
    @JoinColumn(name = "party_state_1_fk")
    private AbState partyState1;

    @ManyToOne
    @JoinColumn(name = "party_state_2_fk")
    private AbState partyState2;

    @ManyToOne
    @JoinColumn(name = "party_state_3_fk")
    private AbState partyState3;

    @ManyToOne
    @JoinColumn(name = "party_state_4_fk")
    private AbState partyState4;

    @ManyToOne
    @JoinColumn(name = "party_state_5_fk")
    private AbState partyState5;

    @Column(name = "subabstate")
    private String subAbState;

    @Column(name = "dispelindex")
    private Long dispelIndex;

    @Column(name = "subdispelindex")
    private Long subDispelIndex;

    @Column(name = "abstatesavetype")
    private Long saveType;

    @Column(name = "mainstateinx")
    private String mainStateIndex;

    @Column(name = "duplicate")
    private int stackable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInxname() {
        return inxname;
    }

    public void setInxname(String inxname) {
        this.inxname = inxname;
    }

    public AbStateView getView() { return view; }

    public void setView(AbStateView view) { this.view = view; }

    public Long getIndex() {
        return index;
    }

    public void setIndex(Long index) {
        this.index = index;
    }

    public Long getKeepTimeRatio() {
        return keepTimeRatio;
    }

    public void setKeepTimeRatio(Long keepTimeRatio) {
        this.keepTimeRatio = keepTimeRatio;
    }

    public Long getKeepTimePower() {
        return keepTimePower;
    }

    public void setKeepTimePower(Long keepTimePower) {
        this.keepTimePower = keepTimePower;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public AbState getPartyState1() { return partyState1; }

    public void setPartyState1(AbState partyState1) { this.partyState1 = partyState1; }

    public AbState getPartyState2() { return partyState2; }

    public void setPartyState2(AbState partyState2) { this.partyState2 = partyState2; }

    public AbState getPartyState3() { return partyState3; }

    public void setPartyState3(AbState partyState3) { this.partyState3 = partyState3; }

    public AbState getPartyState4() { return partyState4; }

    public void setPartyState4(AbState partyState4) { this.partyState4 = partyState4; }

    public AbState getPartyState5() { return partyState5; }

    public void setPartyState5(AbState partyState5) { this.partyState5 = partyState5; }

    public String getSubAbState() {
        return subAbState;
    }

    public void setSubAbState(String subAbState) {
        this.subAbState = subAbState;
    }

    public Long getDispelIndex() {
        return dispelIndex;
    }

    public void setDispelIndex(Long dispelIndex) {
        this.dispelIndex = dispelIndex;
    }

    public Long getSubDispelIndex() {
        return subDispelIndex;
    }

    public void setSubDispelIndex(Long subDispelIndex) {
        this.subDispelIndex = subDispelIndex;
    }

    public Long getSaveType() {
        return saveType;
    }

    public void setSaveType(Long saveType) {
        this.saveType = saveType;
    }

    public String getMainStateIndex() {
        return mainStateIndex;
    }

    public void setMainStateIndex(String mainStateIndex) {
        this.mainStateIndex = mainStateIndex;
    }

    public int getStackable() {
        return stackable;
    }

    public void setStackable(int stackable) {
        this.stackable = stackable;
    }
}
