package com.fiestawiki.skills.dao.passiveskillview;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PassiveSkillViewDTO {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("icon_index")
    private Long icon;

    @JsonProperty("icon_file")
    private String iconFile;

    @JsonProperty("red")
    private Long red;

    @JsonProperty("green")
    private Long green;

    @JsonProperty("blue")
    private Long blue;

    @JsonProperty("description")
    private String description;

    @JsonProperty("required_level")
    private Long requiredLevel;

    @JsonProperty("required_class")
    private Long requiredClass;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIcon() {
        return icon;
    }

    public void setIcon(Long icon) {
        this.icon = icon;
    }

    public String getIconFile() {
        return iconFile.toLowerCase();
    }

    public void setIconFile(String iconFile) {
        this.iconFile = iconFile;
    }

    public Long getRed() {
        return red;
    }

    public void setRed(Long red) {
        this.red = red;
    }

    public Long getGreen() {
        return green;
    }

    public void setGreen(Long green) {
        this.green = green;
    }

    public Long getBlue() {
        return blue;
    }

    public void setBlue(Long blue) {
        this.blue = blue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getRequiredLevel() {
        return requiredLevel;
    }

    public void setRequiredLevel(Long requiredLevel) {
        this.requiredLevel = requiredLevel;
    }

    public Long getRequiredClass() {
        return requiredClass;
    }

    public void setRequiredClass(Long requiredClass) {
        this.requiredClass = requiredClass;
    }
}
