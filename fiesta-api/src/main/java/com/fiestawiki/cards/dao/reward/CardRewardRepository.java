package com.fiestawiki.cards.dao.reward;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CardRewardRepository extends JpaRepository<CardReward, Long> {
}
