import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService, Credentials } from '../../services/login.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-admin-login-page',
  templateUrl: './admin-login-page.component.html',
  styleUrls: ['./admin-login-page.component.scss']
})
export class AdminLoginPageComponent implements OnInit {

   loginFrom: FormGroup;

   get isLoggedIn() { return this.loginService.isLoggedIn(); }

   constructor(
      private router: Router,
      private loginService: LoginService,
      private formBuilder: FormBuilder,
   ) {
      this.loginFrom = this.formBuilder.group({
         username: '',
         password: ''
      });
   }

   ngOnInit() {
   }

   onSubmit(loginData: Credentials) {

      this.loginService.authenticate(loginData).subscribe(
         auth => {
            if (auth.ok) {
               this.router.navigate(['/admin']);
            }
         }
      );
   }
}
