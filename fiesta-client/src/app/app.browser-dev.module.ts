import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppModule } from './app.module';
import { AppComponent } from '@components/app.component';
import { AdminModule } from 'admin/admin.module';
import { SandboxModule } from 'sandbox/sandbox.module';

@NgModule({
    imports: [
        AdminModule,
        SandboxModule,
        AppModule,
        BrowserModule.withServerTransition({ appId: 'fiesta' }),
    ],
    bootstrap: [AppComponent]
})
export class BrowserAppDevModule { }
