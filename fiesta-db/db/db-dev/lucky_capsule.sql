CREATE TABLE "lucky_capsule"."lucky_capsule"(
 ID  bigint UNIQUE PRIMARY KEY,
 Inxname  VARCHAR(2000),
 ServerID  int,
 Type  smallint,
 capsule_item_fk  bigint
);
INSERT INTO "lucky_capsule"."lucky_capsule"(ID, Inxname, ServerID, Type, capsule_item_fk) VALUES (1, E'LuckyCapsuleRed', 58062, 2, 58062);
INSERT INTO "lucky_capsule"."lucky_capsule"(ID, Inxname, ServerID, Type, capsule_item_fk) VALUES (2, E'LuckyCapsuleRedGold', 63886, 2, 63886);
INSERT INTO "lucky_capsule"."lucky_capsule"(ID, Inxname, ServerID, Type, capsule_item_fk) VALUES (3, E'LuckyCapsuleBlue', 58063, 2, 58063);
INSERT INTO "lucky_capsule"."lucky_capsule"(ID, Inxname, ServerID, Type, capsule_item_fk) VALUES (4, E'LuckyCapsuleBlue_2', 872, 2, 872);
INSERT INTO "lucky_capsule"."lucky_capsule"(ID, Inxname, ServerID, Type, capsule_item_fk) VALUES (5, E'D_LuckyCapsule01', 59044, 1, 59044);
INSERT INTO "lucky_capsule"."lucky_capsule"(ID, Inxname, ServerID, Type, capsule_item_fk) VALUES (6, E'D_LuckyCapsule02', 59045, 1, 59045);
INSERT INTO "lucky_capsule"."lucky_capsule"(ID, Inxname, ServerID, Type, capsule_item_fk) VALUES (7, E'D_LuckyCapsule03', 59046, 1, 59046);
INSERT INTO "lucky_capsule"."lucky_capsule"(ID, Inxname, ServerID, Type, capsule_item_fk) VALUES (8, E'LuckyCapsule', 58061, 3, 58061);
