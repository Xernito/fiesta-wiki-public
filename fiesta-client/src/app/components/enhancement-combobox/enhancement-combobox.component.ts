import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';

@Component({
    selector: 'app-enhancement-combobox',
    templateUrl: './enhancement-combobox.component.html',
    styleUrls: ['./enhancement-combobox.component.scss']
})
export class EnhancementComboboxComponent implements OnInit {

    @Input()
    defaultValue = 0;

    @Input()
    max = 12;

    @Output() enhancementSelected = new EventEmitter<number>();

    get enhancementsUpgrade(): Array<number> {
        return new Array<number>(this.max + 1).fill(0).map((x, i) => i);
    }

    constructor() { }

    ngOnInit() {
    }

    onEnhancementChanged(change: MatSelectChange) {

        this.enhancementSelected.emit(change.value);
    }
}
