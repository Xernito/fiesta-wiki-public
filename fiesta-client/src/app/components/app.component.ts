import { Component, PLATFORM_ID, Inject } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { environment } from '@environments/environment';
import { MatIconRegistry } from '@angular/material/icon';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { DomSanitizer } from '@angular/platform-browser';
import { NestedTreeControl } from '@angular/cdk/tree';
import { ROUTES_URL } from '@app/routing/routes';
import { isPlatformBrowser } from '@angular/common';
import { CLASSES_NAME, PAGES_NAME, WEAPONS_NAME } from '@app/global-strings';

// declare ga as a function to set and sent the events
declare let gtag: Function;

interface LinkNode {
    name: string;
    link?: string;
    children?: LinkNode[];
}

const TREE_DATA: LinkNode[] = [
    {
        name: PAGES_NAME.quests,
        link: ROUTES_URL.quests
    },
    {
        name: PAGES_NAME.items,
        children: [
            {
                name: PAGES_NAME.weapons,
                children: [
                    {
                        name: CLASSES_NAME.fighter,
                        children: [
                            {
                                name: WEAPONS_NAME.oneHandSword,
                                link: ROUTES_URL.weaponsOneHandSword
                            },
                            {
                                name: WEAPONS_NAME.twoHandSword,
                                link: ROUTES_URL.weaponsTwoHandSword
                            },
                            {
                                name: WEAPONS_NAME.axe,
                                link: ROUTES_URL.weaponsAxe
                            }
                        ]
                    },
                    {
                        name: CLASSES_NAME.cleric,
                        children: [
                            {
                                name: WEAPONS_NAME.mace,
                                link: ROUTES_URL.weaponsMace
                            },
                            {
                                name: WEAPONS_NAME.hammer,
                                link: ROUTES_URL.weaponsHammer
                            }
                        ]
                    },
                    {
                        name: CLASSES_NAME.archer,
                        children: [
                            {
                                name: WEAPONS_NAME.bow,
                                link: ROUTES_URL.weaponsBow
                            },
                            {
                                name: WEAPONS_NAME.crossbow,
                                link: ROUTES_URL.weaponsCrossbow
                            }
                        ]
                    },
                    {
                        name: CLASSES_NAME.mage,
                        children: [
                            {
                                name: WEAPONS_NAME.staff,
                                link: ROUTES_URL.weaponsStaff
                            },
                            {
                                name: WEAPONS_NAME.wand,
                                link: ROUTES_URL.weaponsWand
                            }
                        ]
                    },
                    {
                        name: CLASSES_NAME.trickster,
                        children: [
                            {
                                name: WEAPONS_NAME.claws,
                                link: ROUTES_URL.weaponsClaws
                            },
                            {
                                name: WEAPONS_NAME.dualSword,
                                link: ROUTES_URL.weaponsDualSword
                            }
                        ]
                    },
                    {
                        name: CLASSES_NAME.crusader,
                        children: [
                            {
                                name: WEAPONS_NAME.blade,
                                link: ROUTES_URL.weaponsBlade
                            }
                        ]
                    }
                ]
            },
            {
                name: PAGES_NAME.shields,
                children: [
                    {
                        name: PAGES_NAME.fighterShield,
                        link: ROUTES_URL.shieldsFighter
                    },
                    {
                        name: PAGES_NAME.clericShield,
                        link: ROUTES_URL.shieldsCleric
                    }
                ]
            },
            {
                name: PAGES_NAME.gears,
                children: [
                    {
                        name: CLASSES_NAME.fighter,
                        children: [
                            {
                                name: CLASSES_NAME.fighter,
                                link: ROUTES_URL.gearsFighter
                            },
                            {
                                name: CLASSES_NAME.cleverFighter,
                                link: ROUTES_URL.gearsCleverFighter
                            },
                            {
                                name: CLASSES_NAME.warrior,
                                link: ROUTES_URL.gearsWarrior
                            },
                            {
                                name: CLASSES_NAME.gladiator,
                                link: ROUTES_URL.gearsGladiator
                            },
                            {
                                name: CLASSES_NAME.knight,
                                link: ROUTES_URL.gearsKnight
                            }
                        ]
                    },
                    {
                        name: CLASSES_NAME.cleric,
                        children: [
                            {
                                name: CLASSES_NAME.cleric,
                                link: ROUTES_URL.gearsCleric
                            },
                            {
                                name: CLASSES_NAME.highCleric,
                                link: ROUTES_URL.gearsHighCleric
                            },
                            {
                                name: CLASSES_NAME.paladin,
                                link: ROUTES_URL.gearsPaladin
                            },
                            {
                                name: CLASSES_NAME.guardian,
                                link: ROUTES_URL.gearsGuardian
                            },
                            {
                                name: CLASSES_NAME.holyKnight,
                                link: ROUTES_URL.gearsHolyKnight
                            }
                        ]
                    },
                    {
                        name: CLASSES_NAME.archer,
                        children: [
                            {
                                name: CLASSES_NAME.archer,
                                link: ROUTES_URL.gearsArcher
                            },
                            {
                                name: CLASSES_NAME.hawkArcher,
                                link: ROUTES_URL.gearsHawkArcher
                            },
                            {
                                name: CLASSES_NAME.scout,
                                link: ROUTES_URL.gearsScout
                            },
                            {
                                name: CLASSES_NAME.ranger,
                                link: ROUTES_URL.gearsRanger
                            },
                            {
                                name: CLASSES_NAME.sharpShooter,
                                link: ROUTES_URL.gearsSharpShooter
                            }
                        ]
                    },
                    {
                        name: CLASSES_NAME.mage,
                        children: [
                            {
                                name: CLASSES_NAME.mage,
                                link: ROUTES_URL.gearsMage
                            },
                            {
                                name: CLASSES_NAME.wizMage,
                                link: ROUTES_URL.gearsWizMage
                            },
                            {
                                name: CLASSES_NAME.enchanter,
                                link: ROUTES_URL.gearsEnchanter
                            },
                            {
                                name: CLASSES_NAME.wizard,
                                link: ROUTES_URL.gearsWizard
                            },
                            {
                                name: CLASSES_NAME.warlock,
                                link: ROUTES_URL.gearsWarlock
                            }
                        ]
                    },
                    {
                        name: CLASSES_NAME.trickster,
                        children: [
                            {
                                name: CLASSES_NAME.trickster,
                                link: ROUTES_URL.gearsTrickster
                            },
                            {
                                name: CLASSES_NAME.gambit,
                                link: ROUTES_URL.gearsGambit
                            },
                            {
                                name: CLASSES_NAME.renegade,
                                link: ROUTES_URL.gearsRenegade
                            },
                            {
                                name: CLASSES_NAME.reaper,
                                link: ROUTES_URL.gearsReaper
                            },
                            {
                                name: CLASSES_NAME.spectre,
                                link: ROUTES_URL.gearsSpectre
                            }
                        ]
                    },
                    {
                        name: CLASSES_NAME.crusader,
                        children: [
                            {
                                name: CLASSES_NAME.crusader,
                                link: ROUTES_URL.gearsCrusader
                            },
                            {
                                name: CLASSES_NAME.templar,
                                link: ROUTES_URL.gearsTemplar
                            }
                        ]
                    }
                ]
            },
            {
                name: PAGES_NAME.jewels,
                children: [
                    {
                        name: PAGES_NAME.rings,
                        link: ROUTES_URL.jewelsRings
                    },
                    {
                        name: PAGES_NAME.earrings,
                        link: ROUTES_URL.jewelsEarrings
                    },
                    {
                        name: PAGES_NAME.necklace,
                        link: ROUTES_URL.jewelsNecklace
                    }
                ]
            },
            {
                name: PAGES_NAME.bracelets,
                link: ROUTES_URL.bracelets
            },
            {
                name: PAGES_NAME.talismans,
                link: ROUTES_URL.talismans
            },
            {
                name: PAGES_NAME.premiumItems,
                link: ROUTES_URL.premium
            }
        ]
    },
    {
        name: PAGES_NAME.skills,
        link: ROUTES_URL.skills
    },
    {
        name: PAGES_NAME.alchemy,
        children: [
            {
                name: PAGES_NAME.stones,
                link: ROUTES_URL.alchemyStones
            },
            {
                name: PAGES_NAME.potions,
                link: ROUTES_URL.alchemyPotions
            },
            {
                name: PAGES_NAME.scrolls,
                link: ROUTES_URL.alchemyScrolls
            },
            {
                name: PAGES_NAME.composition,
                link: ROUTES_URL.alchemyComp
            },
            {
                name: PAGES_NAME.decomposition,
                link: ROUTES_URL.alchemyDecomp
            },
        ]
    },
    {
        name: PAGES_NAME.titles,
        link: ROUTES_URL.titles
    },
    {
        name: PAGES_NAME.cards,
        link: ROUTES_URL.cards
    },
    {
        name: PAGES_NAME.luckyCapsule,
        link: ROUTES_URL.luckyCapsule
    },
    {
        name: PAGES_NAME.mobs,
        link: ROUTES_URL.mobs
    },
    {
        name: PAGES_NAME.licenses,
        link: ROUTES_URL.licenses
    },
];

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

    panelOpenState = false;

    treeControl = new NestedTreeControl<LinkNode>(node => node.children);
    dataSource = new MatTreeNestedDataSource<LinkNode>();

    hasChild = (_: number, node: LinkNode) => !!node.children && node.children.length > 0;

    constructor(
        @Inject(PLATFORM_ID) platformId: any,
        private iconRegistry: MatIconRegistry,
        private sanitizer: DomSanitizer,
        public router: Router,
    ) {
        const isBrowser = isPlatformBrowser(platformId);

        this.dataSource.data = TREE_DATA;

        if (isBrowser) {
            // subscribe to router events and send page views to Google Analytics
            this.router.events.subscribe(event => {
                if (event instanceof NavigationEnd) {
                    gtag(
                        'event',
                        'page_view',
                        {
                            send_to: environment.common.googleAnalyticsTrackingCode,
                            page_path: event.urlAfterRedirects
                        }
                    );
                }
            });
        }

        // SSR need full path to retreive assets
        const domain = isBrowser ? '' : environment.domain;

        this.iconRegistry.addSvgIcon(
            'loot',
            this.sanitizer.bypassSecurityTrustResourceUrl(domain + 'assets/loot-icon.svg'));

        this.iconRegistry.addSvgIcon(
            'site-icon',
            this.sanitizer.bypassSecurityTrustResourceUrl(domain + 'assets/site-icon.svg'));

        this.iconRegistry.addSvgIcon(
            'arrow-right',
            this.sanitizer.bypassSecurityTrustResourceUrl(domain + 'assets/arrow-right.svg'));

        this.iconRegistry.addSvgIcon(
            'flag-de',
            this.sanitizer.bypassSecurityTrustResourceUrl(domain + 'assets/flags/de.svg'));

        this.iconRegistry.addSvgIcon(
            'flag-us',
            this.sanitizer.bypassSecurityTrustResourceUrl(domain + 'assets/flags/us.svg'));
    }
}
