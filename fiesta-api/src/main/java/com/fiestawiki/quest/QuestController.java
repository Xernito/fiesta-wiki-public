package com.fiestawiki.quest;

import com.fiestawiki.quest.dao.questlevelinfo.QuestLevelInfoDTO;
import com.fiestawiki.quest.dto.QuestDTO;
import com.fiestawiki.quest.dao.dialog.QuestDialog;
import com.fiestawiki.quest.dto.QuestDataDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class QuestController
{
    @Autowired
    private QuestService questService;

    @GetMapping("dialog/{id}")
    public ResponseEntity<QuestDialog> getDialog(@PathVariable(value = "id") Long id)
            throws Exception
    {
        QuestDialog questDialog = questService.getDialog(id);

        return ResponseEntity.ok(questDialog);
    }

    @GetMapping("quest/{id}")
    public ResponseEntity<QuestDTO> getQuest(@PathVariable(value = "id") Long id)
            throws Exception
    {
        QuestDTO quest = questService.getQuest(id);

        return ResponseEntity.ok(quest);
    }

    @GetMapping("quest-data/{id}")
    public ResponseEntity<QuestDataDTO> getQuestData(@PathVariable(value = "id") Long id)
            throws Exception
    {
        QuestDataDTO quest = questService.getQuestData(id);

        return ResponseEntity.ok(quest);
    }

    @GetMapping("quest/level/{level}")
    public ResponseEntity<QuestLevelInfoDTO> getQuestsByLevel(
            @PathVariable(value = "level") Integer level,
            @RequestParam(name = "minLevel") Optional<Integer> minLevel
    )
            throws Exception
    {
        QuestLevelInfoDTO questLevelInfo = questService.getQuestsByLevel(level, minLevel);

        return ResponseEntity.ok(questLevelInfo);
    }

    @GetMapping("quests/page")
    public ResponseEntity<List<QuestLevelInfoDTO>> getQuestsPage(
            @RequestParam(name = "start") Integer start,
            @RequestParam(name = "range") Integer range,
            @RequestParam(name = "hideEvents", defaultValue = "false") Boolean hideEvents,
            @RequestParam(name = "class") Optional<Integer> iClass)
            throws Exception
    {
        List<QuestLevelInfoDTO> quest_list = questService.getQuestsPage(start, range, hideEvents, iClass);

        return ResponseEntity.ok(quest_list);
    }
}
