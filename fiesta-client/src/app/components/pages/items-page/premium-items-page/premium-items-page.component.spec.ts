import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PremiumItemsPageComponent } from './premium-items-page.component';

describe('PremiumItemsPageComponent', () => {
  let component: PremiumItemsPageComponent;
  let fixture: ComponentFixture<PremiumItemsPageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PremiumItemsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PremiumItemsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
