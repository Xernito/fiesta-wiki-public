﻿using iQuest;
using System.Data;
using System.IO;
using System.Linq;

namespace SHN_SQL_Converter.Files
{
    class PassiveSkillAbState : SHNFileBase
    {
        public override void Load(string input_folder)
        {
            SHNFile shn = new SHNFile(Path.Combine(input_folder, Filename));

            Table = shn.table;
            Table.Namespace = Namespace;
            Table.TableName = Tablename;

            Table.Columns["PS_InxName"].Unique = true;
        }

        public override void Patch(string patch_folder)
        {
            CSVPatch patch = new CSVPatch();
            patch.Load(Path.Combine(patch_folder, @"passive_skill_ab_state_patch.csv"), Table.Columns);

            // PointAttack is Ranger passive lv100 skill
            // It doesnt have AbState so remove it
            (from row in Table.AsEnumerable()
             where row["PS_InxName"].ToString().StartsWith("PointAttack")
             select row).ToList().ForEach(row => Table.Rows.Remove(row));

            Utils.PatchTable_Update(Table, patch.table, "ps_inxname", new string[] { "ps_abstateinx", "strength" });
        }

        public override void CreateConstraints()
        {
            Utils.AddForeignKeyColumn(
                DataSet,
                Table.TableName, new string[] { "PS_AbStateInx" },
                "ab_state", new string[] { "InxName" },
                "ab_state_fk");
        }
    }
}
